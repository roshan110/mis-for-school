<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Parents extends Model
{
    protected $fillable=['school_id','name','email','address','contact','status'];
    protected $table='parents';
    public function students()
    {
    	return $this->hasOne('App\Student');
    }
}
