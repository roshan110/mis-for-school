<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Book extends Model
{
    protected $attributes = [
        'status' => 1,


    ];
    protected $fillable=['title','category_id','subcategory_id','author','quantity','details','remaining'];

    public function category()
    {
        return $this->belongsTo('App\Bookcategory')->orderby('order_by');
    }

    public function subcategory()
    {
        return $this->belongsTo('App\Bookcategory');
    }
    public function getSubcategoryTitleAttribute()
    {
        return $this->subcategory ? $this->subcategory->title : 'None';
    }
    public function getCategoryTitleAttribute()
    {
        return $this->category ? $this->category->title : 'None';
    }

    public function getUrlAttribute()
    {
        return url('book/' . $this->slug);
    }
    public function issue(){
        return $this->hasMany('App\Issue');
    }
}
