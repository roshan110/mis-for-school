<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Exam extends Model
{
    protected $fillable=['school_id','examtype','description','status'];
    protected $attribute=['status'=>1];
    protected $table='exams';
}
