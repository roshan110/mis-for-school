<?php

namespace App\Mail;
use App\Bill;
use App\Student;
use App\Academicinfo;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class FeeDetails extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public $student;
    public $student_details;
    public $bill; 

    public function __construct(Student $student,Bill $bill,$student_details)
    {
        
        $this->student=$student;
        $this->bill=$bill;
        $email=$student->parent->email;
        $student_details=Academicinfo::find($student_details['id']);
        $this->student_details=$student_details;
        $this->email=$email;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->markdown('emails.fee.feeitem')
        ->to($this->email,$this->student->parent->name);
    }

}
