<?php

namespace App\Mail;
use App\Academicinfo;
use App\Student;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class MarksheetDetails extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public $exam_name;
    public $data;
    public $student; 
    public $student_details;
    public $marksheet_status;

    public function __construct(Student $student,$student_details,$exam_name,$data,$marksheet_status)
    {
        $this->student=$student;
        $student_details=Academicinfo::find($student_details['id']);
        $this->student_details=$student_details;
        $this->data=$data;
        $this->exam_name=$exam_name;
        // dd($data);
        $this->marksheet_status=$marksheet_status;
        $email=$student->parent->email;
        $this->email=$email;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->markdown('emails.marksheet.marksheetitem')->to($this->email,$this->student->parent->name);;
    }
}
