<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Teacher extends Model
{
    protected $fillable=['user_id','school_id','name','email','address','contact','status'];
    protected $table='teachers';
}
