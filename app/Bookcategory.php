<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Bookcategory extends Model
{
    protected $attributes = [
        'status' => 1,
        'parent_id' => 0,
    ];

    protected $table='book_categories';

    protected $fillable=['title','parent_id','details'];

    public function parent()
    {
        return $this->belongsTo('App\Bookcategory', 'parent_id');
    }

    public function child()
    {
        return $this->hasMany('App\Bookcategory', 'parent_id');
    }

    public function books()
    {
        return $this->hasMany('App\Book')->orderby('order_by');
    }

    public function getParentTitleAttribute()
    {
        return $this->parent ? $this->parent->title : 'None';
    }

    public function getUrlAttribute()
    {
        return url('bookcategory/'.$this->slug);
    }
}
