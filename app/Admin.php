<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Admin extends Model
{
    protected $fillable=['admin_id','school_id','name','address','contact','profile_img','status'];
    protected $table='admin_details';

    public function school()
    {
        return $this->belongsTo('App\School');
    }
}
