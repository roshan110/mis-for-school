<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Subject extends Model
{
    protected $fillable=['school_id','name','fullmarks','practical_marks','theory_marks','description','status'];

    protected $table='subjects';

    public function grades()
    {
        return $this->belongsToMany('App\Grade');
    }
}
