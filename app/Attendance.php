<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Attendance extends Model
{
    protected $table='attendances';
    protected $attributes=[
    	'status'=>1,
    ];
    protected $fillable=['grade_id','section_id','date','status','st_id'];

    public function student()
	{
   		 return $this->belongsTo('App\Student');
	}
	public function grade()
	{
		return $this->belongsTo('App\Grade','grade_id');
	}
	public function section()
	{
		return $this->belongsTo('App\Section','section_id');
	}

	public static function student_name($id)
	{
		$name = \App\Student::find($id);
		return $name->name;
	}

	public static function grade_name($id)
	{
		$name = \App\Grade::find($id);
		return $name->name;
	}
	public static function section_name($id)
	{
		$name = \App\Section::find($id);
		return $name->name;
	}
	
}
