<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class School extends Model
{
    protected $fillable=['name','address','contact','status'];

    protected $table='schools';

	public function admin()
	{
		return $this->hasOne('App\Admin');
	}

    public static function school_name($id)
	{
		$name = \App\School::find($id);
		return $name->name;
	}
	public static function school_address($id)
	{
		$name = \App\School::find($id);
		return $name->address;
	}
}
