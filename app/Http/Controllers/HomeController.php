<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use jeremykenedy\LaravelRoles\Traits\HasRoleAndPermission;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    use HasRoleAndPermission;
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        if (Auth::user()->hasRole('superadmin'))
        return redirect('superadmin');

        elseif(Auth::user()->hasRole('admin'))
        {
            return redirect('admin');
        }
        elseif(Auth::user()->hasRole('teacher'))
        {
            return redirect('teacher');
        }
        elseif(Auth::user()->hasRole('librarian'))
        {
            return redirect('librarian');
        }
        elseif(Auth::user()->hasRole('accountant'))
        {
            return redirect('accountant');
        }
    }
}
