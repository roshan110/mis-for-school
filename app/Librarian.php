<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Librarian extends Model
{
    protected $fillable=[
        'school_id','user_id','name','address','contact','image','status'

    ];
    protected $attribute=[
        'status'=>1,
    ];

    public function scopeActive($query)
    {
        return $query->where('status',1);
    }
}
