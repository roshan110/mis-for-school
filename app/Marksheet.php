<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Marksheet extends Model
{
    protected $fillable=['school_id','grade_id','section_id','student_id','exam_id','total_theory_marks','total_practical_marks','total_marks','percentage','result','rank','gpa','grade','status'];
    protected $table='marksheets';
    public static function exam_name($id)
	{
		$name = \App\Exam::find($id);
		return $name->name;
	}
}
