<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Bill extends Model
{
    protected $fillable=['student_id','total','date','status'];
    protected $table='bills';
    	public static function feetype_name($id)
	{
		$name = \App\Feetype::find($id);
		return $name->feetype;
	}
		public function student()
    {
        return $this->belongsTo('App\Student');
    }
    public function billitems()
    {
        return $this->hasMany('App\BillItem');
    }
}
