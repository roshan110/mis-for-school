<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Grade extends Model
{
    protected $table='grades';
    protected $fillable=['name','school_id','description','status','subject'];

    public function subjects()
    {
        return $this->belongsToMany('App\Subject');
    }
    public function students()
    {
        return $this->hasMany('App\Student');
    }

}
