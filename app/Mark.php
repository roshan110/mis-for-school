<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Mark extends Model
{
  	protected $fillable=['school_id','grade_id','section_id','subject_id','st_id','marks','practical_marks','examtype','status'];
    protected $table='marks';

    public static function exam_name($id)
    {
        $name = \App\Exam::find($id);
        return $name->examtype;
    }

    public function subject(){
    	return $this->belongsTo('App\Subject');
    }
}
