<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BillItem extends Model
{
    protected $fillable=['bill_id','feetype','price','status'];
    protected $table='bill_items';
     public function bill()
    {
        return $this->belongsTo('App\Bill');
    }

}
