<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Academicinfo extends Model
{
     protected $fillable=['s_id','registration_no','academic_year','grade_id','section_id','roll_no','status'];
        protected $attribute=[
        'status'=>1,
    ];
     protected $table='academicinfos';

	public static function grade_name($id)
	{
		$name = \App\Grade::find($id);
		return $name->name;
	}
	public static function section_name($id)
	{
		$name = \App\Section::find($id);
		return $name->name;
	}	
}
