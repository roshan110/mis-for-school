<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Student extends Model
{
    protected $fillable=['school_id','parent_id','grade_id','section_id','name','email','address','contact','status','date_of_birth','gender','religion'];

    protected $table='students';

    public function attendances()
    {
    	return $this->hasMany('App\Attendance');
    }
    public function grades()
    {
        return $this->belongsTo('App\Grade','grade_id');
    }
     public function bills()
    {
        return $this->hasMany('App\Bill');
    }

    public function parent()
    {
        return $this->belongsTo('App\Parents');
    }
}
