<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Gradefee extends Model
{
    protected $fillable=['grade_id','type_id','price','status'];
    protected $table='gradefees';
}
