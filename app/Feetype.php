<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Feetype extends Model
{
    protected $fillable=['school-id','feetype','description','status'];
    protected $table='feetypes';
}
