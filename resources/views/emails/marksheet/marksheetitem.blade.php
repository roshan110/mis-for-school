@component('mail::message')
# <p style="text-align: center;">{{\App\School::school_name($student ->school_id)}}</p>
**Student Name:** {{$student->name}}<br>
**Class:** {{\App\Academicinfo::grade_name($student_details['grade_id'])}}<br>
**Section:** {{\App\Academicinfo::section_name($student_details['section_id'])}}<br>
**Roll No:** {{$student_details['roll_no']}}<br>
**Exam:** {{$exam_name['examtype']}}<br>

<table width="100%" bgcolor="#3C8DBC" cellspacing="1">
	<tr bgcolor="#fff" style="font-size: 10px;">
		<th align="left" style="padding: 10px;">S.N.</th>		
		<th align="right" style="padding: 10px;">Course Name</th>
		<th align="left" style="padding: 10px;">Full Marks</th>
		<th align="right" style="padding: 10px;">Theory Marks</th>
		<th align="right" style="padding: 10px;">Practical Marks</th>
		<th align="right" style="padding: 10px;">Obtained Theory Marks</th>
		<th align="right" style="padding: 10px;">Obtained Practical Marks</th>
		<th align="right" style="padding: 10px;">Total Obtained Marks</th>
	</tr>
	@php $i=1; @endphp
	@foreach($data as $datas)
		<tr bgcolor="white" style="color:#000; font-size: 10px;">
			<td style="padding: 5px 5px;" align="right">{{$i}}</td>
			<td style="padding: 5px 5px;" align="right">{{$datas['subject_name']}}</td>
            <td style="padding: 5px 5px;" align="right">{{$datas['fullmarks']}}</td>
            <td style="padding: 5px 5px;" align="right">{{$datas['theory_marks']}}</td>
            <td style="padding: 5px 5px;" align="right">{{$datas['practical_marks']}}</td>
            <td style="padding: 5px 5px;" align="right">{{$datas['obtained_theory_marks']}}</td>
            <td style="padding: 5px 5px;" align="right">{{$datas['obtained_practical_marks']}}</td>
            <td style="padding: 5px 5px;" align="right">{{$datas['total_obtained_marks']}}</td>
		</tr>
		@php $i++; @endphp
		@endforeach

	<tr  bgcolor="#f1f1f1" style="color:#000; font-size: 10px;">
		<td colspan="5" style="padding: 5px 5px;" align="right">Total</td>
		<td style="padding: 5px 5px;" align="right">{{$marksheet_status->total_theory_marks}}</td>
        <td style="padding: 5px 5px;" align="right">{{$marksheet_status->total_practical_marks}}</td>
        <td style="padding: 5px 5px;" align="right">{{$marksheet_status->total_marks}}</td> 

	</tr>
</table>
<br>
**Result:** {{$marksheet_status->result}}<br>
**Percentage:** {{$marksheet_status->percentage}} % <br>
Thanks,<br>
# {{\App\School::school_name($student ->school_id)}}<br>

{{ config('app.name') }}
@endcomponent
