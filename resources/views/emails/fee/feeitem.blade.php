@component('mail::message')

## Dear {{$student->parent->name}} <br>
 The fee of your child has been issued on date {{$bill->date}} <br>
 ## The fee details are listed below.

 **Bill Id:** {{$bill->id}}<br><br>
 **Student Name:** {{$student->name}}<br>
 **Class:** {{\App\Academicinfo::grade_name($student_details['grade_id'])}}<br>
 **Section:** {{\App\Academicinfo::section_name($student_details['section_id'])}}<br>
** Roll No:** {{$student_details['roll_no']}}<br>

<table width="100%" bgcolor="#ddd" cellspacing="1">
	<tr bgcolor="$ddd">
		<th align="left" style="padding: 10px;">Fee Type</th>
		<th align="right" style="padding: 10px;">Price</th>
	</tr>
	@foreach($bill->billitems as $bil)
		<tr bgcolor="white">
			<td style="padding: 5px 10px;" align="right">{{$bil->feetype}}</td>
			<td style="padding: 5px 10px;" align="right">{{$bil->price}}</td>
		</tr>
	@endforeach
	<tr bgcolor="#f1f1f1">
		<td style="padding: 5px 10px" align="right">Total</td>
		<td style="padding: 5px 10px" align="right">{{$bill->total}}</td>
	</tr>
</table>
<br>
	

Thanks,<br>
{{ config('app.name') }}



@endcomponent
