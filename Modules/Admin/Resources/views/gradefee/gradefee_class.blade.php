@extends('layouts.admin_dashboard')

@section('content')
    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="header_content">
                <h2 class="text-left">
                    Add Fee
                </h2>

            </div>
        </div>
    </div>
    <div class="container">
        <div class="row">
             <div class="grade_class">
                @foreach($grade_data as $gd)              
                    <div class="col-sm-1">
                <a href="{{route('gradefee/add',$gd['id'])}}" class="btn btn-primary"><i class="fa fa-user"></i>{{$gd['name']}}</a>        
                    </div>
                    @endforeach                   
                </div>    
            </div>
        </div>
    </div>
@endsection
