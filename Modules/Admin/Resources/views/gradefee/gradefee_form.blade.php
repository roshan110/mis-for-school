
<h5 class="text-muted">GradeFee Details</h5>
<hr>

<div class="form-group row">
    <div class="col-md-3 col-sm-4 col-xs-6">
        <label for="grade">Grade:{{$grade->name}}</label>
        <input type="hidden" name="grade_id" value="{{$grade->id}}">
    </div>
</div>

 <div class="body_block mt-5" style="background-color: white; margin-top: 20px;">
                <div class="table-responsive">
                    <table class=" table table-hover">
                        <thead class="thead-dark bg-primary">
                        <tr>
                            <th>S.N</th>
                            <th>Fee Type</th>
                            <th>Price</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php $i=1; ?>
                        @foreach($feetype as $ft)                     
                            <tr>
                                <td>{{$i}}</td>
                                <td>{{$ft->feetype}}<input type="hidden" name="feetype[]" value="{{$ft->id}}"></td>
                                <td><input  type="number" autocomplete="off" class="first_column"  id="" name="gradefee[]"></td>
                            </tr>                            
                        <?php $i++; ?>
                        @endforeach
                        <tr>                                                    
                        </tr>                                    
                        </tbody>
                    </table>
                </div>
            </div>
<hr>
<div class="form-group row">
        <div class="col-md-3 col-sm-3 col-xs-6">
            <label for="sr-only"></label>
        </div>
        <div class="col-md-9 col-sm-8 col-xs-6">
            <div class="form-check-inline">
                <label class="form-check-label">

              


                </label>
            </div>

        </div>
    </div>

<button type="submit" class="btn btn-success text-white mx-auto">
    <i class="fa fa-save"> Save</i>
</button>
