@extends('layouts.admin_dashboard')

@section('content')

    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="header_content">
                <h2 class="text-left">
                    Accountant Edit {{$accountant->name}}
                </h2>

            </div>
        </div>
    </div>
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-sm-8 col-xs-12 mx-auto">
                <form action="{{route('accountants.update',$accountant->id)}}" method="post" enctype="multipart/form-data">
                    {{ method_field('PUT') }}
                    {!! csrf_field() !!}

                    @include('admin::accountant.form')

                    <button type="submit" class="btn btn-success text-white mx-auto">
                        <i class="fa fa-save"> Save</i>
                    </button>
                </form>
            </div>
        </div>
    </div>
@endsection
