<h5 class="text-muted">Student Record</h5>
<form method="POST" action="{{route('attendance/store')}}]">
<div class="class-section-details">
    <div class="row">
        <div class="col-sm-4">
            Grade:{{\App\Attendance::grade_name($grade_id)}}
            <input type="hidden" name="grade_id" value="{{$grade_id}}">
        </div>
        <div class="col-sm-4">
             Section:{{$section}}
             <input type="hidden" name="section_id" value="{{$section_id}}">
        </div>
        <div class="col-sm-4">
             Date:{{$date_data}}
             <input type="hidden" name="date" value="{{$date_data}}">
        </div>
    </div>
</div>
<div class="body_block">

<div class="table-responsive">
    <table class=" table table-hover">
        <thead class="thead-dark">
            <tr>
                <th>S.N.</th>
                    <th>Name</th>
                    <th>Attendance Status</th>
            </tr>
        </thead>
        <tbody>
            @if(count($section_data)>0)
            <?php $i=0; ?>
            @foreach($section_data as $sd)
            <tr>
                <td>{{$i+1}}</td>
                <td>{{$sd['name']}}
              <input type="hidden" name="student[{{$i}}]" value="{{$sd['id']}}">      
                </td>
                <td>
                    <div class="radio">                                        
                        <label>
                            <input type="radio" name="status[{{$i}}]" value="1" checked>Present                                        
                        </label>
                    </div>
                         <div class="radio">
                                            <label>
                                            <input type="radio" name="status[{{$i}}]" value="0" >
                                        Absent
                                    .</label>
                                        </div>
                </td>            
            </tr>
            <?php $i++; ?>
            @endforeach
            @else
            <tr>
            <td>No students.</td>
        </tr>
            @endif

        </tbody>
    </table>
</div>
</div>
<button type="submit" class="btn btn-success text-white mx-auto">
    <i class="fa fa-save"> Save</i>
</button>
</form>

