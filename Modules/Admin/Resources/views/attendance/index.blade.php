@extends('layouts.admin_dashboard')

@section('content')

    {{--container-fluid already included--}}

    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="heading_block">
                <div class="row">
                    <div class="col-md-6 col-sm-6 col-xs-12">
                        <h4>Attendance</h4>
                    </div>
                               </div>
            </div>
            <hr>
            <div class="grade">
                <div class="row">
                    @foreach($grade_data as $gd)
                    <div class="col-md-1">
                        <a href="{{route('attendance/sectioncheck',$gd->id)}}" class="btn btn-primary">{{$gd->name}}</a>
                    </div>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
@endsection
