@extends('layouts.admin_dashboard')

@section('content')

    {{--container-fluid already included--}}

    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="heading_block">
                <div class="row">
                    <div class="col-md-6 col-sm-6 col-xs-12">
                        <h4>Attendance</h4>
                        <h5 class="text-muted">Section of class <b>{{$grade}}</b> </h5>
                    </div>                        
                
                </div>
            </div>
            <div class="class_section">
                <div class="row">  
                @foreach($section_data as $sd)              
                    <div class="col-sm-1">
                <a href="{{route('attendance/create',$sd['id'])}}" class="btn btn-primary"><i class="fa fa-user"></i>{{$sd['name']}}</a>        
                    </div>
                    @endforeach                   
                </div>    
            </div>
                
            </div>            
   
        </div>
    </div>
@endsection
