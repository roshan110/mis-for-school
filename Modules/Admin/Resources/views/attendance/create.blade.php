@extends('layouts.admin_dashboard')
@section('content')

    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="heading_block">
                <div class="row">
                    <div class="col-md-6 col-sm-6 col-xs-12">
                        <h4>Attendance</h4>
                    </div>                        
                
                </div>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-sm-8 col-xs-12 mx-auto">
                <form action="{{route('attendance/store')}}" method="post" enctype="multipart/form-data">
                    {{csrf_field()}}
                    @include('admin::attendance.attendance_form')

                </form>
            </div>
        </div>
    </div>

@endsection

@section('script')
   

@endsection
