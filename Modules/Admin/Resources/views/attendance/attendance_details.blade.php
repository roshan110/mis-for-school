@extends('layouts.admin_dashboard')

@section('content')

    {{--container-fluid already included--}}

    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="heading_block">
                <div class="row">
                    <div class="col-md-6 col-sm-6 col-xs-12">
                        <h4>Attendance Record of class <b>{{\App\Attendance::grade_name($class)}}</b> , section <b>{{\App\Attendance::section_name($section)}}</b>  of date <b>{{$date}}</b></h4>
                    </div>
                    
                </div>
            </div>

            <div class="body_block">
                <div class="table-responsive">
                    <table class=" table table-hover">
                        <thead class="thead-dark">
                        <tr>
                            <th>S.N</th>
                            <th>Name</th>
                            <th>Attendance Status</th>
                        </tr>
                        </thead>
                        <tbody>
                            <?php $i=1; ?>
                            @foreach($check as $ad)
                            <tr>
                                <td>{{$i}}</td>
                                <td>{{\App\Attendance::student_name($ad->st_id)}}</td>
                                <td><span class="label {{$ad['status'] == '1' ? 'label-success' : 'label-danger'}}">{{$ad['status'] =='1' ?  'Present' : 'Absent'}}</span></td>
                                <?php $i++; ?>
                                @endforeach
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection
