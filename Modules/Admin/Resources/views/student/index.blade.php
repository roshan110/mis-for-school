@extends('layouts.admin_dashboard')

@section('content')

    {{--container-fluid already included--}}

    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="heading_block">
                <div class="row">
                    <div class="col-md-6 col-sm-6 col-xs-12">
                        <h4>Student</h4>
                    </div>
                    <div class="col-md-6 col-sm-6 col-xs-12 text-right">
                        <a href="{{route('student/create')}}" class="btn btn-primary"><i class="fa fa-plus"></i>Add Student</a>
                    </div>
                </div>
            </div>

            <div class="body_block">
                <div class="table-responsive">
                    <table class=" table table-hover">
                        <thead class="thead-dark">
                        <tr>
                            <th>#</th>
                            <th>Image</th>
                            <th>Name</th>
                            <th>Email</th>
                            <th>Address</th>
                            <th>Contact</th>
                            <th>Status</th>
                            <th>Action</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php $i=1; ?>
                        @foreach($student_data as $sd)
                            <tr>
                                <td>{{$i}}</td>
                                <td>
                                    <img src="{{$sd->image ? asset('uploads/profile/'.$sd->image) :asset('uploads/profile/user.png') }}" alt="Profile" width="50px" height="50px">
                                </td>
                                <td>{{$sd->name}}</td>
                                <td>{{$sd->email}}</td>
                                <td>{{$sd->address}}</td>
                                <td>{{$sd->contact}}</td>
                                <td>
                                     @if($sd->status)
                            <a class="text-success" data-toggle="tooltip" data-placement="bottom" title="Click for inactive" href="{{route('students.showhidestudent',$sd->id)}}"><i class="lnr lnr-checkmark-circle"></i>Active</a>
                            @else
                            <a class="text-danger" data-toggle="tooltip" data-placement="bottom" title="Click for active" href="{{route('students.showhidestudent',$sd->id)}}"><i class="lnr lnr-cross-circle"></i>InActive</a>
                            @endif

                        </td> 
                                <td>
                            <ul class="list-inline">
                                <li class="list-inline-item">
                                    <a href="{{route('student/edit',$sd->id)}}"><i class="fa fa-edit"></i></a>
                                </li>
                                <li class="list-inline-item">
                                    <a href="{{route('student/delete',$sd->id)}}"><i class="fa fa-trash text-danger"></i></a>
                                </li>
                                 <li class="list-inline-item">
                                    <a href="{{route('student/view',$sd->id)}}"><i class="fa fa-eye text-success"></i></a> 
                                </li>                                               
                             </ul>
                        </td>
                            </tr>
                        <?php $i++; ?>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection
