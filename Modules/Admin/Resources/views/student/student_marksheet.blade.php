@extends('layouts.admin_dashboard')

@section('content')

    {{--container-fluid already included--}}

    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="heading_block">
                <div class="row">
                    <div class="col-md-6 col-sm-6 col-xs-12">
                        <h4>Student</h4>
                    </div>
                    
                </div>
            </div>

  
        </div>
           <div class="body_block" style="background-color: white; padding-top: 20px;">
            <div class="container">
                <div class="row">
                    <div class="col-md-6" >
                        <div class="list-group row">
                            <div class="col-md-3 col-sm-4 col-xs-6">
                                <label for="name">Name:</label>
                            </div>
                            <div class="col-md-9 col-sm-8 col-xs-6">
                                 {{$student->name}}
                            </div>
                        </div>
                        <div class="list-group row">
                            <div class="col-md-3 col-sm-4 col-xs-6">
                                <label for="name">Grade:</label>
                            </div>
                            <div class="col-md-9 col-sm-8 col-xs-6">
                                 {{\App\Academicinfo::grade_name($student_details['grade_id'])}}
                            </div>
                        </div>
                        <div class="list-group row">
                            <div class="col-md-3 col-sm-4 col-xs-6">
                                <label for="name">Section:</label>
                            </div>
                            <div class="col-md-9 col-sm-8 col-xs-6">
                                  {{\App\Academicinfo::section_name($student_details['section_id'])}}
                            </div>
                        </div>
                    </div>
                     <div class="col-md-6">
                        <div class="list-group row">
                            <div class="col-md-3 col-sm-4 col-xs-6">
                                <label for="name">Roll No:</label>
                            </div>
                            <div class="col-md-9 col-sm-8 col-xs-6">
                                 {{$student_details['roll_no']}}
                            </div>
                        </div>
                        <div class="list-group row">
                            <div class="col-md-3 col-sm-4 col-xs-6">
                                <label for="name">Exam:</label>
                            </div>
                            <div class="col-md-9 col-sm-8 col-xs-6">
                                 {{$exam_name['examtype']}}
                            </div>
                        </div>
                      
                    </div>
                </div>
            </div>
            </div>
         <div class="body_block mt-5" style="background-color: white; margin-top: 20px;">
                <div class="table-responsive">
                    <table class=" table table-hover">
                        <thead class="thead-dark bg-primary">
                        <tr>
                            <th>S.N</th>
                            <th>Course Name</th>
                            <th>Full Marks</th>
                            <th>Theory Marks</th>                           
                            <th>Practical Marks</th>                            
                            <th>Obtained Theory Marks</th>
                            <th>Obtained Practical Marks</th>
                            <th>Total Obtained Marks</th>
                            <!-- <th></th> -->
                        </tr>

                        </thead>
                        <tbody>
                        <?php $i=1; ?>
                        @foreach($data as $datas)
                            <tr>
                                <td>{{$i}}</td>
                                <td>{{$datas['subject_name']}}</td>
                                <td>{{$datas['fullmarks']}}</td>
                                <td>{{$datas['theory_marks']}}</td>
                                <td>{{$datas['practical_marks']}}</td>
                                <td>{{$datas['obtained_theory_marks']}}</td>
                                <td>{{$datas['obtained_practical_marks']}}</td>
                                <td>{{$datas['total_obtained_marks']}}</td>

                               <!--  <td>
                                    <a href=""><button class="btn btn-xs text-black">Create Marksheet</button></a>
                                </td> -->
                            </tr>
                            
                        <?php $i++; ?>
                        @endforeach

                        <tr>
                            <td  colspan="5" class="text-bold text-center">Total:</td>
                            <td class="text-bold">{{$total_obtained_theory_marks}}</td>
                            <td class="text-bold">{{$total_obtained_practical_marks}}</td>
                            <td class="text-bold">{{$total_obtained_fullmarks}}</td>                                
                        </tr>
                        </tbody>
                    </table>
                </div>
            </div><br>

    </div>
@endsection
