@extends('layouts.admin_dashboard')
@section('css')
    <style>
    .profile_image_body
    {
    background: gainsboro;
        margin-top: 55px;

    }
        .profile_image
        {
            padding: 30px;;
        }
        .change_p_image
        {
            margin-top: 20px;;
        }
    </style>

@endsection


@section('content')

    {{--container-fluid already included--}}

    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="heading_block">
                <div class="row">
                    <div class="col-md-6 col-sm-6 col-xs-12">
                        <h4>Student</h4>
                    </div>
                   
                </div>
            </div>

         
        </div>
    </div>
    <div class="container-fluid">
        <div class="container">
            <div class="row">
                <div class="col-md-3">
                     <div class="profile_image_body">
                        <div class="text-center profile_image">
                             <img src="{{isset($student->image) ? asset('uploads/profile/'.$student->image) : asset('images/icons-user.png') }}" alt="Profile" style="height: 205px;"><br>
                            <!-- <button type="button" class="btn btn-sm btn-secondary btn-flat mt-3" data-toggle="modal" data-target="#exampleModalLong">Change Profile Picture</button> -->
                        </div>
                     </div>
                     <h5>Registration Number:</h5>
                     <h5>Phone: {{$student->contact}}</h5>
                     <h5>Email: {{$student->email}}</h5>
                </div>
                <div class="col-md-9">
                      <ul class="nav nav-tabs">
    <li class="active"><a data-toggle="tab" href="#home">Profile</a></li>
    <li><a data-toggle="tab" href="#menu1">Subjects</a></li>
    <li><a data-toggle="tab" href="#menu2">Attendances</a></li>
    <li><a data-toggle="tab" href="#menu3">Marks and Results</a></li>
  </ul>

  <div class="tab-content">
    <div id="home" class="tab-pane fade in active">
      <h4 class="text-secondary">Student's Personal Info:</h4>
      <div class="row">
        <div class="col-md-6">
      <div class="list-group row">
            <div class="col-md-3 col-sm-4 col-xs-6">
                <label for="name">Name:</label>
            </div>
             <div class="col-md-9 col-sm-8 col-xs-6">
              {{$student->name}} 
    </div>
      </div>
      <div class="list-group row">
            <div class="col-md-3 col-sm-4 col-xs-6">
                <label for="name">Gender:</label>              
            </div>
            <div class="col-md-9 col-sm-8 col-xs-6">
                {{$student->gender}} 
            </div>            
      </div>
      <div class="list-group row">
            <div class="col-md-3 col-sm-4 col-xs-6">
                <label for="name">Email:</label>
            </div>
            <div class="col-md-9 col-sm-8 col-xs-6">
                {{$student->email}} 
            </div>
      </div>
      <div class="list-group row">
            <div class="col-md-3 col-sm-4 col-xs-6">
                <label for="name">Contact:</label>
            </div>
            <div class="col-md-9 col-sm-8 col-xs-6">
              {{$student->contact}} 
             </div>
      </div>
    </div>
        <div class="col-md-6">
      <div class="list-group row">
            <div class="col-md-3 col-sm-4 col-xs-6">
                <label for="name">Date of Birth:</label>
            </div>
            <div class="col-md-9 col-sm-8 col-xs-6">
                {{$student->date_of_birth}} 
            </div>
      </div>
      <div class="list-group row">
            <div class="col-md-3 col-sm-4 col-xs-6">
                <label for="name">Religion:</label>
            </div>
            <div class="col-md-9 col-sm-8 col-xs-6">
                 {{$student->religion}} 
            </div>
      </div>
    </div>
    </div>
   <h4 class="text-secondary">Parents Info:</h4>
      <div class="row">
        <div class="col-md-6">
      <div class="list-group row">
            <div class="col-md-3 col-sm-4 col-xs-6">
                <label for="name">Name:</label>
            </div>
             <div class="col-md-9 col-sm-8 col-xs-6">  
                  {{$parent_data['name']}}           
             </div>
      </div>
      <div class="list-group row">
            <div class="col-md-3 col-sm-4 col-xs-6">
                <label for="name">Email:</label>
            </div>
            <div class="col-md-9 col-sm-8 col-xs-6">
                {{$parent_data['email']}} 
            </div>
      </div>
      <div class="list-group row">
            <div class="col-md-3 col-sm-4 col-xs-6">
                <label for="name">Contact:</label>
            </div>
            <div class="col-md-9 col-sm-8 col-xs-6">
               {{$parent_data['contact']}} 
             </div>
      </div>
      <div class="list-group row">
            <div class="col-md-3 col-sm-4 col-xs-6">
                <label for="name">Address:</label>
            </div>
            <div class="col-md-9 col-sm-8 col-xs-6">
                {{$parent_data['address']}} 
             </div>
      </div>
    </div>

    </div>
    <div class="row">
      <div class="col-md-6">
           <h4 class="text-secondary">Academic Info:</h4>
      </div>
      <div class="col-md-6">
        <a href=""  data-toggle="modal" data-target="#exampleModal" data-toggle="tooltip" data-placement="bottom" title="Upgrade Class" ><button class="btn btn-primary btn-xs"><i class="fa fa-arrow-up">Upgrade</i></button></a>
           <!--   <a href=""><button class="btn btn-primary btn-xs"><i class="fa fa-edit">Edit</i></button></a> -->
      </div>
    </div>
      
      <div class="row">
        <div class="col-md-6">
      <div class="list-group row">
            <div class="col-md-3 col-sm-4 col-xs-6">
                <label for="name">Academic Year:</label>
            </div>
             <div class="col-md-9 col-sm-8 col-xs-6">  
                  {{$academic_data['academic_year']}}    
             </div>
      </div>
      <div class="list-group row">
            <div class="col-md-3 col-sm-4 col-xs-6">
                <label for="name">Class:</label>
            </div>
            <div class="col-md-9 col-sm-8 col-xs-6">
                {{$grade_data['name']}} 
            </div>
      </div>
      <div class="list-group row">
            <div class="col-md-3 col-sm-4 col-xs-6">
                <label for="name">Roll No:</label>
            </div>
            <div class="col-md-9 col-sm-8 col-xs-6">
                 {{$academic_data['roll_no']}}
             </div>
      </div>
      <div class="list-group row">
           <div class="col-md-3 col-sm-4 col-xs-6">
                <label for="name">Section:</label>
            </div>
            <div class="col-md-9 col-sm-8 col-xs-6">
                 {{$section_data['name']}}
            </div>            
      </div>
    </div>
            <div class="col-md-6">
      <div class="list-group row">
         <div class="col-md-3 col-sm-4 col-xs-6">
                <label for="name">Registration No:</label>
            </div>
            <div class="col-md-9 col-sm-8 col-xs-6">
                {{$student->id}}
             </div>
      </div>
 
    </div>
    </div>
    </div>
    <div id="menu1" class="tab-pane fade">
      <h3>Subjects</h3>
    </div>
    <div id="menu2" class="tab-pane fade">
      <h3>Attendances</h3>
    </div>
    <div id="menu3" class="tab-pane fade">
      <h3>Marks and Result</h3>
       @foreach($examtype as $et)
                        <div class="col-md-1">
                          

                            <a href="" class="btn btn-primary">{{\App\Mark::exam_name($et['exam_id'])}}</a>

                            
                        </div>
                    @endforeach
    </div>
  </div>
                </div>
            </div>

</div>
</div> 


<!-- Modal -->
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h3 class="modal-title" id="exampleModalLabel">Upgrade Class</h3>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form action="{{route('student/academicinfostore',$academic_data['id'])}}" method="post" enctype="multipart/form-data">
          @csrf
                   

                
        <div class="form-group row">
    <div class="col-md-3 col-sm-4 col-xs-6">
        <label for="class">Class</label>
    </div>
    <div class="col-md-9 col-sm-8 col-xs-6">
      <select name="grades" id="class" class="form-control dynamic" data-dependent="section">
            <option  selected="selected" value=""></option>
            @foreach($grades_data as $gd)
                <option value="{{$gd->id}}">{{$gd->name}}</option>
            @endforeach
        </select>
    </div>
</div>
<div class="form-group row">
    <div class="col-md-3 col-sm-4 col-xs-6">
        <label for="section">Section:</label>
    </div>
    <div class="col-md-9 col-sm-8 col-xs-6">
        <select name="section" id="section" class="form-control dynamic">
            <option  selected="selected">Select Section</option>

        </select>
    </div>
</div>
<div class="form-group row">
    <div class="col-md-3 col-sm-4 col-xs-6">
        <label for="rollno">Roll No:</label>
    </div>
    <div class="col-md-9 col-sm-8 col-xs-6">
       <input type="number" name="rollno" id="rollno" class="form-control" autocomplete="off" value="">
    </div>
</div>
<div class="form-group row">
    <div class="col-md-3 col-sm-4 col-xs-6">
        <label for="ayear">Academic Year:</label>
    </div>
    <div class="col-md-9 col-sm-8 col-xs-6">
       <input type="text" name="academicyear" id="year" class="form-control" autocomplete="off" value="">
    </div>
</div><hr style="border-top: 1px solid #f1f1f1;">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary">Save changes</button>
      </form>
      </div>
     <!--  <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary">Save changes</button>
      </div> -->
    </div>
  </div>
</div> 
@endsection
@section('script')
    <script>
        $(document).ready(function(){

            $('.dynamic').change(function(){
                if($(this).val() != '')
                {
                    var select = $(this).attr("id");
                    var value = $(this).val();
                    var dependent = $(this).data('dependent');
                    var _token = $('input[name="_token"]').val();
                    console.log(select);
                    console.log(value);
                    console.log(dependent);
                    $.ajax({
                        url:"{{ route('dynamindependent.fetch_section') }}",
                        method:"POST",
                        data:{select:select, value:value, _token:_token, dependent:dependent},
                        success:function(result)
                        {

                            $('#'+dependent).html(result);
                        }

                    })
                }
            });

            $('#class').change(function(){
                $('#section').val('');
            });



        });
    </script>

@endsection

