<h5 class="text-muted">Class Details</h5>
<hr>
<div class="form-group row">
    <div class="col-md-3 col-sm-4 col-xs-6">
        <label for="parent">Parent:</label>
    </div>
    <div class="col-md-9 col-sm-8 col-xs-6">
        <select name="parentid" id="parent" class="form-control">
            <option  selected="selected">Select Parent</option>
            @foreach($parent_data as $pd)
                <option value="{{$pd->id}}">{{$pd->name}}</option>
            @endforeach
        </select>
    </div>
</div>
<div class="form-group row">
    <div class="col-md-3 col-sm-4 col-xs-6">
        <label for="class">Class:</label>
    </div>
    <div class="col-md-9 col-sm-8 col-xs-6">
        <select name="grade" id="class" class="form-control dynamic" data-dependent="section">
            <option  selected="selected">Select Class</option>
            @foreach($grade_data as $cd)
                <option value="{{$cd->id}}">{{$cd->name}}</option>
            @endforeach
        </select>
    </div>
</div>
<div class="form-group row">
    <div class="col-md-3 col-sm-4 col-xs-6">
        <label for="section">Section:</label>
    </div>
    <div class="col-md-9 col-sm-8 col-xs-6">
        <select name="section" id="section" class="form-control dynamic">
            <option  selected="selected">Select Section</option>

        </select>
    </div>
</div>
<div class="form-group row">
    <div class="col-md-3 col-sm-4 col-xs-6">
        <label for="rollno">Roll No:</label>
    </div>
    <div class="col-md-9 col-sm-8 col-xs-6">
        <input type="number" name="rollno" class="form-control" min="1">
    </div>
</div>
<h5 class="text-muted">Student Details</h5>
<hr>
<div class="form-group row">
    <div class="col-md-3 col-sm-4 col-xs-6">
        <label for="name">Name:</label>
    </div>
    <div class="col-md-9 col-sm-8 col-xs-6">
        <input type="text" name="name" id="name" class="form-control" autocomplete="off" value="{{old('name',isset($student) ? $student->name : null)}}">
    </div>
</div>
<div class="form-group row">
    <div class="col-md-3 col-sm-4 col-xs-6">
        <label for="email">Email:</label>
    </div>
    <div class="col-md-9 col-sm-8 col-xs-6">
        <input type="email" name="email" id="email" class="form-control" autocomplete="off" value="{{old('email',isset($student) ? $student->email : null)}}">
    </div>
</div>
<div class="form-group row">
    <div class="col-md-3 col-sm-4 col-xs-6">
        <label for="address">Address:</label>
    </div>
    <div class="col-md-9 col-sm-8 col-xs-6">
        <input type="text" name="address" id="address" class="form-control" autocomplete="off" value="{{old('address',isset($student) ? $student->address : null)}}">
    </div>
</div>
<div class="form-group row">
    <div class="col-md-3 col-sm-4 col-xs-6">
        <label for="contact">Contact:</label>
    </div>
    <div class="col-md-9 col-sm-8 col-xs-6">
        <input type="text" name="contact" id="contact" class="form-control" autocomplete="off" value="{{old('contact',isset($student) ? $student->contact : null)}}">
    </div>
</div>
<div class="form-group row">
    <div class="col-md-3 col-sm-4 col-xs-6">
        <label for="image">Image:</label>
    </div>
    <div class="col-md-9 col-sm-8 col-xs-6">
        <input type="file" name="image" id="image" class="form-control" autocomplete="off">
    </div>
</div>
<div class="form-group row">
    <div class="col-md-3 col-sm-4 col-xs-6">
        <label for="dob">Date of Birth:</label>
    </div>
    <div class="col-md-9 col-sm-8 col-xs-6">
        <input type="date" name="dob" id="dob" class="form-control" autocomplete="off" value="">
    </div>
</div>
<div class="form-group row">
    <div class="col-md-3 col-sm-4 col-xs-6">
        <label for="gender">Gender:</label>
    </div>
    <div class="col-md-9 col-sm-8 col-xs-6">
        <input type="radio" name="gender" id="gender" value="male"> Male
        <input type="radio" name="gender" id="gender" value="female"> Female
        <input type="radio" name="gender" id="gender" value="others"> Others
    </div>
</div>
<div class="form-group row">
    <div class="col-md-3 col-sm-4 col-xs-6">
        <label for="religion">Religion:</label>
    </div>
    <div class="col-md-9 col-sm-8 col-xs-6">
        <input type="text" name="religion" id="religion" class="form-control" autocomplete="off" value="{{old('contact',isset($student) ? $student->contact : null)}}">
    </div>
</div>
<button type="submit" class="btn btn-success text-white mx-auto">
    <i class="fa fa-save"> Save</i>
</button>
