@extends('layouts.admin_dashboard')


@section('content')
    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="header_content">
                <h2 class="text-left">
                    Student Add
                </h2>

            </div>
        </div>
    </div>
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-sm-8 col-xs-12 mx-auto">
                <form action="{{route('student/store')}}" method="post" enctype="multipart/form-data">
                    @csrf
                    @include('admin::student.student_form')

                </form>
            </div>
        </div>
    </div>

@endsection

@section('script')
    <script>
        $(document).ready(function(){

            $('.dynamic').change(function(){
                if($(this).val() != '')
                {
                    var select = $(this).attr("id");
                    var value = $(this).val();
                    var dependent = $(this).data('dependent');
                    var _token = $('input[name="_token"]').val();
                    console.log(select);
                    console.log(value);
                    console.log(dependent);
                    $.ajax({
                        url:"{{ route('dynamindependent.fetch_section') }}",
                        method:"POST",
                        data:{select:select, value:value, _token:_token, dependent:dependent},
                        success:function(result)
                        {

                            $('#'+dependent).html(result);
                        }

                    })
                }
            });

            $('#class').change(function(){
                $('#section').val('');
            });



        });
    </script>

@endsection