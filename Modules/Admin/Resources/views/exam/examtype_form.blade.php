<h5 class="text-muted">Exam Details</h5>
<hr>

<div class="form-group row">
    <div class="col-md-3 col-sm-4 col-xs-6">
        <label for="name">Exam Name</label>
    </div>
    <div class="col-md-9 col-sm-8 col-xs-6">
        <input type="text" name="name" id="name" class="form-control" autocomplete="off">
    </div>
</div>

<div class="form-group row">
    <div class="col-md-3 col-sm-4 col-xs-6">
        <label for="description">Description-(optional):</label>
    </div>
    <div class="col-md-9 col-sm-8 col-xs-6">
        <textarea name="description" id="description" cols="20" rows="5" class="form-control" autocomplete="off"></textarea>
    </div>
</div>
<button type="submit" class="btn btn-success text-white mx-auto">
    <i class="fa fa-save"> Save</i>
</button>
