@extends('layouts.admin_dashboard')
@section('content')
{{--    container-fluid already included--}}
    <div class="row">
        <div class="col-md-8">
            <h4>Exam</h4>
        </div>
        <div class="col-md-4 text-right">
            <a href="{{route('examtype/add')}}" class="btn btn-primary">Create</a>
        </div>
    </div>

            <div class="body_block">
                <div class="table-responsive">
                    <table class=" table table-hover">
                        <thead class="thead-dark">
                        <tr>
                            <th>S.N.</th>
                            <th>Exam</th>
                            <th>Status</th>
                            <th>Action</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php $i=1; ?>
                        @foreach($examtype_data as $ed)
                        <tr>
                        <td>{{$i}}</td>                          
                        <td>{{$ed->examtype}}</td>  
                        <td>
                            @if($ed->status)
                            <a class="text-success" data-toggle="tooltip" data-placement="bottom" title="Click for inactive" href="{{route('exams.showhideexam',$ed->id)}}"><i class="lnr lnr-checkmark-circle"></i>Active</a>
                            @else
                            <a class="text-danger" data-toggle="tooltip" data-placement="bottom" title="Click for active" href="{{route('exams.showhideexam',$ed->id)}}"><i class="lnr lnr-cross-circle"></i>InActive</a>
                            @endif

                        </td>                    
                        <td>
                            <ul class="list-inline">
                                <li class="list-inline-item">
                                     <a href="{{route('examtype/edit',$ed->id)}}"><i class="fa fa-edit"></i></a>
                                </li>
                                <li class="list-inline-item">
                                    <a href="{{route('examtype/delete',$ed->id)}}" onclick="return confirm('Want to delete?'); "><i class="fa fa-trash text-danger"></i></a> 
                                </li>                                             
                             </ul>
                        </td>

                       
                      
                        </tr>
                          <?php $i++; ?>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>

    @stop
