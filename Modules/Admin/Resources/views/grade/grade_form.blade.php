<h5 class="text-muted">Grade Details</h5>
<hr>

<div class="form-group row">
    <div class="col-md-3 col-sm-4 col-xs-6">
        <label for="name">Name:</label>
    </div>
    <div class="col-md-9 col-sm-8 col-xs-6">
        <input type="text" name="name" id="name" class="form-control" autocomplete="off" value="{{old('name',isset($grade) ? $grade->name : null)}}">
    </div>
</div>

<div class="form-group row">
    <div class="col-md-3 col-sm-4 col-xs-6">
        <label for="description">Description-(optional):</label>
    </div>
    <div class="col-md-9 col-sm-8 col-xs-6">
        <textarea name="description" id="description" cols="20" rows="5" class="form-control" autocomplete="off"></textarea>
    </div>
</div>

<h5 class="text-muted">Choose subject</h5>
<hr>
<div class="form-group row">
        <div class="col-md-3 col-sm-3 col-xs-6">
            <label for="sr-only"></label>
        </div>
        <div class="col-md-9 col-sm-8 col-xs-6">
            <div class="form-check-inline">
                <label class="form-check-label">

              

                    @foreach($subjects as $subject)
                    <input name="subject[]" type="checkbox" class="form-check-input" value="{{$subject->id}}" {{ is_array(old('subject', isset($grade) ? $grade->subjects()->allRelatedIds()->toArray():null)) && in_array($subject->id, old('subject', isset($grade) ? $grade->subjects()->allRelatedIds()->toArray():null)) ? 'checked=checked': null }} >{{$subject->name}}
                    &nbsp;&nbsp;
                    @endforeach

                </label>
            </div>

        </div>
    </div>

<button type="submit" class="btn btn-success text-white mx-auto">
    <i class="fa fa-save"> Save</i>
</button>
