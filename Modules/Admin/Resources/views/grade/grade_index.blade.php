@extends('layouts.admin_dashboard')
@section('content')
{{--    container-fluid already included--}}
    <div class="row">
        <div class="col-md-8">
            <h4>Grades</h4>
        </div>
        <div class="col-md-4 text-right">
            <a href="{{route('grade/create')}}" class="btn btn-primary">Create</a>
        </div>
    </div>
     <div class="body_block">
                <div class="table-responsive">
                    <table class=" table table-hover">
                        <thead class="thead-dark">                        	
                        <tr>
                            <th>S.N.</th>
                            <th>Grade Name</th>
                            <th>Status</th>
                            <th>Action</th>
                        </tr>
                        </thead>
                        <tbody>
                        	<?php $i=1; ?>
                        	@foreach($grade_data as $gd)
                        	<tr>
                        		<td>{{$i}}</td>
                        		<td>{{$gd->name}}</td>
                                        <td>
                                     @if($gd->status)
                            <a class="text-success" data-toggle="tooltip" data-placement="bottom" title="Click for inactive" href="{{route('grades.showhidegrade',$gd->id)}}"><i class="lnr lnr-checkmark-circle"></i>Active</a>
                            @else
                            <a class="text-danger" data-toggle="tooltip" data-placement="bottom" title="Click for active" href="{{route('grades.showhidegrade',$gd->id)}}"><i class="lnr lnr-cross-circle"></i>InActive</a>
                            @endif

                        </td>
                                <td>
                            <ul class="list-inline">
                                <li class="list-inline-item">
                                     <a href="{{route('grade/edit',$gd->id)}}"><i class="fa fa-edit"></i></a>
                                </li>
                                <li class="list-inline-item">
                                    <a href="{{route('grade/delete',$gd->id)}}" onclick="return confirm('Want to delete?'); "><i class="fa fa-trash text-danger"></i></a> 
                                </li>                                             
                             </ul>
                        </td>
                        	</tr>
                        	<?php $i++; ?>
                        	@endforeach
                        </tbody>
                    </table>
                </div>
            </div>
    @stop
