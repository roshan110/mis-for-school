<h5 class="text-muted">Student Fee Details</h5>

<hr>
<div class="form-group row">
    <div class="col-md-3 col-sm-4 col-xs-6">
        <label for="parent">Select Fee Type:</label>
    </div>
    <div class="col-md-9 col-sm-8 col-xs-6">
          <div class="form-check-inline">
                <label class="form-check-label">            
                    @foreach($feetype as $ft)
                    <input name="feetype[]" type="checkbox" class="form-check-input" value="{{$ft->type_id}}">{{\App\Bill::feetype_name($ft->type_id)}}
                    &nbsp;&nbsp;
                    @endforeach
                </label>
            </div>
    </div>
</div>
@if(empty($student_details))
<button type="submit" class="btn btn-success text-white mx-auto">
    <i class="fa fa-save"> Save</i>
</button>
@endif
