@extends('layouts.admin_dashboard')
@section('content')
{{--    container-fluid already included--}}
    <div class="row">
        <div class="col-md-8">
            <h4>Student Fees</h4>
        </div>
        <div class="col-md-4 text-right">
            <a href="{{route('studentfee/grade')}}" class="btn btn-primary">Create Fee</a>
        </div>
    </div>
     <div class="body_block">
                <div class="table-responsive">
                    <table class=" table table-hover">
                        <thead class="thead-dark">                        	
                        <tr>
                            <th>S.N.</th>
                            
                        </tr>
                        </thead>
                        <tbody>
                        	
                        </tbody>
                    </table>
                </div>
            </div>
    @stop
