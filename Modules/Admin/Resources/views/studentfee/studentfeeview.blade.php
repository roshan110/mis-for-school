@extends('layouts.admin_dashboard')
@section('content')
{{--    container-fluid already included--}}
    <div class="row">
        <div class="col-md-8">
            <h4>Student Bill List</h4>
        </div>
    </div>
     <div class="body_block">
                <div class="table-responsive">
                    <table class=" table table-hover">
                        <thead class="thead-dark">                        	
                        <tr>
                            <th>S.N.</th>
                            <th>Bill</th>
                            <th>Fee Type</th>
                            <th>Total</th>
                            <th>Created At</th>
                            <th>Action</th>
                        </tr>
                        </thead>
                        <tbody>

                        <?php $i=1; ?> 
                        @foreach($student->bills as $sb)                           
                                <tr>
                                <td>{{$i}}</td>  
                                <td>{{$sb->id}}</td> 
                                <td>
                                    @foreach($sb->billitems as $bi)
                                    {{$bi->feetype}} - {{$bi->price}}<br> 
                                    @endforeach
                                </td>
                                <td>{{$sb->total}}</td>
                                <td>{{$sb->date}}</td>
                                <td>
                                    <ul class="list-inline">
                                        <li class="list-inline-item">
                                    <a href="{{route('fee/generatefeepdf',['bill'=>encrypt($sb->id),'student_id'=>$student->id])}}" class="btn btn-primary btn-sm"><i class="fa fa-file"> Download Fee Invoice</i></a></li>
                                    <li class="list-inline-item"><a href="{{route('fee/sendfeemail',['bill'=>encrypt($sb->id),'student_id'=>$student->id])}}" class="btn btn-primary btn-sm"><i class="fa fa-file"> Send Mail</i></a></li>
                                </td>

                                </tr>
                                <?php $i++; ?>
                                @endforeach
                        

                        </tbody>
                    </table>
                </div>
            </div>
    @stop
