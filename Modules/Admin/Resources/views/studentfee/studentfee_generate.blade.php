@extends('layouts.admin_dashboard')
@section('content')
{{--    container-fluid already included--}}
    <div class="row">
        <div class="col-md-8">
            <h4>Generate Student Fee</h4>

        </div>
        <div class="col-md-4">
             <!-- <a href="" class="btn btn-primary btn-sm pull-right"><i class="fa fa-file"> Download Fee</i></a> -->
        </div>
    </div>
      <div class="container">
        <div class="row">
            <div class="col-md-8 col-sm-8 col-xs-12 mx-auto">
                <form action="{{route('studentfee/store')}}" method="post" enctype="multipart/form-data">
                    @csrf
                    <input type="hidden" name="grade_id" value="{{$student->grade_id}}">
                     <input type="hidden" name="student_id" value="{{$student->id}}">
                    <!-- <p>Grade:{{$student->grade_id}}</p> -->
                    @include('admin::studentfee.studentfee_form')

                </form>
            </div>
        </div>
    </div>
 

    @stop
