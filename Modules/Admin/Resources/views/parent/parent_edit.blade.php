@extends('layouts.admin_dashboard')


@section('content')
    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="header_content">
                <h2 class="text-left">
                    Parent Edit
                </h2>

            </div>
        </div>
    </div>
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-sm-8 col-xs-12 mx-auto">
                <form action="{{route('parent/editstore',$parent->id)}}" method="post" enctype="multipart/form-data">
                    <!-- {{method_field('PUT')}} -->
                    {!! csrf_field() !!}
                    @include('admin::parent.parent_form')

                </form>
            </div>
        </div>
    </div>

@endsection
