<h5 class="text-muted">Teacher Details</h5>
<hr>
<div class="form-group row">
    <div class="col-md-3 col-sm-4 col-xs-6">
        <label for="name">Name:</label>
    </div>
    <div class="col-md-9 col-sm-8 col-xs-6">
        <input type="text" name="name" id="name" class="form-control" autocomplete="off" value="{{old('name',isset($teacher) ? $teacher->name : null)}}" required>
    </div>
</div>
<div class="form-group row">
    <div class="col-md-3 col-sm-4 col-xs-6">
        <label for="email">Email:</label>
    </div>
    <div class="col-md-9 col-sm-8 col-xs-6">
        <input type="email" name="email" id="email" class="form-control" autocomplete="off" value="{{old('email',isset($teacher) ? $teacher->name : null)}}" required>
    </div>
</div>
<div class="form-group row">
    <div class="col-md-3 col-sm-4 col-xs-6">
        <label for="password">Password:</label>
    </div>
    <div class="col-md-9 col-sm-8 col-xs-6">
        <input type="password" name="password" id="password" class="form-control" autocomplete="off" required>
    </div>
</div>
<div class="form-group row">
    <div class="col-md-3 col-sm-4 col-xs-6">
        <label for="address">Address:</label>
    </div>
    <div class="col-md-9 col-sm-8 col-xs-6">
        <input type="text" name="address" id="address" class="form-control" autocomplete="off" value="{{old('address',isset($teacher) ? $teacher->address : null)}}" required>
    </div>
</div>
<div class="form-group row">
    <div class="col-md-3 col-sm-4 col-xs-6">
        <label for="contact">Contact:</label>
    </div>
    <div class="col-md-9 col-sm-8 col-xs-6">
        <input type="text" name="contact" id="contact" class="form-control" autocomplete="off" value="{{old('contact',isset($teacher) ? $teacher->contact : null)}}" required>
    </div>
</div>
<div class="form-group row">
    <div class="col-md-3 col-sm-4 col-xs-6">
        <label for="image">Image:</label>
    </div>
    <div class="col-md-9 col-sm-8 col-xs-6">
        <input type="file" name="image" id="image" class="form-control" autocomplete="off">
    </div>
</div>
<button type="submit" class="btn btn-success text-white mx-auto">
    <i class="fa fa-save"> Save</i>
</button>
