@extends('layouts.admin_dashboard')

@section('content')

    {{--container-fluid already included--}}

    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="heading_block">
                <div class="row">
                    <div class="col-md-6 col-sm-6 col-xs-12">
                        <h4>Teacher</h4>
                    </div>
                    <div class="col-md-6 col-sm-6 col-xs-12 text-right">
                        <a href="{{route('teacher/create')}}" class="btn btn-primary"><i class="fa fa-plus"></i>Add Teacher</a>
                    </div>
                </div>
            </div>

            <div class="body_block">
                <div class="table-responsive">
                    <table class=" table table-hover">
                        <thead class="thead-dark">
                        <tr>
                            <th>#</th>
                            <th>Name</th>
                            <th>Email</th>
                            <th>Address</th>
                            <th>Contact</th>
                            <th>Image</th>
                            <th></th>
                            <th>Action</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php $i=1; ?>
                        @foreach($teacher_data as $td)
                            <tr>
                                <td>{{$i}}</td>
                                <td>{{$td->name}}</td>
                                <td>{{$td->email}}</td>
                                <td>{{$td->address}}</td>
                                <td>{{$td->contact}}</td>
                                <td>
                                    <img src="{{$td->image ? asset('uploads/profile/'.$td->image) :  asset('back_vendor/dist/img/user2-160x160.jpg') }}" alt="{{$td->image}}" width="50px" height="50px">
                                </td>
                                <td><a href="{{route('teacher/view',$td->id)}}"><span class="label-primary">View as a Teacher</span></a></td>
                                <td>
                            <ul class="list-inline">
                                <li class="list-inline-item">
                                     <a href="{{route('teacher/edit',$td->id)}}"><i class="fa fa-edit"></i></a>
                                </li>
                                <li class="list-inline-item">
                                    <a href="{{route('teacher/delete',$td->id)}}" onclick="return confirm('Want to delete?'); "><i class="fa fa-trash text-danger"></i></a> 
                                </li>                                             
                             </ul>
                                </td>
                            </tr>
                            <?php $i++; ?>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection
