@extends('layouts.admin_dashboard')

@section('content')

    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="header_content">
                <h2 class="text-left">
                    Edit Feetype
                </h2>

            </div>
        </div>
    </div>
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-sm-8 col-xs-12 mx-auto">
                <form action="{{route('feetype/editstore',$feetype->id)}}" method="post">
                    {!! csrf_field() !!}
                    @include('admin::finance.feetype_form')

                </form>
            </div>
        </div>
    </div>
@endsection
