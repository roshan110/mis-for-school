@extends('layouts.admin_dashboard')

@section('content')

    {{--container-fluid already included--}}

    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="heading_block">
                <div class="row">
                    <div class="col-md-6 col-sm-6 col-xs-12">
                        <h4>Fee Type</h4>
                    </div>
                    <div class="col-md-6 col-sm-6 col-xs-12 text-right">
                        <a href="{{route('feetype/add')}}" class="btn btn-primary"><i class="fa fa-plus"></i>Add Fee Type</a>
                    </div>
                </div>
            </div>

           
        </div>
    </div>
     <div class="body_block">
                <div class="table-responsive">
                    <table class=" table table-hover">
                        <thead class="thead-dark">                          
                        <tr>
                            <th>S.N.</th>
                            <th>Feetype</th>
                            <th>Status</th>
                            <th>Action</th>
                        </tr>
                        </thead>
                        <tbody>
                            <?php $i=1; ?>
                            @foreach($feetype_data as $fd)
                            <tr>
                                <td>{{$i}}</td>
                                <td>{{$fd->feetype}}</td>
                                        <td>
                                     @if($fd->status)
                            <a class="text-success" data-toggle="tooltip" data-placement="bottom" title="Click for inactive" href="{{route('feetypes.showhidefeetype',$fd->id)}}"><i class="lnr lnr-checkmark-circle"></i>Active</a>
                            @else
                            <a class="text-danger" data-toggle="tooltip" data-placement="bottom" title="Click for active" href="{{route('feetypes.showhidefeetype',$fd->id)}}"><i class="lnr lnr-cross-circle"></i>InActive</a>
                            @endif

                        </td>
                           <td>
                            <ul class="list-inline">
                                <li class="list-inline-item">
                                     <a href="{{route('feetype/edit',$fd->id)}}"><i class="fa fa-edit"></i></a>
                                </li>
                                <li class="list-inline-item">
                                    <a href="{{route('feetype/delete',$fd->id)}}" onclick="return confirm('Want to delete?'); "><i class="fa fa-trash text-danger"></i></a> 
                                </li>                                             
                             </ul>
                        </td>
                               
                            </tr>
                            <?php $i++; ?>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
@endsection
