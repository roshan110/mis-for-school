@extends('layouts.admin_dashboard')

@section('content')

    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="header_content">
                <h2 class="text-left">
                    Subject Add
                </h2>

            </div>
        </div>
    </div>
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-sm-8 col-xs-12 mx-auto">
                <form action="{{route('subject/store')}}" method="post">
                    @csrf
                    @include('admin::subject.subject_form')

                </form>
            </div>
        </div>
    </div>
@endsection
