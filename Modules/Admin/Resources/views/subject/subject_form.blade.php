<h5 class="text-muted">Subject Details</h5>
<hr>


<div class="form-group row">
    <div class="col-md-3 col-sm-4 col-xs-6">
        <label for="name">Name:</label>
    </div>
    <div class="col-md-9 col-sm-8 col-xs-6">
        <input type="text" name="name" id="name" class="form-control" autocomplete="off">
    </div>
</div>

<div class="form-group row">
    <div class="col-md-3 col-sm-4 col-xs-6">
        <label for="fullmarks">Fullmarks:</label>
    </div>
    <div class="col-md-9 col-sm-8 col-xs-6">
        <input type="number" name="fullmarks" id="fullmarks" class="form-control" autocomplete="off">
    </div>
</div>

<div class="form-group row">
    <div class="col-md-3 col-sm-4 col-xs-6">
        <label for="theorymarks">Theory Marks:</label>
    </div>
    <div class="col-md-9 col-sm-8 col-xs-6">
        <input type="number" name="theorymarks" id="theorymarks" class="form-control" autocomplete="off">
    </div>
</div>

<div class="form-group row">
    <div class="col-md-3 col-sm-4 col-xs-6">
        <label for="practicalmarks">Practical Marks:</label>
    </div>
    <div class="col-md-9 col-sm-8 col-xs-6">
        <input type="number" name="practicalmarks" id="practicalmarks" class="form-control" autocomplete="off">
    </div>
</div>


<div class="form-group row">
    <div class="col-md-3 col-sm-4 col-xs-6">
        <label for="description">Description-(optional):</label>
    </div>
    <div class="col-md-9 col-sm-8 col-xs-6">
        <textarea name="description" id="description" cols="20" rows="5" class="form-control" autocomplete="off"></textarea>
    </div>
</div>
<button type="submit" class="btn btn-success text-white mx-auto">
    <i class="fa fa-save"> Save</i>
</button>
