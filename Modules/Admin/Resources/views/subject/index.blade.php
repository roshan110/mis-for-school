@extends('layouts.admin_dashboard')

@section('content')

    {{--container-fluid already included--}}


    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="heading_block">
                <div class="row">
                    <div class="col-md-6 col-sm-6 col-xs-12">
                        <h4>Subject</h4>
                    </div>
                    <div class="col-md-6 col-sm-6 col-xs-12 text-right">
                        <a href="{{route('subject/create')}}" class="btn btn-primary"><i class="fa fa-plus"></i>Add Subject</a>
                    </div>




                </div>
            </div>

            <div class="body_block">
                <div class="table-responsive">
                    <table class=" table table-hover">
                        <thead class="thead-dark">
                        <tr>
                        <th>#</th>
                        <th>Name</th>
                        <th>Fullmarks</th>
                            <th>Theorymarks</th>
                            <th>Practicalmarks</th>
                            <th>Status</th>
                        <th>Action</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php $i=1; ?>
                        @foreach($subject_data as $sd)
                        <tr>
                            <td>{{$i}}</td>
                            <td>{{$sd->name}}</td>
                            <td>{{$sd->fullmarks}}</td>
                            <td>{{$sd->theory_marks}}</td>
                            <td>{{$sd->practical_marks}}</td>
                            <td>
                                     @if($sd->status)
                            <a class="text-success" data-toggle="tooltip" data-placement="bottom" title="Click for inactive" href="{{route('subjects.showhidesubject',$sd->id)}}"><i class="lnr lnr-checkmark-circle"></i>Active</a>
                            @else
                            <a class="text-danger" data-toggle="tooltip" data-placement="bottom" title="Click for active" href="{{route('subjects.showhidesubject',$sd->id)}}"><i class="lnr lnr-cross-circle"></i>InActive</a>
                            @endif

                        </td>
                            <td>
                                <ul class="list-inline">
                                    <li class="list-inline-item">
                                         <a href="{{route('subject/edit',$sd->id)}}"><i class="fa fa-edit"></i></a>
                                    </li>
                                    <li class="list-inline-item">
                                        <a href="{{route('subject/delete',$sd->id)}}" onclick="return confirm('Want to delete?'); "><i class="fa fa-trash text-danger"></i></a> 
                                    </li>                                             
                                </ul>
                            </td>
                            <?php $i++; ?>
                        </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection