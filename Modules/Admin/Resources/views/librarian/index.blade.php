@extends('layouts.admin_dashboard')
@section('content')
    {{--    container-fluid already included--}}
    <div class="row">
        <div class="col-md-8">
            <h4>Librarian</h4>
        </div>
        <div class="col-md-4 text-right">
            <a href="{{route('librarians.create')}}" class="btn btn-primary">Create</a>
        </div>
    </div>
    <div class="body_block">
        <div class="table-responsive">
            <table class=" table table-hover">
                <thead class="thead-dark">
                <tr>
                    <th>S.N.</th>
                    <th>Image</th>
                    <th>Name</th>
                    <th>Address</th>
                    <th>Contact</th>
                    <th>Status</th>
                    <th></th>
                </tr>
                </thead>
                <tbody>
                <?php $i=1; ?>
               @foreach($librarians as $librarian)
                  <tr>
                      <td>{{$i}}</td>
                      <td>
                      <img src="{{$librarian->image ? asset('uploads/profile/'.$librarian->image) :asset('uploads/profile/user.png') }}" alt="Profile" width="50px" height="50px">
                      </td>
                      <td>{{$librarian->name}}</td>
                      <td>{{$librarian->address}}</td>
                      <td>{{$librarian->contact}}</td>
                      <td>
                          @if ($librarian->status)
                              <a class="text-success" data-toggle="tooltip" data-placement="bottom" title="Click for inactive" href="{{ route('librarians.showhide', $librarian->id) }}">
                                  <i class="lnr lnr-checkmark-circle"></i>
                                  Active
                              </a>
                          @else
                              <a class="text-danger" data-toggle="tooltip" data-placement="bottom" title="Click for active" href="{{ route('librarians.showhide', $librarian->id) }}">
                                  <i class="lnr lnr-cross-circle"></i>
                                  Inactive
                              </a>
                          @endif
                      </td>
                      <td class="text-right">
                          <a class="btn btn-primary btn-sm" href="{{ route('librarians.edit', $librarian->id) }}"><i class="lnr lnr-pencil"></i></a>

                          <div class="pull-right" style="margin-left: 10px;">
                              <form onsubmit="return confirm('Are you sure?')" action="{{ route('librarians.destroy', $librarian->id) }}" method="post">
                                  {{ method_field('DELETE') }}
                                  {{ csrf_field() }}
                                  <button type="submit" class="btn btn-danger btn-sm"><i class="lnr lnr-trash"></i></button>
                              </form>
                          </div>
                      </td>
                  </tr>

                   <?php $i++; ?>
                @endforeach


                </tbody>
            </table>
        </div>
    </div>
@stop
