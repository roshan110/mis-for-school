
<div class="form-group row">
    <div class="col-md-3 col-sm-4 col-xs-6">
        <label for="name">Name:</label>
    </div>
    <div class="col-md-9 col-sm-8 col-xs-6">
        <input type="text" name="name" id="name" class="form-control" autocomplete="off" value="{{old('name',isset($librarian) ? $librarian->name : null)}}">
    </div>
</div>
<div class="form-group row {{isset($librarian) ? 'hidden' : ''}}">
    <div class="col-md-3 col-sm-4 col-xs-6">
        <label for="email">Email:</label>
    </div>
    <div class="col-md-9 col-sm-8 col-xs-6">
        <input type="email" name="email" id="email" class="form-control" autocomplete="off" value="{{old('email',isset($user) ? $user->email : null)}}" {{old('email',isset($user) ? 'readonly' : '')}}>
    </div>
</div>

<div class="form-group row {{isset($librarian) ? 'hidden' : ''}}">
    <div class="col-md-3 col-sm-4 col-xs-6">
        <label for="password">Password:</label>
    </div>
    <div class="col-md-9 col-sm-8 col-xs-6">
        <input type="password" name="password" id="password" class="form-control" autocomplete="off" {{old('password',isset($librarian) ? 'readonly' : '')}}>
    </div>
</div>

<div class="form-group row">
    <div class="col-md-3 col-sm-4 col-xs-6">
        <label for="address">Address:</label>
    </div>
    <div class="col-md-9 col-sm-8 col-xs-6">
        <textarea name="address" id="address" cols="20" rows="5" class="form-control" autocomplete="off">{{old('address',isset($librarian) ? $librarian->address : null)}}</textarea>
    </div>
</div>

<div class="form-group row">
    <div class="col-md-3 col-sm-4 col-xs-6">
        <label for="contact">Contact:</label>
    </div>
    <div class="col-md-9 col-sm-8 col-xs-6">
        <input type="text" name="contact" id="contact" class="form-control" autocomplete="off" value="{{old('contact',isset($librarian) ? $librarian->contact : null)}}">
    </div>
</div>

<div class="form-group row">
    <div class="col-md-3 col-sm-4 col-xs-6">
        <label for="image">Image:</label>
    </div>
    <div class="col-md-9 col-sm-8 col-xs-6">
        <input type="file" name="image" id="image" class="form-control" autocomplete="off">
    </div>
</div>