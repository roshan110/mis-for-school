@extends('layouts.admin_dashboard')
            @section('css')
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.css">
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.18/css/dataTables.bootstrap4.min.css">
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/responsive/2.2.3/css/responsive.bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/responsive/2.2.3/css/responsive.jqueryui.min.css">
@endsection
@section('content')

    {{--container-fluid already included--}}

    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="heading_block">
                <div class="row">
                    <div class="col-md-6 col-sm-6 col-xs-12">
                        <h4>Create Marksheet</h4>
                        <h5 class="text-muted">Marksheet Details<b></b> </h5>
                    </div> 
                     @if(empty($marksheet_status))
                    
                    @else
                      <div class="col-md-6 col-sm-6 col-xs-12">
                        <ul class="list-inline">
                            <li>
                                <a href="{{route('marksheet/generatepdf',['id'=>$student->id,'examtype'=>$exam_name->id,'marksheet_status'=>$marksheet_status->id])}}" class="btn btn-primary btn-sm pull-right"><i class="fa fa-file"> Download Marksheet</i></a>
                            </li>
                            <li>
                                 <a href="{{route('marksheet/sendmarksheetmail',['id'=>$student->id,'examtype'=>$exam_name->id,'marksheet_status'=>$marksheet_status->id])}}" class="btn btn-primary btn-sm pull-right"><i class="fa fa-file"> Send Mail</i></a>
                            </li>
                        </ul>
                        
                    </div>
                    @endif
                                         
                </div>
            </div>
            </div>    
        </div>  
       
          <div class="body_block" style="background-color: white; padding-top: 20px;">
            <div class="container">
                <div class="row">
                    <div class="col-md-6" >
                        <div class="list-group row">
                            <div class="col-md-3 col-sm-4 col-xs-6">
                                <label for="name">Name:</label>
                            </div>
                            <div class="col-md-9 col-sm-8 col-xs-6">
                                 {{$student->name}}
                            </div>
                        </div>
                        <div class="list-group row">
                            <div class="col-md-3 col-sm-4 col-xs-6">
                                <label for="name">Grade:</label>
                            </div>
                            <div class="col-md-9 col-sm-8 col-xs-6">
                                 {{\App\Academicinfo::grade_name($student_details['grade_id'])}}
                            </div>
                        </div>
                        <div class="list-group row">
                            <div class="col-md-3 col-sm-4 col-xs-6">
                                <label for="name">Section:</label>
                            </div>
                            <div class="col-md-9 col-sm-8 col-xs-6">
                                  {{\App\Academicinfo::section_name($student_details['section_id'])}}
                            </div>
                        </div>
                    </div>
                     <div class="col-md-6">
                        <div class="list-group row">
                            <div class="col-md-3 col-sm-4 col-xs-6">
                                <label for="name">Roll No:</label>
                            </div>
                            <div class="col-md-9 col-sm-8 col-xs-6">
                                 {{$student_details['roll_no']}}
                            </div>
                        </div>
                        <div class="list-group row">
                            <div class="col-md-3 col-sm-4 col-xs-6">
                                <label for="name">Exam:</label>
                            </div>
                            <div class="col-md-9 col-sm-8 col-xs-6">
                                 {{$exam_name['examtype']}}
                            </div>
                        </div>
                      
                    </div>
                </div>
            </div>
            </div>
         <div class="body_block mt-5" style="background-color: white; margin-top: 20px;">
                <div class="table-responsive">
                    <table class=" table table-hover">
                        <thead class="thead-dark bg-primary">
                        <tr>
                            <th>S.N</th>
                            <th>Course Name</th>
                            <th>Full Marks</th>
                            <th>Theory Marks</th>                           
                            <th>Practical Marks</th>                            
                            <th>Obtained Theory Marks</th>
                            <th>Obtained Practical Marks</th>
                            <th>Total Obtained Marks</th>
                            <!-- <th></th> -->
                        </tr>

                        </thead>
                        <tbody>
                        <?php $i=1; ?>
                        @foreach($data as $datas)
                            <tr>
                                <td>{{$i}}</td>
                                <td>{{$datas['subject_name']}}</td>
                                <td>{{$datas['fullmarks']}}</td>
                                <td>{{$datas['theory_marks']}}</td>
                                <td>{{$datas['practical_marks']}}</td>
                                <td>{{$datas['obtained_theory_marks']}}</td>
                                <td>{{$datas['obtained_practical_marks']}}</td>
                                <td>{{$datas['total_obtained_marks']}}</td>

                               <!--  <td>
                                    <a href=""><button class="btn btn-xs text-black">Create Marksheet</button></a>
                                </td> -->
                            </tr>
                            
                        <?php $i++; ?>
                        @endforeach

                        <tr>
                            <td  colspan="5" class="text-bold text-center">Total:</td>
                            <td class="text-bold">{{$total_obtained_theory_marks}}</td>
                            <td class="text-bold">{{$total_obtained_practical_marks}}</td>
                            <td class="text-bold">{{$total_obtained_fullmarks}}</td>                                
                        </tr>
                        </tbody>
                    </table>
                </div>
            </div>
            <div class="body_block" style="background-color: white">
                <div class="table-responsive">
                    <table class=" table table-hover">
                        <thead class="thead-dark">
                        <tr>
                            <th>Result: @if($result_value=='Fail') <span class="text-danger">{{$result_value}}</span>@else
                            {{$result_value}}@endif</th>
                            <th class="text-center">Percentage: {{round($percentage,2)}} %</th>
                        </tr>

                        </thead>
                        <tbody>
                      
                        </tbody>
                    </table>
                </div>
            </div><br>
              <div class="">
                <form method="post" action="{{route('marksheet/store')}}">
                    @csrf
                    <input type="hidden" name="school_id" value="{{$student->school_id}}">
                    <input type="hidden" name="grade_id" value="{{$student_details->grade_id}}">
                    <input type="hidden" name="section_id" value="{{$student_details->section_id}}">
                    <input type="hidden" name="student_id" value="{{$student->id}}">
                    <input type="hidden" name="exam_id" value="{{$exam_name->id}}">
                    <input type="hidden" name="total_theory_marks" value="{{$total_obtained_theory_marks}}">
                    <input type="hidden" name="total_practical_marks" value="{{$total_obtained_practical_marks}}">
                    <input type="hidden" name="total_marks" value="{{$total_obtained_fullmarks}}">
                    <input type="hidden" name="percentage" value="{{round($percentage,2)}}">
                    <input type="hidden" name="result" value="{{$result_value}}">
                    @if(empty($marksheet_status))
                    <input type="submit" class="btn btn-primary" value="Save">
                    @endif
                </form>
                    </div>
@endsection



  

