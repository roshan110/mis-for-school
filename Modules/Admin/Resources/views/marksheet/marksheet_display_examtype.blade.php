@extends('layouts.admin_dashboard')

@section('content')

    {{--container-fluid already included--}}

    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="heading_block">
                <div class="row">
                    <div class="col-md-6 col-sm-6 col-xs-12">
                        <h4>Exam Type</h4>
                    </div>
                </div>
            </div>
            <hr>
            <div class="grade">
                <div class="row">
                    @foreach($examtype as $et)
                        <div class="col-md-1">
                            <a href="{{route('marksheet/marksheetgenerate',['id' => encrypt($et['st_id']),'exam_id'=>$et['examtype']])}}" class="btn btn-primary">{{\App\Mark::exam_name($et['examtype'])}}</a>                            
                        </div>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
@endsection
