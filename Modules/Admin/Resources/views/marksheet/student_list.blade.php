@extends('layouts.admin_dashboard')
@section('content')
{{--    container-fluid already included--}}
    <div class="row">
        <div class="col-md-8">
            <h4>Student List</h4>
        </div>
    </div>
     <div class="body_block">
                <div class="table-responsive">
                    <table class=" table table-hover">
                        <thead class="thead-dark">                        	
                        <tr>
                            <th>S.N.</th>
                            <th>Student Name</th>
                            <th>Action</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php $i=1; ?>
                            @foreach($student_data as $std)
                                <tr>
                                <td>{{$i}}</td>
                                <td>{{$std['name']}}</td>
                                <td>
                                    <a href="{{route('marksheet/marksheetdisplay',encrypt($std['id']))}}" class="text-warning"><i class="fa fa-eye"></i></a>
                                </td>

                                </tr>
                                <?php $i++; ?>
                            @endforeach

                        </tbody>
                    </table>
                </div>
            </div>
    @stop
