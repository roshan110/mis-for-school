<!DOCTYPE html>
<html>
<head>
    <title> {{$student->name}},{{\App\Academicinfo::grade_name($student_details['grade_id'])}} - {{$exam_name['examtype']}} :: result</title>
    <style type="text/css">
        table, th, td {
            border: 1px solid black;
            padding: 10px;
            border-collapse: collapse;
}
    </style>
</head>
<body>
    <div class="body_block" style="background-color: white; padding-top: 20px;">
         <h1 style="text-align: center;">{{\App\School::school_name($student ->school_id)}} </h1>
         <p style="text-align: center;">{{\App\School::school_address($student ->school_id)}}</p>
            <div class="container">
                <div class="row">
                    <div class="col-md-6" >
                        <div class="list-group row">
                            <div class="col-md-3 col-sm-4 col-xs-6">
                                <label for="name">Name: {{$student->name}}</label>
                            </div>                           
                                 
                            
                        </div>
                        <div class="list-group row">
                            <div class="col-md-3 col-sm-4 col-xs-6">
                                <label for="name">Grade: {{\App\Academicinfo::grade_name($student_details['grade_id'])}}</label>
                            </div>
                            <div class="col-md-9 col-sm-8 col-xs-6">
                                 
                            </div>
                        </div>
                        <div class="list-group row">
                            <div class="col-md-3 col-sm-4 col-xs-6">
                                <label for="name">Section: {{\App\Academicinfo::section_name($student_details['section_id'])}}</label>
                            </div>
                            <div class="col-md-9 col-sm-8 col-xs-6">
                                  
                            </div>
                        </div>
                    </div>
                     <div class="col-md-6">
                        <div class="list-group row">
                            <div class="col-md-3 col-sm-4 col-xs-6">
                                <label for="name">Roll No: {{$student_details['roll_no']}}</label>
                            </div>
                            <div class="col-md-9 col-sm-8 col-xs-6">
                                
                            </div>
                        </div>
                        <div class="list-group row">
                            <div class="col-md-3 col-sm-4 col-xs-6">
                                <label for="name">Exam: {{$exam_name['examtype']}}</label>
                            </div>
                            <div class="col-md-9 col-sm-8 col-xs-6">
                                 
                            </div>
                        </div>
                      
                    </div>
                </div>
            </div>
    </div>

    <div class="body_block" style="background-color: white;">
                <div class="table-responsive">
                    <table class=" table table-hover">
                        <thead class="thead-dark bg-primary">
                        <tr>
                            <th>S.N</th>
                            <th>Course Name</th>
                            <th>Full Marks</th>
                            <th>Theory Marks</th>                           
                            <th>Practical Marks</th>                            
                            <th>Obtained Theory Marks</th>
                            <th>Obtained Practical Marks</th>
                            <th>Total Obtained Marks</th>
                        </tr>

                        </thead>
                        <tbody>
                        <?php $i=1; ?>
                        @foreach($data as $datas)
                            <tr>
                                <td>{{$i}}</td>
                                <td>{{$datas['subject_name']}}</td>
                                <td>{{$datas['fullmarks']}}</td>
                                <td>{{$datas['theory_marks']}}</td>
                                <td>{{$datas['practical_marks']}}</td>
                                <td>{{$datas['obtained_theory_marks']}}</td>
                                <td>{{$datas['obtained_practical_marks']}}</td>
                                <td>{{$datas['total_obtained_marks']}}</td>

                               <!--  <td>
                                    <a href=""><button class="btn btn-xs text-black">Create Marksheet</button></a>
                                </td> -->
                            </tr>
                            
                        <?php $i++; ?>
                        @endforeach
                         <tr>
                            <td  colspan="5" class="text-bold text-center">Total:</td>
                            <td class="text-bold">{{$marksheet_status->total_theory_marks}}</td>
                            <td class="text-bold">{{$marksheet_status->total_practical_marks}}</td>
                            <td class="text-bold">{{$marksheet_status->total_marks}}</td>                                
                        </tr>
<!--  -->
                        </tbody>
                    </table>
                </div>
    </div>
   
                
                    <p>Result: {{$marksheet_status->result}}</p>
                        <span style="text-align: right;">Percentage: {{$marksheet_status->percentage}} %</span>               
                    </p>
               

</body>
</html>


   