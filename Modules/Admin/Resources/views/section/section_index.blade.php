@extends('layouts.admin_dashboard')
@section('content')
    {{--    container-fluid already included--}}
    <div class="row">
        <div class="col-md-8">
            <h4>Section</h4>
        </div>
        <div class="col-md-4 text-right">
            <a href="{{route('section/create')}}" class="btn btn-primary">Create</a>
            {{--<button class="btn btn-primary"><a href="" class="text-danger">Create</a></button>--}}
        </div>
    </div>
@stop
