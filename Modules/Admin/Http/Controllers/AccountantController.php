<?php

namespace Modules\Admin\Http\Controllers;

use App\Accountant;
use App\Admin;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class AccountantController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        $school_data = Admin::where('admin_id', Auth::user()->id)->select('school_id')->distinct('school_id')->get();
        foreach ($school_data as $sd) {
            $school_id = $sd->school_id;
//            dd($school_id);
        }
        $data['accountants']=Accountant::where('school_id',$school_id)->get();
        return view('admin::accountant.index',$data);
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        return view('admin::accountant.create');
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Response
     */
    public function store(Request $request)
    {
//        $this->validate($request, [
//
//            'name' => 'required|min:3',
//            'image' => 'mimes:jpeg,png',
//        ]);
        $school_data = Admin::where('admin_id', Auth::user()->id)->select('school_id')->distinct('school_id')->get();
        foreach ($school_data as $sd) {
            $school_id = $sd->school_id;
//            dd($school_id);
        }
        if($request!=null) {

            $user= array(
                'name'=>$request->name,
                'email'=>$request->email,
                'password'=>Hash::make($request->password),
            );

            $user_result=User::create($user);

            $accountant = new Accountant();
            $accountant->school_id=$school_id;
            $accountant->user_id=$user_result->id;
            $accountant->name = $request->name;
            $accountant->address = $request->address;
            $accountant->contact = $request->contact;
            $accountant->status = 1;

            $accountant->save();

            if ($request->hasFile('image')) {
                $image_name = 'accountant-' . $accountant->id . '.' . $request->image->extension();
                $path = $request->image->move('uploads/profile/', $image_name);
                $accountant->image = $image_name;
                $accountant->save();
            }
            if($user_result->exists) {

                $role = config('roles.models.role')::where('name', '=', 'Accountant')->first();
                $user_result->attachRole($role);
                return redirect()->route('accountants.index')->with('success', 'User Accountant successfully added.');
            }
            else
            {
                return redirect()->route('accountants.index')->with('error', 'Error while adding user Accountant.');
            }

        }
    }


    /**
     * Show the specified resource.
     * @param int $id
     * @return Response
     */
    public function show($id)
    {
        return view('admin::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Response
     */
    public function edit(Accountant $accountant,User $user)
    {
        return view('admin::accountant.edit',compact('accountant','user'));
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Response
     */
    public function update(Request $request, Accountant $accountant)
    {
        $accountant->name = $request->name;
        $accountant->address = $request->address;
        $accountant->contact = $request->contact;
        $accountant->status = 1;

        $accountant->save();




        if ($request->hasFile('image')) {
            if($accountant->image && file_exists(public_path('uploads/profile/'.$accountant->image))){
                unlink(public_path('uploads/profile/'.$accountant->image));
            }
            $image_name = 'accountant-' . $accountant->id . '.' . $request->image->extension();
            $path = $request->image->move('uploads/profile/', $image_name);
            $accountant->image = $image_name;
            $accountant->save();
        }

        return redirect()->route('accountants.index')->with('success', 'Accountant successfully saved.');
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Response
     */
    public function destroy(Accountant $accountant)
    {
        if($accountant){
            if($accountant->image){
                $image = public_path('uploads/profile/' . $accountant->image);
                if(file_exists($image)){
                    unlink($image);
                }
            }

            $accountant->delete();

            $user_data=User::find($accountant->user_id)->delete();
            return redirect()->route('accountants.index')->with('success', 'User Accountant deleted.');
        }else{
            return redirect()->route('accountants.index')->with('error', 'Error while deleting user accountant.');
        }
    }
    public function showhide(Accountant $accountant)
    {
        if($accountant->status){
            $accountant->status = 0;
        }else{
            $accountant->status = 1;
        }
        $accountant->save();
        return redirect()->route('accountants.index')->with('success', 'Status Updated.');
    }
}
