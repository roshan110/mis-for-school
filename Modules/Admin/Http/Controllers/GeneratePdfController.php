<?php

namespace Modules\Admin\Http\Controllers;
use App\Student;
use App\Marksheet;
use App\Academicinfo;
use App\Exam;
use App\Mark;
use Dompdf\Dompdf;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\DB;

class GeneratePdfController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        return view('admin::index');
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        return view('admin::create');
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Response
     */
    public function show($id)
    {
        return view('admin::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Response
     */
    public function edit($id)
    {
        return view('admin::edit');
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Response
     */
    public function destroy($id)
    {
        //
    }
    public function generatepdf($id,$examtype,Marksheet $marksheet)
    {
        $student=Student::find($id);
        $student_details=Academicinfo::where('s_id',$id)->latest()->first();
        
        $exam_name=Exam::find($examtype);
        // dd($exam_name);

        // dd($student_details);
        // $grade_data=Grade::where('id',$student_details->grade_id)->first();
        // $section_data=Section::where('id',$student_details_data->section_id)->first();
//        dd($student);
        $subjects=DB::table('subjects')->join('grade_subject','grade_subject.subject_id','=','subjects.id')->where('grade_subject.grade_id','=',$student->grade_id)->get();
//       dd($subjects);
//        dd($student->grades->subjects);
        $marks=Mark::where('st_id',$id)->where('examtype',$examtype)->get();
//        dd($student);
        $total_fullmarks=0;
        $i=0;       
        $data=$student->grades->subjects->map(function($subject) use($id,$examtype,$total_fullmarks)
        {
            $mark=Mark::where('st_id',$id)->where('examtype',$examtype)->where('subject_id',$subject->id)->first();
//            dd($mark);
            $data_marks=array(
                'fullmarks'=>$subject->fullmarks,
                'subject_name'=>$subject->name,
                'theory_marks'=>$subject->theory_marks,
                'practical_marks'=>$subject->practical_marks,
                'theory_passmarks'=>((40 / 100) * ($subject->theory_marks)),
                'practical_passmarks'=>((40 / 100) * ($subject->practical_marks)),
                
                // 'totals_fullmark'=>$subject->fullmarks+$total_fullmarks,
            );            
            if($mark)
            {
                $data_marks += array(

                    'obtained_theory_marks'=>$mark->marks,
                    'obtained_practical_marks'=>$mark->practical_marks,
                    'total_obtained_marks'=>$total_fullmarks+($mark->marks)+($mark->practical_marks),
                );
            }
            else{
                $data_marks += array(

                    'obtained_theory_marks'=>0,
                    'obtained_practical_marks'=>0,
                    'total_obtained_marks'=>$total_fullmarks,
                );
            }            
             return $data_marks;
             // $total_marks=$total_fullmarks_+($fullsmarks[$i]);
        });
       $marksheet_status=Marksheet::where('student_id',$student->id)->where('exam_id',$examtype)->where('grade_id',$student_details->grade_id)->where('section_id',$student_details->section_id)->first();
         // ------*save status*---------
        
        $view= view('admin::marksheet.pdfview', compact('student','student_details','exam_name','data','total_obtained_theory_marks','marksheet_status'));
        $dompdf = new Dompdf();
        $dompdf->loadHtml("$view");
        $dompdf->render();
        $dompdf->stream($student->id . ".pdf", array("Attachment" => false));
    }
    public function generatefeepdf()
    {
        bill=Bill::find(decrypt($bill));
        $student_details=Academicinfo::where('s_id',$student->id)->latest()->first();        
        $view= view('admin::studentfee.feepdfview',compact('bill','student','student_details'));

        $dompdf = new Dompdf();
        $dompdf->loadHtml("$view");
        $dompdf->render();
        $dompdf->stream($student->id . ".pdf", array("Attachment" => false));
    }
}
