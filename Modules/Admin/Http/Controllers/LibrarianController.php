<?php

namespace Modules\Admin\Http\Controllers;

use App\Admin;
use App\Librarian;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class LibrarianController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        $school_data = Admin::where('admin_id', Auth::user()->id)->select('school_id')->distinct('school_id')->get();
        foreach ($school_data as $sd) {
            $school_id = $sd->school_id;
//            dd($school_id);
        }
        $data['librarians']=Librarian::where('school_id',$school_id)->get();
        return view('admin::librarian.index',$data);

    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        return view('admin::librarian.create');
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        $school_data = Admin::where('admin_id', Auth::user()->id)->select('school_id')->distinct('school_id')->get();
        foreach ($school_data as $sd) {
            $school_id = $sd->school_id;
//            dd($school_id);
        }
        if($request!=null) {

            $user= array(
                'name'=>$request->name,
                'email'=>$request->email,
                'password'=>Hash::make($request->password),
            );

            $user_result=User::create($user);

            $librarian = new Librarian();
            $librarian->school_id=$school_id;
            $librarian->user_id=$user_result->id;
            $librarian->name = $request->name;
            $librarian->address = $request->address;
            $librarian->contact = $request->contact;
            $librarian->status = 1;

            $librarian->save();

            if ($request->hasFile('image')) {
                $image_name = 'librarian-' . $librarian->id . '.' . $request->image->extension();
                $path = $request->image->move('uploads/profile/', $image_name);
                $librarian->image = $image_name;
                $librarian->save();
            }
            if($user_result->exists) {

                $role = config('roles.models.role')::where('name', '=', 'Librarian')->first();
                $user_result->attachRole($role);
                return redirect()->route('librarians.index')->with('success', 'User Librarian successfully added.');
            }
            else
            {
                return redirect()->route('librarians.index')->with('error', 'Error while adding user Librarian.');
            }

        }
    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Response
     */
    public function show($id)
    {
        return view('admin::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Response
     */
    public function edit(Librarian $librarian)
    {
        return view('admin::librarian.edit',compact('librarian'));

    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Response
     */
    public function update(Request $request, Librarian $librarian)
    {
        $librarian->name = $request->name;
        $librarian->address = $request->address;
        $librarian->contact = $request->contact;
        $librarian->status = 1;

        $librarian->save();




        if ($request->hasFile('image')) {
            if($librarian->image && file_exists(public_path('uploads/profile/'.$librarian->image))){
                unlink(public_path('uploads/profile/'.$librarian->image));
            }
            $image_name = 'librarian-' . $librarian->id . '.' . $request->image->extension();
            $path = $request->image->move('uploads/profile/', $image_name);
            $librarian->image = $image_name;
            $librarian->save();
        }

        return redirect()->route('librarians.index')->with('success', 'Librabrian successfully saved.');
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Response
     */
    public function destroy(Librarian $librarian)
    {
        if($librarian){
            if($librarian->image){
                $image = public_path('uploads/profile/' . $librarian->image);
                if(file_exists($image)){
                    unlink($image);
                }
            }

            $librarian->delete();

            $user_data=User::find($librarian->user_id)->delete();
            return redirect()->route('librarians.index')->with('success', 'User Librarian deleted.');
        }else{
            return redirect()->route('librarians.index')->with('error', 'Error while deleting user librarian.');
        }
    }
    public function showhide(Librarian $librarian)
    {
        if($librarian->status){
            $librarian->status = 0;
        }else{
            $librarian->status = 1;
        }
        $librarian->save();
        return redirect()->route('librarians.index')->with('success', 'Status Updated.');
    }
}
