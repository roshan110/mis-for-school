<?php

namespace Modules\Admin\Http\Controllers;

use App\Admin;
use App\Grade;
use App\Feetype;
use App\Gradefee;
use App\Student;
use App\Bill;
use App\Mail\FeeDetails;
use App\BillItem;
use App\Academicinfo;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Mail;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Auth;

class FinancialController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        return view('admin::index');
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        return view('admin::create');
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Response
     */
    public function show($id)
    {
        return view('admin::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Response
     */
    public function edit($id)
    {
        return view('admin::edit');
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Response
     */
    public function destroy($id)
    {
        //
    }
    public function feetypeindex()
    {
        $school_data=Admin::where('admin_id',Auth::user()->id)->select('school_id')->distinct('school_id')->get();
            foreach ($school_data as $sd)
            {
                $school_id=$sd->school_id;
//            dd($school_id);
            }
            $feetype_data=Feetype::where('school_id',$school_id)->get();
        return view('admin::finance.index',['feetype_data'=>$feetype_data]);
    }
    public function feetypeadd()
    {
        return view('admin::finance.feetype_add');
    }
    public function feetypestore(Request $request)
    {
        $school_data=Admin::where('admin_id',Auth::user()->id)->select('school_id')->distinct('school_id')->get();
        foreach ($school_data as $sd)
        {
            $school_id=$sd->school_id;
        }
        $data=new Feetype();
        $data->school_id=$school_id;
        $data->feetype=$request->name;
        $data->description=$request->description;
        $data->status=1;
        $data->save();
        return redirect()->route('feetype/index')->with('success','Feetype added ');
    }
    public function showhidefeetype(Feetype $feetype)
    {
        if($feetype->status)
        {
            $feetype->status=0;
        }
        else
        {
            $feetype->status=1;
        }
        $feetype->save();
        return redirect()->route('feetype/index')->with('success','Status Updated');
    }
    public function editfeetype(Feetype $feetype)
    {
        return view('admin::finance.feetype_edit',compact('feetype'));
    }
    public function editstorefeetype(Request $request,Feetype $feetype)
    {
        $edit=$feetype->update(['feetype'=>$request->name]);
        return redirect()->route('feetype/index')->with('status','Feetype succesfully edited');
    }
    public function deletefeetype(Feetype $feetype)
    {
        $delete=$feetype->delete();
        return redirect()->route('feetype/index')->with('status','Feetype succesfully deleted');
    }
    public function gradefeeindex()
    {
        return view('admin::gradefee.index');
    }
    public function gradefeedetails()
    {
        $school_data=Admin::where('admin_id',Auth::user()->id)->select('school_id')->distinct('school_id')->get();
        foreach ($school_data as $sd)
        {
            $school_id=$sd->school_id;
//            dd($school_id);
        }
        $grade_data=Grade::where('school_id',$school_id)->get();
        return view('admin::gradefee.gradefee_class',['grade_data'=>$grade_data]);
    }
    public function gradefeeadd(Grade $grade)
    {
        $school_data=Admin::where('admin_id',Auth::user()->id)->select('school_id')->distinct('school_id')->get();
        foreach ($school_data as $sd)
        {
            $school_id=$sd->school_id;
//            dd($school_id);
        }      

        $feetype=Feetype::where('school_id',$school_id)->get();
        $grade_fee=Gradefee::where('grade_id',$grade->id);
        // dd($feetype);
        return view('admin::gradefee.gradefee_add',['grade'=>$grade,'feetype'=>$feetype]);
    }
    public function gradefeestore(Request $request)
    {
        if($request!=null)
        {
            $data=$request->all();
            // dd($data);
            for ($i=0; $i < count($data['feetype']); $i++) { 
                $input = [
                    'grade_id'=>$data['grade_id'],
                    'type_id'=>$data['feetype'][$i],
                    'price'=>$data['gradefee'][$i],
                    'status'=>1,
                ];
                $result=Gradefee::create($input);
            }
            // $data=new Gradefee();
            // $data->grade_id=$request->grade_id;
            // $data->status=1;
            // if($request->feetype)
            // {
            //     $data->type_id=Feetype::find($request->feetype);     
            // }
            // if ($request->gradefee) {
            //     $data->price=$request->gradefee;
            // }
            // $data->save();
            return redirect()->route('gradefee/index')->with('success','GradeFee succesfully added');
        }
    }
    public function studentfeeindex()
    {
        return view('admin::studentfee.index');
    }
    public function studentfeegrade()
    {
        $school_data=Admin::where('admin_id',Auth::user()->id)->select('school_id')->distinct('school_id')->get();
        foreach ($school_data as $sd)
        {
            $school_id=$sd->school_id;
//            dd($school_id);
        }
        $grade_data=Grade::where('school_id',$school_id)->get();
        // dd($grade_data);
        return view('admin::studentfee.student_grade',['grade_data'=>$grade_data]);
    }
    public function studentfeelist($id)
    {   
        $school_data=Admin::where('admin_id',Auth::user()->id)->select('school_id')->distinct('school_id')->get();
        foreach ($school_data as $sd)
        {
            $school_id=$sd->school_id;
        }
        $student_data=Student::where('grade_id',$id)->get();
        // dd($student_data);
        return view('admin::studentfee.studentfee_list',compact('student_data'));
    }
    public function studentfeegenerate(Student $student)
    {  
        $school_data=Admin::where('admin_id',Auth::user()->id)->select('school_id')->distinct('school_id')->get();
        foreach ($school_data as $sd)
        {
            $school_id=$sd->school_id;
        }      
        $feetype=Gradefee::where('grade_id',$student->grade_id)->get();
        return view('admin::studentfee.studentfee_generate',['feetype'=>$feetype,'student'=>$student]);
    }
    public function studentfeestore(Request $request)
    {   
        // $totaltype=array();
        if($request!=null)
        {
            $datas=$request->all();
            // dd($datas);
            for ($i=0; $i < count($datas['feetype']); $i++) {
                    $total_type=Gradefee::where('grade_id',$datas['grade_id'])->where('type_id',$datas['feetype'][$i])->first();
                    $totaltype[]=$total_type->price;    
                
              
            }
            $total_fee_sum=array_sum($totaltype);
            // dd($total_fee_sum);
            

            
            $data=new Bill();
            // $data->grade_id=$request->grade_id;
            $data->student_id=$datas['student_id'];
            $data->status=1; 
            $data->date=Carbon::now()->format('Y-m-d');
            $data->total=$total_fee_sum;   
            // dd($data);       
            $bill_result=$data->save();
            // dd($bill_result);

            if($bill_result)
            {
                for ($i=0; $i < count($datas['feetype']); $i++) {
                    $total_type=Gradefee::where('grade_id',$datas['grade_id'])->where('type_id',$datas['feetype'][$i])->first();
                    $fee_type_data=Feetype::find($datas['feetype'][$i]);

                    $bill_item=[
                        'bill_id'=>$data->id,
                        'feetype'=>$fee_type_data->feetype,
                        'price'=>$total_type->price,
                        'status'=>1,
                    ];

                    $bill_item_result=BillItem::create($bill_item);

                }
            }
            if($bill_result && $bill_item_result==true)
            {
            return redirect()->route('gradefee/index')->with('success','Data succesfully added');
            }
            else
            {
                return redirect()->route('gradefee/index')->with('error','Something went wrong'); 
            }
        }
    }
     public function studentfeeview(Student $student)
    {
        return view('admin::studentfee.studentfeeview',compact('student'));
    }
    public function sendfeemail($bill,Student $student)
    {
        $bill=Bill::find(decrypt($bill));
        $student_details=Academicinfo::where('s_id',$student->id)->latest()->first(); 
        Mail::send(new FeeDetails($student,$bill,$student_details));
        return redirect()->back();
    }
}
