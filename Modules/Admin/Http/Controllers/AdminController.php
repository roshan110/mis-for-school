<?php

namespace Modules\Admin\Http\Controllers;

use App\Admin;
use App\Attendance;
use App\Grade;
use App\School;
use App\Parents;
use App\Student;
use App\Exam;
use App\Mark;
use App\Mail\MarksheetDetails;
use App\Marksheet;
use App\Section;
use App\Subject;
use App\Teacher;
use App\User;
use App\Academicinfo;
use Carbon\Carbon;
use PDF;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Mail;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\DB;
use jeremykenedy\LaravelRoles\Traits\HasRoleAndPermission;

class AdminController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Response
     */
    use HasRoleAndPermission;

    public function __construct()
    {
        $this->middleware('role:admin');

    }
    public function index()
    {
        return view('admin::index');
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        return view('admin::create');
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Response
     */
    public function show($id)
    {
        return view('admin::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Response
     */
    public function edit($id)
    {
        return view('admin::edit');
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Response
     */
    public function destroy($id)
    {
        //
    }
    public function grade()
    {
        $school_data=Admin::where('admin_id',Auth::user()->id)->select('school_id')->distinct('school_id')->get();
            foreach ($school_data as $sd)
            {
                $school_id=$sd->school_id;
//            dd($school_id);
            }
            $grade_data=Grade::where('school_id',$school_id)->get();
            return view('admin::grade.grade_index',['grade_data'=>$grade_data]);
    }
    public function gradecreate()
    {
        $school_data=Admin::where('admin_id',Auth::user()->id)->select('school_id')->distinct('school_id')->get();
        foreach ($school_data as $sd)
        {
            $school_id=$sd->school_id;
//            dd($school_id);
        }
        $data['subjects']=Subject::where('school_id',$school_id)->get();
        return view('admin::grade.grade_add',$data);

    }
    public function storegrade(Request $request)
    {
//        dd($request->subject);
        $school_data=Admin::where('admin_id',Auth::user()->id)->select('school_id')->distinct('school_id')->get();
        foreach ($school_data as $sd)
        {
            $school_id=$sd->school_id;
//            dd($school_id);
        }
//        dd($school_data);
        $data=new Grade();
        $data->school_id=$school_id;
        $data->name=$request->name;
        $data->description=$request->description;
        $data->status=1;
        $data->save();
        if($request->subject)
        {
            $subject=Subject::find($request->subject);
            $data->subjects()->sync($subject);
        }


//         $data->subjects()->attach($request->subject);

        // $data->subjects()->detach($request->subject);


        return redirect()->route('grade/index');
    }
    public function editgrade(Grade $grade)
    {
        $school_data=Admin::where('admin_id',Auth::user()->id)->select('school_id')->distinct('school_id')->get();
        foreach ($school_data as $sd)
        {
            $school_id=$sd->school_id;
//            dd($school_id);
        }
        $subjects=Subject::where('school_id',$school_id)->get();
        return view('admin::grade.grade_edit',compact('grade','subjects'));
    }
    public function editstoregrade(Request $request,Grade $grade)
    {
//        dd($request);
        $edit=$grade->update(['name'=>$request->name,'description'=>$request->description]);
        if($request->subject)
        {
            $subjects=Subject::find($request->subject);
            $grade->subjects()->sync($subjects);
        }
        else
        {
            $grade->subjects()->detach();
        }

        return redirect()->route('grade/index')->with('status','Grade succesfully edited');
    }
    public function deletegrade(Grade $grade)
    {
        $delete=$grade->delete();
        $grade->subjects()->detach();
        return redirect()->route('grade/index')->with('status','Grade succesfully deleted');
    }
    public function showhidegrade(Grade $grade)
    {
        if($grade->status)
        {
            $grade->status=0;
        }
        else
        {
            $grade->status=1;
        }
        $grade->save();
        return redirect()->route('grade/index')->with('success','Status Updated');
    }
    public function section()
    {
        return view('admin::section.section_index');
    }
    public function sectioncreate()
    {
        $school_data=Admin::where('admin_id',Auth::user()->id)->select('school_id')->distinct('school_id')->get();
        foreach ($school_data as $sd)
        {
            $school_id=$sd->school_id;
//            dd($school_id);
        }
        $grade_data=Grade::where('school_id',$school_id)->get();
//        dd($grade_data);
        return view('admin::section.section_add',['grade_data'=>$grade_data]);
    }
    public function storesection(Request $request)
    {

        $data=new Section();
        $data->grade_id=$request->grade;
        $data->name=$request->name;
        $data->description=$request->description;
        $data->status=1;
        $data->save();
        return redirect()->route('section/index');
    }
    public function editsection(Section $section)
    {
        return view('admin::section.section_edit',compact('section'));
    }
    public function editstoresection(Request $request,Section $section)
    {
        $edit=$section->update(['name'=>$request->name]);
        return redirect()->route('section/index')->with('status','Section succesfully edited');
    }
    public function deletesection(Section $section)
    {
        $delete=$parent->delete();
        return redirect()->route('section/index')->with('status','Parent succesfully deleted');
    }

//    Subject

        public function subject()
        {
            $school_data=Admin::where('admin_id',Auth::user()->id)->select('school_id')->distinct('school_id')->get();
            foreach ($school_data as $sd)
            {
                $school_id=$sd->school_id;
//            dd($school_id);
            }
            $subject_data=Subject::where('school_id',$school_id)->get();
            return view('admin::subject.index',['subject_data'=>$subject_data]);
        }
    public function subjectcreate()
    {
        return view('admin::subject.create');
    }

    public function subjectstore(Request $request)
    {

        $school_data=Admin::where('admin_id',Auth::user()->id)->select('school_id')->distinct('school_id')->get();
        foreach ($school_data as $sd)
        {
            $school_id=$sd->school_id;
//            dd($school_id);
        }
        if($request !=null)
        {


        $data=new Subject();
        $data->school_id=$school_id;
        $data->name=$request->name;
        $data->fullmarks=$request->fullmarks;
        $data->theory_marks=$request->theorymarks;
        $data->practical_marks=$request->practicalmarks;
        $data->status=1;

            $data->description=$request->description;
            if(($data->theory_marks + $data->practical_marks)==$data->fullmarks)
            {
                $result=$data->save();

                if($result==true)
                {
                    return redirect()->route('subject/index')->with('success','Subject added successfully');
                }
                else{
                    return redirect()->route('subject/index')->with('error','Something went wrong');
                }
            }
            else{
                return redirect()->route('subject/index')->with('error','Sum to theory marks and practical marks must be equal to fullmarks.');
            }



        }
        else
        {
            return redirect()->route('subject/index')->with('error','Something went wrong');
        }
    }
     public function editsubject(Subject $subject)
    {
        return view('admin::subject.subject_edit',compact('subject'));
    }
    public function editstoresubject(Request $request,Subject $subject)
    {
        $edit=$subject->update(['name'=>$request->name,'fullmarks'=>$request->fullmarks,'practical_marks'=>$request->practicalmarks,'theory_marks'=>$request->theorymarks]);
        return redirect()->route('subject/index')->with('status','Subject succesfully edited');
    }
    public function deletesubject(Subject $subject)
    {
        $delete=$subject->delete();
        return redirect()->route('subject/index')->with('status','Subject succesfully deleted');
    }
    public function showhidesubject(Subject $subject)
    {
        if($subject->status)
        {
            $subject->status=0;
        }
        else
        {
            $subject->status=1;
        }
        $subject->save();
        return redirect()->route('subject/index')->with('success','Status Updated');
    }
//    Student
    public function student()
    {
        $school_data=Admin::where('admin_id',Auth::user()->id)->select('school_id')->distinct('school_id')->get();
        foreach ($school_data as $sd)
        {
            $school_id=$sd->school_id;
//            dd($school_id);
        }
        $student_data=Student::where('school_id',$school_id)->get();
        return view('admin::student.index',['student_data'=>$student_data]);
//       ,['student_data'=>$student_data]
    }
    public function studentcreate()
    {
        $school_data=Admin::where('admin_id',Auth::user()->id)->select('school_id')->distinct('school_id')->get();
        foreach ($school_data as $sd)
        {
            $school_id=$sd->school_id;
//            dd($school_id);
        }
        $parent_data=Parents::where('school_id',$school_id)->get();
        $grade_data=Grade::where('school_id',$school_id)->get();
        return view('admin::student.student_add',['parent_data'=>$parent_data,'grade_data'=>$grade_data]);
    }

    public function studentstore(Request $request)
    {
        $school_data = Admin::where('admin_id', Auth::user()->id)->select('school_id')->distinct('school_id')->get();
        foreach ($school_data as $sd) {
            $school_id = $sd->school_id;
//            dd($school_id);
        }
        if ($request != null) {
            $data = new Student();
            $data->school_id = $school_id;
            $data->parent_id=$request->parentid;
            $data->grade_id=$request->grade;
            $data->section_id=$request->section;
            $data->name = $request->name;
            $data->email = $request->email;
            $data->address = $request->address;
            $data->contact = $request->contact;
            $data->date_of_birth=$request->dob;
            $data->gender=$request->gender;
            $data->religion=$request->religion;
            $data->status=1;
           // 
//            $data->save();
            $result = $data->save();
//            dd($result);
            if ($request->hasFile('image')) {
                $image_name =$request->name . '-'.$request->contact . '.'.$request->image->extension();
                $path = $request->image->move('uploads/profile/', $image_name);
                $data->image = $image_name;
                $data->save();
            }

            $student_info= array(
            's_id'=>$data->id,
            'registration_no'=>$data->id,
            'academic_year'=>Carbon::now()->format('Y'),
            'grade_id'=>$request->grade,
            'section_id'=>$request->section,
            'roll_no'=>$request->rollno,        
            'status'=>1,
           
        
        );
 $student_result=Academicinfo::create($student_info);
            if (($result &&  $student_result ) == true) {
                return redirect()->route('student/index')->with('success', 'Student added successfully');
            } else {
                return redirect()->route('student/index')->with('error', 'Something went wrong');
            }

        } else {
            return redirect()->route('student/index')->with('error', 'Something went wrong');
        }
    }
    public function editstudent(Student $student)
    {

        $school_data=Admin::where('admin_id',Auth::user()->id)->select('school_id')->distinct('school_id')->get();
        foreach ($school_data as $sd)
        {
            $school_id=$sd->school_id;
//            dd($school_id);
        }
        $parent_data=Parents::where('school_id',$school_id)->get();
        $grade_data=Grade::where('school_id',$school_id)->get();
        return view('admin::student.student_edit',['parent_data'=>$parent_data,'grade_data'=>$grade_data],compact('student'));
    }
    public function editstorestudent(Request $request,Student $student)
    {
        $edit=$student->update(['name'=>$request->name,'email'=>$request->email,'address'=>$request->address,'contact'=>$request->contact]);
        if ($request->hasFile('image')) {
            if($student->image && file_exists(public_path('uploads/profile/'.$student->image))){
                unlink(public_path('uploads/profile/'.$student->image));
            }
            $image_name = $student->name . '-'.$student->contact . '.'.$request->image->extension();
            $path = $request->image->move('uploads/profile/', $image_name);
            $student->image = $image_name;
            $student->save();
        }
        return redirect()->route('student/index')->with('success','Student succesfully edited');
    }
    public function deletestudent(Student $student)
    {
        $delete=$student->delete();
        return redirect()->route('student/index')->with('error','Student succesfully deleted');
    }
    public function viewstudent(Student $student)
    {
       // $parent_id=Student::where('parent_id',$student->parent_id)->first();  
       // dd($parent_id);  
       $parent_data=Parents::where('id',$student->parent_id)->first();
     
        $academic_data=Academicinfo::where('s_id',$student->id)->latest()->take(1)->first();
          $grade_data=Grade::where('id',$academic_data->grade_id)->first();
       $section_data=Section::where('id',$academic_data->section_id)->first();
       $examtype=Marksheet::where('student_id',$student->id)->groupBy('exam_id')->get();
       $school_data=Admin::where('admin_id',Auth::user()->id)->select('school_id')->distinct('school_id')->get();
      
        foreach ($school_data as $sd)
        {
            $school_id=$sd->school_id;
//            dd($school_id);
        }
        $grades_data=Grade::where('school_id',$school_id)->get();

        // dd($academic_data);
       // dd($parent_data);
        return view('admin::student.student_view',compact('student','parent_data','grade_data','section_data','academic_data','grades_data','examtype','marksheet_data'));
    }
   
    public function academicinfostore(Academicinfo $academicinfo,Request $request)
    {
        $student_data=Student::find($academicinfo->s_id);
        $student_data->grade_id=$request->grades;
        $student_data->section_id=$request->section;
        // dd($student_data);
        $student_data->save();
        // dd($student_data);
        $data=new Academicinfo();
        $data->registration_no=$academicinfo->s_id;
        $data->grade_id=$request->grades;
        $data->section_id=$request->section;
        $data->academic_year=$request->academicyear;
        $data->roll_no=$request->rollno;
        $data->s_id=$academicinfo->s_id;
        $data->status=1;
        // dd($data);
        $data->save(); 
        return redirect()->route('student/view',['student'=>$academicinfo->s_id]); 
    }
    public function showhidestudent(Student $student)
    {
        if($student->status)
        {
            $student->status=0;
        }
        else
        {
            $student->status=1;
        }
        $student->save();
        return redirect()->route('student/index')->with('success','Status Updated');
    }
    public function academicinfoedit()
    {
        return view('admin::student.student_academicinfo');
    }

//    Section Fetch
    public function fetchsection(Request $request)
    {
        $select = $request->get('select');
        $value = $request->get('value');
        $dependent = $request->get('dependent');
        $data = Section::where('grade_id', $value)
            ->get();
        $output = '<option value="">Select '.ucfirst('Section').'</option>';
        foreach($data as $row)
        {
            $output .= '<option value="'.$row->id.'">'.$row->name.'</option>';
        }
        echo $output;
    }

//    Parent
    public function parent()
    {
        $school_data=Admin::where('admin_id',Auth::user()->id)->select('school_id')->distinct('school_id')->get();
        foreach ($school_data as $sd)
        {
            $school_id=$sd->school_id;
//            dd($school_id);
        }
        $parent_data=Parents::where('school_id',$school_id)->get();
        return view('admin::parent.index',['parent_data'=>$parent_data]);
//
    }
    public function parentcreate()
    {
        return view('admin::parent.parent_add');
    }

    public function parentstore(Request $request)
    {
        $school_data = Admin::where('admin_id', Auth::user()->id)->select('school_id')->distinct('school_id')->get();
        foreach ($school_data as $sd) {
            $school_id = $sd->school_id;
//            dd($school_id);
        }
        if ($request != null) {
            $data = new Parents();
            $data->school_id = $school_id;
            $data->name = $request->name;
            $data->email = $request->email;
            $data->address = $request->address;
            $data->contact = $request->contact;
            $data->status=1;
            $result = $data->save();
//            dd($result);
            if ($request->hasFile('image')) {
                $image_name =$request->name . '-'.$request->contact . '.'.$request->image->extension();
                $path = $request->image->move('uploads/profile/', $image_name);
                $data->image = $image_name;
                $data->save();
            }
            if (($result) == true) {
                return redirect()->route('parent/index')->with('success', 'Parent added successfully');
            } else {
                return redirect()->route('parent/index')->with('error', 'Something went wrong');
            }

        } else {
            return redirect()->route('parent/index')->with('error', 'Something went wrong');
        }
    }
    public function editparent(Parents $parent)
    {
        return view('admin::parent.parent_edit',compact('parent'));
    }
    public function editstoreparent(Request $request,Parents $parent)
    {
        $edit=$parent->update(['name'=>$request->name,'email'=>$request->email,'address'=>$request->address,'contact'=>$request->contact]);
        if ($request->hasFile('image')) {
            if($parent->image && file_exists(public_path('uploads/profile/'.$parent->image))){
                unlink(public_path('uploads/profile/'.$parent->image));
            }
            $image_name = $parent->name . '-'.$parent->contact . '.'.$request->image->extension();
            $path = $request->image->move('uploads/profile/', $image_name);
            $parent->image = $image_name;
            $parent->save();
        }
        return redirect()->route('parent/index')->with('success','Parent succesfully edited');
    }
    public function deleteparent(Parents $parent)
    {
        $delete=$parent->delete();
        return redirect()->route('parent/index')->with('error','Parent succesfully deleted');
    }
    public function showhideparent(Parents $parent)
    {
        if($parent->status)
        {
            $parent->status=0;
        }
        else
        {
            $parent->status=1;
        }
        $parent->save();
        return redirect()->route('parent/index')->with('success','Status Updated');
    }
//    Teacher
    public function teacher()
    {
        $school_data=Admin::where('admin_id',Auth::user()->id)->select('school_id')->distinct('school_id')->get();
        foreach ($school_data as $sd)
        {
            $school_id=$sd->school_id;
        }
        $teacher_data=Teacher::where('school_id',$school_id)->get();
        return view('admin::teacher.index',['teacher_data'=>$teacher_data]);
    }
    public function teachercreate()
    {
        return view('admin::teacher.teacher_add');
    }
    public function teacherstore(Request $request)
    {
        $school_data=Admin::where('admin_id',Auth::user()->id)->select('school_id')->distinct('school_id')->get();
        foreach ($school_data as $sd)
        {
            $school_id=$sd->school_id;
        }


        $user= array(
            'name'=>$request->name,
            'email'=>$request->email,
            'password'=>Hash::make($request->password),
        );

        $user_result=User::create($user);
        

        $data=new Teacher();
        $data->user_id=$user_result->id;
        $data->school_id=$school_id;
        $data->name=$request->name;
        $data->email=$request->email;
        $data->address=$request->address;
        $data->contact=$request->contact;
        $data->status=1;
//        $data->image=$request->image;
        $data->save();
        if ($request->hasFile('image')) {
            $image_name =$request->name . '-'.$request->contact . '.'.$request->image->extension();
            $path = $request->image->move('uploads/profile/', $image_name);
            $data->image = $image_name;
            $data->save();
        }
         if ($user_result->exists) {

                $role = config('roles.models.role')::where('name', '=', 'Teacher')->first();
                $user_result->attachRole($role);

                return redirect()->route('teacher/index')->with('message','Teacher Sucessfully Added.');

            }

    }
    public function editteacher(Teacher $teacher)
    {
        return view('admin::teacher.teacher_edit',compact('teacher'));
    }
    public function editstoreteacher(Request $request,Teacher $teacher)
    {
        $edit=$teacher->update(['name'=>$request->name,'email'=>$request->email,'password'=>$request->password,'address'=>$request->address,'contact'=>$request->contact]);
        if ($request->hasFile('image')) {
            if($teacher->image && file_exists(public_path('uploads/profile/'.$teacher->image))){
                unlink(public_path('uploads/profile/'.$teacher->image));
            }
            $image_name = $teacher->name . '-'.$teacher->contact . '.'.$request->image->extension();
            $path = $request->image->move('uploads/profile/', $image_name);
            $teacher->image = $image_name;
            $teacher->save();
        }
        return redirect()->route('teacher/index')->with('success','Teacher succesfully edited');
    }
    public function deleteteacher(Teacher $teacher)
    {
       
        if($teacher){
            if($teacher->image){
                $image = public_path('uploads/profile/' . $teacher->image);
                if(file_exists($image)){
                    unlink($image);
                }
            }

            $teacher->delete();

            $user_data=User::find($teacher->user_id)->delete();
            return redirect()->route('teacher/index')->with('success', 'Teacher deleted.');
        }else{
            return redirect()->route('teacher/index')->with('error', 'Error while deleting teacher.');
        }
      
    }
//    Atttendance
    public function attendance()
    {
                $school_data=Admin::where('admin_id',Auth::user()->id)->select('school_id')->distinct('school_id')->get();
        foreach ($school_data as $sd)
        {
            $school_id=$sd->school_id;
//            dd($school_id);
        }
        $grade_data=Grade::where('school_id',$school_id)->get();
        // $attendance_data=array();
        
        // foreach ($grade_data as $gd) {

        //     $attendance_data=Attendance::where('grade_id',$gd->id)->get();
        //     // dd($attendance_data);
        //     $counted=$attendance_data->count();
        //     // dd($counted);   
            
        //     for ($i=0 ; $i <$counted; $i++)
        //     {
               
        //             # code...
        //         // dd($adt->date);
        //             $data[] =array(
        //                 'date'=>$attendance_data[$i]->date,
        //                 'stu_id'=>$attendance_data[$i]->st_id,
        //                 'class_id'=>$attendance_data[$i]->grade_id,
        //                 'section_id'=>$attendance_data[$i]->section_id,
        //                 'status'=>$attendance_data[$i]->status,
        //             );
                
        //     }
            
        //     // dd($data);
        // }
        
        

        return view('admin::attendance.index',['grade_data'=>$grade_data]);
    }
    public function attendancesectioncheck($id)
    {

        $grade_name=Grade::find($id);
        // dd($grade_name);
        $grade=$grade_name->name;
        // dd
        $section_data=Section::where('grade_id',$id)->get();

        
        return view('admin::attendance.attendance_section',['section_data'=>$section_data,'grade'=>$grade]);
    }
    public function attendancecreate($id)
    {
        $section_name=Section::find($id);
        // dd($section_name);
        $section=$section_name->name;
        $section_id=$section_name->id;
        $grade_id=$section_name->grade_id;
        $section_data=Student::where('section_id',$id)->get();


        $date_data=Carbon::now()->format('Y-m-d');
        return view('admin::attendance.create',['section'=>$section,'section_data'=>$section_data,'grade_id'=>$grade_id,'date_data'=>$date_data,'section_id'=>$section_id]);
    }
    public function attendancestore(Request $request)
    {
        if($request!=null)
        {
            $data=$request->all();

            // dd($data);
            for ($i=0; $i < count($data['student']); $i++) { 
                $input = [
                    'date'=>$data['date'],
                    'grade_id'=>$data['grade_id'],
                    'section_id'=>$data['section_id'],
                    'st_id'=>$data['student'][$i],
                    'status'=>$data['status'][$i],

                ];
                $result=Attendance::create($input);
            }
            return redirect()->route('attendance/index')->with('success','Attendance Created for date '.$data['date']);
        } 
        else{
             return redirect()->route('attendance/index')->with('error','Something went wrong');
        }      
       
    }
    public function attendancecheck()
    {
        $school_data=Admin::where('admin_id',Auth::user()->id)->select('school_id')->distinct('school_id')->get();
        foreach ($school_data as $sd)
        {
            $school_id=$sd->school_id;
//            dd($school_id);
        }
        $grade_data=Grade::where('school_id',$school_id)->get();
//        dd($grade_data);
//        return view('admin::section.section_add',['grade_data'=>$grade_data]);
        return view('admin::attendance.checkattendance',['grade_data'=>$grade_data]);
    }
    public function attendancedetails(Request $request)
    {        
        $check=Attendance::where('date',$request->date)->where('grade_id',$request->grade_id)->where('section_id',$request->section_id)->get();
        $class=$request->grade_id;
        $section=$request->section_id;
        $date=$request->date;

        return view('admin::attendance.attendance_details',['check'=>$check,'class'=>$class,'section'=>$section,'date'=>$date]);
    }
    // Student Fetch
    public function fetchstudent(Request $request)
    {
        $select = $request->get('select');
        $value = $request->get('value');
        $dependent = $request->get('dependent');
        $datas = Student::where('section_id', $value)
            ->get();
           $i=0;
           $count=1;
//          return $datas;
        $outputs='';
        foreach($datas as $row)
        {
            $outputs .= '<tr class="dynamical">
                                <td>'.$count.'</td>
                                <td>'.$row->name.
                                    '<input type="hidden" name="student['.$i.']" value="'.$row->id.'">
                                </td>

                                <td>
                                <div class="radio">
                                        <input type="radio" id="attendance" name="status['.$i.']" value="1" checked >
                                        <label for="attendance">Present</label>
                                </div>
                                        <div class="radio">
                                            <input type="radio" id="attendance" name="status['.$i.']" value="0">
                                            <label for="attendance">Absent</label>
                                        </div>


                               </td>
                            </tr>';
                $i++;
                $count++;
        }
        echo $outputs;
    }
    // Exam
    public function examtypeindex()
    {
        $school_data=Admin::where('admin_id',Auth::user()->id)->select('school_id')->distinct('school_id')->get();
        foreach ($school_data as $sd)
        {
            $school_id=$sd->school_id;
//            dd($school_id);
        }
        $examtype_data=Exam::where('school_id',$school_id)->get();
        return view('admin::exam.examtype_index',['examtype_data'=>$examtype_data]);
    }
    public function examtypeadd()
    {
        return view('admin::exam.examtype_add');
    }
    public function examtypestore(Request $request)
    {
         $school_data=Admin::where('admin_id',Auth::user()->id)->select('school_id')->distinct('school_id')->get();
        foreach ($school_data as $sd)
        {
            $school_id=$sd->school_id;
        }
        $data=new Exam();
        $data->school_id=$school_id;
        $data->examtype=$request->name;
        $data->description=$request->description;
        $data->status=1;
        $data->save();
        return redirect()->route('examtype/index');
    }
    public function editexamtype(Exam $examtype)
    {
        return view('admin::exam.examtype_edit',compact('examtype'));
    }
    public function editstoreexamtype(Request $request,Exam $examtype)
    {
        $edit=$examtype->update(['examtype'=>$request->name,'description'=>$request->description]);
        return redirect()->route('examtype/index')->with('success','ExamType succesfully edited');
    }
    public function deleteexamtype(Exam $examtype)
    {
        $delete=$examtype->delete();
        return redirect()->route('examtype/index')->with('error','ExamType succesfully deleted');
    }
    public function showhideexam(Exam $examtype)
    {
        if($examtype->status)
        {
            $examtype->status=0;
        }
        else
        {
            $examtype->status=1;
        }
        $examtype->save();
        return redirect()->route('examtype/index')->with('success','Status Updated');
    }

    // Marksheet Generate
    public function marksheetindex()
    {
        $school_data=Admin::where('admin_id',Auth::user()->id)->select('school_id')->distinct('school_id')->get();
        foreach ($school_data as $sd)
        {
            $school_id=$sd->school_id;
//            dd($school_id);
        }
        $grade_data=Grade::where('school_id',$school_id)->get();
        return view('admin::marksheet.marksheet_grade',['grade_data'=>$grade_data]);
    }
    public function marksheetsectioncheck($id)
    {
        $grade_name=Grade::find($id);
        $grade=$grade_name->name;
        $section_data=Section::where('grade_id',$id)->get();        
        return view('admin::marksheet.marksheet_section',['section_data'=>$section_data,'grade'=>$grade]);
    }
    public function marksheetstudentlist( $id)
    {
//        dd($student);
        $student_data=Student::where('section_id',$id)->get();
        return view('admin::marksheet.student_list',compact('student_data'));
    }
    public function marksheetdisplay($id)
    {
//        dd($id);
        $examtype=Mark::where('st_id',decrypt($id))->groupBy('examtype')->get();
        // dd($examtype);
        $student_id=decrypt($id);
        // dd($student_id);
        return view('admin::marksheet.marksheet_display_examtype',compact('examtype','student_id'));
    }  
    public function marksheetgenerate($id,$examtype)
    {
        // dd(decrypt($id));
        // dd($examtype);
        $student=Student::find(decrypt($id));

        $student_details=Academicinfo::where('s_id',decrypt($id))->latest()->first();
        $exam_name=Exam::find($examtype);
        // dd($exam_name);

        // dd($student_details);
        // $grade_data=Grade::where('id',$student_details->grade_id)->first();
        // $section_data=Section::where('id',$student_details_data->section_id)->first();
//        dd($student);
        $subjects=DB::table('subjects')->join('grade_subject','grade_subject.subject_id','=','subjects.id')->where('grade_subject.grade_id','=',$student->grade_id)->get();
//       dd($subjects);
//        dd($student->grades->subjects);
        $marks=Mark::where('st_id',decrypt($id))->where('examtype',$examtype)->get();
//        dd($student);
        $total_fullmarks=0;
        $i=0;       
        $data=$student->grades->subjects->map(function($subject) use($id,$examtype,$total_fullmarks)
        {
            $mark=Mark::where('st_id',decrypt($id))->where('examtype',$examtype)->where('subject_id',$subject->id)->first();
//            dd($mark);
            $data_marks=array(
                'fullmarks'=>$subject->fullmarks,
                'subject_name'=>$subject->name,
                'theory_marks'=>$subject->theory_marks,
                'practical_marks'=>$subject->practical_marks,
                'theory_passmarks'=>((40 / 100) * ($subject->theory_marks)),
                'practical_passmarks'=>((40 / 100) * ($subject->practical_marks)),
                
                // 'totals_fullmark'=>$subject->fullmarks+$total_fullmarks,
            );            
            if($mark)
            {
                $data_marks += array(

                    'obtained_theory_marks'=>$mark->marks,
                    'obtained_practical_marks'=>$mark->practical_marks,
                    'total_obtained_marks'=>$total_fullmarks+($mark->marks)+($mark->practical_marks),
                );
            }
            else{
                $data_marks += array(

                    'obtained_theory_marks'=>0,
                    'obtained_practical_marks'=>0,
                    'total_obtained_marks'=>$total_fullmarks,
                );
            }            
             return $data_marks;
             // $total_marks=$total_fullmarks_+($fullsmarks[$i]);
        });
        $total_full_marks=0;

       $total_obtained_fullmarks= 0;
       $total_obtained_theory_marks=0;
       $total_obtained_practical_marks=0;

    foreach ($data as $item) {
        $total_full_marks += $item['fullmarks'];
    $total_obtained_fullmarks += $item['total_obtained_marks'];
    $total_obtained_theory_marks += $item['obtained_theory_marks'];
    $total_obtained_practical_marks += $item['obtained_practical_marks'];
        }


$percentage=($total_obtained_fullmarks/$total_full_marks)*100;

        $count_value=0;
        $count_marks_value=0;
        $fail_marks=0;
        foreach($data as $dts)
        {
           
            $course_id_value=$dts['subject_name'];
                // dd($course_id_value);
            if(!empty($course_id_value))
            {
                $count_value++;
            }
            // dd($count_value);
            $theory_marks_value=$dts['obtained_theory_marks'];
            $practical_marks_value=$dts['obtained_practical_marks'];
            // dd($practical_marks_value);     

//            dd($marks_value);
            if(!empty($theory_marks_value && $practical_marks_value))
            {
                $count_marks_value++;
            }
            // dd($count_marks_value);
            if($theory_marks_value<($dts['theory_passmarks']) && $practical_marks_value<($dts['practical_passmarks']))
            {
                $fail_marks++;
            }
            // dd($fail_marks);
            
        }

        if($fail_marks != 0 )
            {
                $result_value='Fail';
            }
         else
         {
            $result_value='Pass';
         }   
         // ------*save status*---------
         $marksheet_status=Marksheet::where('student_id',$student->id)->where('exam_id',$examtype)->where('grade_id',$student_details->grade_id)->where('section_id',$student_details->section_id)->first();
        // dd($marksheet_status);
               // dd($result_value);
// dd($percentage);
        // dd($total_full_marks);
        // dd($total_obtained_fullmarks);
       // dd($data);
        // *------------check-----------*


        return view('admin::marksheet.marksheet_generate',['student'=>$student,'data'=>$data,'total_obtained_fullmarks'=>$total_obtained_fullmarks,'total_obtained_theory_marks'=>$total_obtained_theory_marks,'total_obtained_practical_marks'=>$total_obtained_practical_marks,'percentage'=>$percentage,'result_value'=>$result_value,'student_details'=>$student_details,'exam_name'=>$exam_name,'marksheet_status'=>$marksheet_status]);
    }
    public function marksheetstore(Request $request)
    {
//        dd($request->all());
        $total_theory_marks=number_format( (float) $request->total_theory_marks, 2, '.', '');
        $total_practical_marks=number_format( (float) $request->total_practical_marks, 2, '.', '');
        $total_marks=number_format( (float) $request->total_marks, 2, '.', '');
        // dd($request->total_obtained_theory_marks);
        $data=new Marksheet();
        $data->school_id=$request->school_id;
        $data->grade_id=$request->grade_id;
        $data->section_id=$request->section_id;
        $data->student_id=$request->student_id;
        $data->exam_id=$request->exam_id;
        $data->total_theory_marks=$total_theory_marks;
        $data->total_practical_marks=$total_practical_marks;
        $data->total_marks=$total_marks;
        $data->percentage=$request->percentage;
        $data->result=$request->result;       
        $data->status=0;
//         dd($data);
        $data->save();

        if($data)
        {
            return redirect()->route('marksheet/index')->with('success','Marksheet Generated');
        }

    }
    public function sendmarksheetemail($id,$examtype,Marksheet $marksheet)
    {
        $student=Student::find($id);
        $student_details=Academicinfo::where('s_id',$id)->latest()->first();
        
        $exam_name=Exam::find($examtype);
        // dd($exam_name);

        // dd($student_details);
        // $grade_data=Grade::where('id',$student_details->grade_id)->first();
        // $section_data=Section::where('id',$student_details_data->section_id)->first();
//        dd($student);
        $subjects=DB::table('subjects')->join('grade_subject','grade_subject.subject_id','=','subjects.id')->where('grade_subject.grade_id','=',$student->grade_id)->get();
//       dd($subjects);
//        dd($student->grades->subjects);
        $marks=Mark::where('st_id',$id)->where('examtype',$examtype)->get();
//        dd($student);
        $total_fullmarks=0;
        $i=0;       
        $data=$student->grades->subjects->map(function($subject) use($id,$examtype,$total_fullmarks)
        {
            $mark=Mark::where('st_id',$id)->where('examtype',$examtype)->where('subject_id',$subject->id)->first();
//            dd($mark);
            $data_marks=array(
                'fullmarks'=>$subject->fullmarks,
                'subject_name'=>$subject->name,
                'theory_marks'=>$subject->theory_marks,
                'practical_marks'=>$subject->practical_marks,
                'theory_passmarks'=>((40 / 100) * ($subject->theory_marks)),
                'practical_passmarks'=>((40 / 100) * ($subject->practical_marks)),
                
                // 'totals_fullmark'=>$subject->fullmarks+$total_fullmarks,
            );            
            if($mark)
            {
                $data_marks += array(
                    'obtained_theory_marks'=>$mark->marks,
                    'obtained_practical_marks'=>$mark->practical_marks,
                    'total_obtained_marks'=>$total_fullmarks+($mark->marks)+($mark->practical_marks),
                );
            }
            else{
                $data_marks += array(

                    'obtained_theory_marks'=>0,
                    'obtained_practical_marks'=>0,
                    'total_obtained_marks'=>$total_fullmarks,
                );
            }            
             return $data_marks;
             // $total_marks=$total_fullmarks_+($fullsmarks[$i]);
        });
        // dd($data);
         $marksheet_status=Marksheet::where('student_id',$student->id)->where('exam_id',$examtype)->where('grade_id',$student_details->grade_id)->where('section_id',$student_details->section_id)->first();

        Mail::send(new MarksheetDetails($student,$student_details,$exam_name,$data,$marksheet_status));
        return redirect()->back();
    }

    public function viewasteacher($id)
    {
        session(['loginAs'=>auth()->user()->id]);
        Auth::logout();
        Auth::loginUsingId($id);
        return redirect('teacher');
    }

}
