<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::group(['middleware' => 'auth', 'prefix' => 'admin'], function() {

//    Parent
    Route::get('/parent',['as'=>'parent/index','uses'=>'AdminController@parent']);
    Route::get('/parent/create',['as'=>'parent/create','uses'=>'AdminController@parentcreate']);
    Route::post('/parent/store',['as'=>'parent/store','uses'=>'AdminController@parentstore']);
    Route::get('/parent/edit/{parent}',['as'=>'parent/edit','uses'=>'AdminController@editparent']);
    Route::post('/parent/editstore/{parent}',['as'=>'parent/editstore','uses'=>'AdminController@editstoreparent']);
    Route::get('/parent/delete/{parent}',['as'=>'parent/delete','uses'=>'AdminController@deleteparent']);
    Route::get('/parent/showhide/{parent}','AdminController@showhideparent')->name('parents.showhideparent');
//Student
    Route::get('/student',['as'=>'student/index','uses'=>'AdminController@student']);
    Route::get('/student/create',['as'=>'student/create','uses'=>'AdminController@studentcreate']);
    Route::post('/student/store',['as'=>'student/store','uses'=>'AdminController@studentstore']);
    Route::get('/student/edit/{student}',['as'=>'student/edit','uses'=>'AdminController@editstudent']);
    Route::post('/student/editstore/{student}',['as'=>'student/editstore','uses'=>'AdminController@editstorestudent']);
    Route::get('/student/delete/{student}',['as'=>'student/delete','uses'=>'AdminController@deletestudent']);
    Route::get('/student/view/{student}',['as'=>'student/view','uses'=>'AdminController@viewstudent']);
    Route::get('/student/showhide/{student}','AdminController@showhidestudent')->name('students.showhidestudent');
    Route::post('/student/academicinfostore/{academicinfo}',['as'=>'student/academicinfostore','uses'=>'AdminController@academicinfostore']);
//    Teacher
    Route::get('/teachers',['as'=>'teacher/index','uses'=>'AdminController@teacher']);
    Route::get('/teacher/create',['as'=>'teacher/create','uses'=>'AdminController@teachercreate']);
    Route::post('/teacher/store',['as'=>'teacher/store','uses'=>'AdminController@teacherstore']);
    Route::get('/teacher/edit/{teacher}',['as'=>'teacher/edit','uses'=>'AdminController@editteacher']);
    Route::post('/teacher/editstore/{teacher}',['as'=>'teacher/editstore','uses'=>'AdminController@editstoreteacher']);
    Route::get('/teacher/delete/{teacher}',['as'=>'teacher/delete','uses'=>'AdminController@deleteteacher']);
    Route::get('/viewasteacher/{id}',['as'=>'teacher/view','uses'=>'AdminController@viewasteacher']);
//    Attendance
    Route::get('/attendance',['as'=>'attendance/index','uses'=>'AdminController@attendance']);
    Route::get('/attendance/sectioncheck/{id}',['as'=>'attendance/sectioncheck','uses'=>'AdminController@attendancesectioncheck']);
    Route::get('/attendance/create/{id}',['as'=>'attendance/create','uses'=>'AdminController@attendancecreate']);
    Route::post('/attendance/store',['as'=>'attendance/store','uses'=>'AdminController@attendancestore']);
    Route::get('/attendance/check',['as'=>'attendance/check','uses'=>'AdminController@attendancecheck']);
    Route::post('/attendance/details',['as'=>'attendance/details','uses'=>'AdminController@attendancedetails']);

    Route::get('/', 'AdminController@index');
//    Grade
    Route::get('/grade',['as'=>'grade/index','uses'=>'AdminController@grade']);
    Route::get('/grade/create',['as'=>'grade/create','uses'=>'AdminController@gradecreate']);
    Route::post('/grade/store',['as'=>'grade/store','uses'=>'AdminController@storegrade']);
    Route::get('/grade/edit/{grade}',['as'=>'grade/edit','uses'=>'AdminController@editgrade']);
    Route::post('/grade/editstore/{grade}',['as'=>'grade/editstore','uses'=>'AdminController@editstoregrade']);
    Route::get('/grade/delete/{grade}',['as'=>'grade/delete','uses'=>'AdminController@deletegrade']);
    Route::get('/grade/showhide/{grade}','AdminController@showhidegrade')->name('grades.showhidegrade');

//    Section
    Route::get('/section',['as'=>'section/index','uses'=>'AdminController@section']);
    Route::get('/section/create',['as'=>'section/create','uses'=>'AdminController@sectioncreate']);
    Route::post('/section/store',['as'=>'section/store','uses'=>'AdminController@storesection']);
    Route::get('/section/edit/{section}',['as'=>'section/edit','uses'=>'AdminController@editsection']);
    Route::post('/section/editstore/{section}',['as'=>'section/editstore','uses'=>'AdminController@editstoresection']);
    Route::get('/section/delete/{section}',['as'=>'section/delete','uses'=>'AdminController@deletesection']);
    //Subject
    Route::get('/subject',[
       'as'=>'subject/index','uses'=>'AdminController@subject']);
    Route::get('/subject/create',['as'=>'subject/create','uses'=>'AdminController@subjectcreate']);
    Route::post('/subject/store',['as'=>'subject/store','uses'=>'AdminController@subjectstore']);
    Route::get('/subject/edit/{subject}',['as'=>'subject/edit','uses'=>'AdminController@editsubject']);
    Route::post('/subject/editstore/{subject}',['as'=>'subject/editstore','uses'=>'AdminController@editstoresubject']);
    Route::get('/subject/delete/{subject}',['as'=>'subject/delete','uses'=>'AdminController@deletesubject']);
    Route::get('/subject/showhide/{subject}','AdminController@showhidesubject')->name('subjects.showhidesubject');

        // Exam
    Route::get('/examtype',['as'=>'examtype/index','uses'=>'AdminController@examtypeindex']);
    Route::get('/examtype/add',['as'=>'examtype/add','uses'=>'AdminController@examtypeadd']);
    Route::post('/examtype/store',['as'=>'examtype/store','uses'=>'AdminController@examtypestore']);
    Route::get('/examtype/edit/{examtype}',['as'=>'examtype/edit','uses'=>'AdminController@editexamtype']);
    Route::post('/examtype/editstore/{examtype}',['as'=>'examtype/editstore','uses'=>'AdminController@editstoreexamtype']);
    Route::get('/examtype/delete/{examtype}',['as'=>'examtype/delete','uses'=>'AdminController@deleteexamtype']);
    Route::get('/examtype/showhide/{examtype}','AdminController@showhideexam')->name('exams.showhideexam');
    // Marksheet

    Route::get('/marksheet',['as'=>'marksheet/index','uses'=>'AdminController@marksheetindex']);
    Route::get('/marksheet/sectioncheck/{id}',['as'=>'marksheet/sectioncheck','uses'=>'AdminController@marksheetsectioncheck']);
    Route::get('/studentlist/{id}',['as'=>'marksheet/studentlist','uses'=>'AdminController@marksheetstudentlist']);
    Route::get('/marksheetdisplay/{id}',['as'=>'marksheet/marksheetdisplay','uses'=>'AdminController@marksheetdisplay']);
    Route::get('/marksheetgenerate/{id}/{examtype}',['as'=>'marksheet/marksheetgenerate','uses'=>'AdminController@marksheetgenerate']);
    Route::post('/marksheetgenerate/store',['as'=>'marksheet/store','uses'=>'AdminController@marksheetstore']);
    Route::get('/sendmarksheetmail/{id}/{examtype}/{marksheet}','AdminController@sendmarksheetemail')->name('marksheet/sendmarksheetmail');
    Route::get('generatepdf/{id}/{examtype}/{marksheet}','GeneratePdfController@generatepdf')->name('marksheet/generatepdf');
    // Financial

    Route::get('/feetype/index',['as'=>'feetype/index','uses'=>'FinancialController@feetypeindex']);
    Route::get('/feetype/add',['as'=>'feetype/add','uses'=>'FinancialController@feetypeadd']);
    Route::post('/feetype/store',['as'=>'feetype/store','uses'=>'FinancialController@feetypestore']);
    Route::get('/feetype/edit/{feetype}',['as'=>'feetype/edit','uses'=>'FinancialController@editfeetype']);
    Route::post('/feetype/editstore/{feetype}',['as'=>'feetype/editstore','uses'=>'FinancialController@editstorefeetype']);
    Route::get('/feetype/delete/{feetype}',['as'=>'feetype/delete','uses'=>'FinancialController@deletefeetype']);
    Route::get('/feetype/showhide/{feetype}','FinancialController@showhidefeetype')->name('feetypes.showhidefeetype');

    Route::get('/gradefee/index',['as'=>'gradefee/index','uses'=>'FinancialController@gradefeeindex']);
    Route::get('/gradefee/details',['as'=>'gradefee/details','uses'=>'FinancialController@gradefeedetails']);
    Route::get('/gradefee/{grade}',['as'=>'gradefee/add','uses'=>'FinancialController@gradefeeadd']);
    Route::post('/gradefee/store',['as'=>'gradefee/store','uses'=>'FinancialController@gradefeestore']);
    Route::get('/gradefee/edit',['as'=>'gradefee/edit','uses'=>'FinancialController@editgradefee']);

    Route::get('/studentfee/index',['as'=>'studentfee/index','uses'=>'FinancialController@studentfeeindex']);
    Route::get('/studentfee/grade',['as'=>'studentfee/grade','uses'=>'FinancialController@studentfeegrade']);
    Route::get('/studentfee/studentlist/{id}',['as'=>'studentfee/studentlist','uses'=>'FinancialController@studentfeelist']);
    Route::get('studentfee/generate/{student}',['as'=>'studentfee/generate','uses'=>'FinancialController@studentfeegenerate']);
    Route::post('/studentfee/store',['as'=>'studentfee/store','uses'=>'FinancialController@studentfeestore']);
     Route::get('/studentfee/view/{student}',['as'=>'studentfee/view','uses'=>'FinancialController@studentfeeview']);
    Route::get('/generatefeepdf/{bill}/{student}','GeneratePdfController@generatefeepdf')->name('fee/generatefeepdf');
    Route::get('/sendfeemail/{bill}/{student}','FinancialController@sendfeemail')->name('fee/sendfeemail');

    Route::get('generatefeepdf','GeneratePdfController@generatefeepdf')->name('fee/generatefeepdf');

    Route::post('dynamic_dependent/fetch',[
        'as'=>'dynamindependent.fetch_section','uses'=>'AdminController@fetchsection'
    ]);

    // Route::post('dynamic_dependent/fetchstudent',[
    //     'as'=>'dynamicindependent.fetch_student','uses'=>'AdminController@fetchstudent'
    // ]);

//    Accountant
    Route::resource('accountants', 'AccountantController');
    Route::get('accountant/showhide/{accountant}', 'AccountantController@showhide')->name('accountants.showhide');

//    Librarian
    Route::resource('librarians', 'LibrarianController');
    Route::get('librarian/showhide/{librarian}', 'LibrarianController@showhide')->name('librarians.showhide');
});
