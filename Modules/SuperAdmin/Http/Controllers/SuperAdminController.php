<?php

namespace Modules\SuperAdmin\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

use Illuminate\Routing\Controller;
use Session;
use Illuminate\Support\Facades\Hash;
use jeremykenedy\LaravelRoles\Traits\HasRoleAndPermission;
use App\Admin;

use App\School;

class SuperAdminController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Response
     */
    use HasRoleAndPermission;

    public function __construct()
    {
        $this->middleware('role:superadmin');

    }
    public function index()
    {

        return view('superadmin::index');
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        return view('superadmin::create');
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Response
     */
    public function show($id)
    {
        return view('superadmin::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Response
     */
    public function edit($id)
    {
        return view('superadmin::edit');
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Response
     */
    public function destroy($id)
    {
        //
    }

//    School Index
    public function schoolindex()
    {
        $admin_details=Admin::all();
        return view('superadmin::school.index',compact('admin_details'));
    }

    public function schooladd()
    {
        return view('superadmin::school.school_add');
    }

    public function schoolstore(Request $request)
    {
        if($request!=null) {
            $user = array(
                'name' => $request->adminname,
                'email' => $request->adminemail,
                'password' => Hash::make($request->adminpassword)
            );

            $result = User::create($user);

            $input = array(

                'name' => $request->schoolname,
                'address' => $request->schooladdress,
                'contact' => $request->schoolcontact

            );

            $school = School::create($input);

            $admin= new Admin();
            $admin->admin_id=$result->id;
            $admin->school_id=$school->id;
            $admin->name = $request->adminname;
            $admin->address = $request->adminaddress;
            $admin->contact = $request->admincontact;
            $admin->save();

            if ($request->hasFile('profile_img')) {
                $image_name = str_slug($request->adminname) . '-'.$result->id . '.'.$request->profile_img->extension();
                $path = $request->profile_img->move('uploads/admin/', $image_name);
                $admin->profile_img = $image_name;
                $admin->save();
            }
//            $inputs = array(
//                'admin_id' => $result->id,
//                'school_id' => $school->id,
//                'name' => $request->adminname,
//                'address' => $request->adminaddress,
//                'contact' => $request->admincontact,
//                'profile_img' => $request->adminprofile
//
//
//            );
//            $admin=Admin::create($inputs);

            if ($result->exists) {

                $role = config('roles.models.role')::where('name', '=', 'Admin')->first();
                $result->attachRole($role);

                return redirect()->route('school/index')->with('message','School Sucessfully Added.');

            }
        }
        else
        {
            return view('superadmin::school.school_add')->with('error','Error Adding School');
        }
    }
}
