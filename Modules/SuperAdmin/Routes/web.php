<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::group(['middleware' => 'auth', 'prefix' => 'superadmin'], function() {

    Route::get('/', 'SuperAdminController@index');

    Route::get('/school',[
       'as'=>'school/index','uses'=>'SuperAdminController@schoolindex'
    ]);

    Route::get('/school/add',[
       'as'=>'school/add','uses'=>'SuperAdminController@schooladd'
    ]);

//    Schhol Store with admin attached
    Route::post('/school/store',[
       'as'=>'school/store','uses'=>'SuperAdminController@schoolstore'
    ]);
});
