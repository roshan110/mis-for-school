@extends('layouts.superadmin_dashboard')

@section('content')

    {{--container_fluid included already--}}
    @if(Session::has('message'))
        <p class="alert alert-success {{ Session::get('alert-class', 'alert-info') }}">{{ Session::get('message') }}</p>
    @endif

    @if (count($errors) > 0)
        @foreach ($errors->all() as $error)
            <p class="alert alert-danger">{{ $error }}</p>
        @endforeach
    @endif




    <div class="row">
        <div class="col-md-8 col-sm-8 col-xs-12">
            <div class="header_content">
                <h2 class="text-left">
                    School & its admin
                </h2>

            </div>
        </div>

        <div class="col-md-4 col-sm-4 col-xs-12">
            <div class="button_header_content text-right mt-2 ">
                <a href="{{route('school/add')}}" class="btn btn-primary"><i class="fa fa-plus"></i>Add New School</a>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="table-responsive">
                <table class="table table-hover">
                    <thead>
                    <tr>
                    <th>#</th>
                    <th>Name</th>
                    <th>Address</th>
                    <th>Contact</th>
                        <th>Admin</th>
                        <th>Admin Address</th>
                        <th>Admin Contact</th>
                    {{--<th>Action</th>--}}
                    </tr>
                    </thead>

                    <tbody>
                    <?php $i=1; ?>
                    @foreach($admin_details as $ad)
                    <tr>
                        <td>{{$i}}</td>
                        <td>{{$ad->school->name}}</td>
                        <td>{{$ad->school->address}}</td>
                        <td>{{$ad->school->contact}}</td>
                        <td>{{$ad->name}}</td>
                        <td>{{$ad->address}}</td>
                        <td>{{$ad->contact}}</td>
                    </tr>
                        <?php $i++; ?>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>


    {{--Admin Details--}}
    {{--<div class="row">--}}
        {{--<div class="col-md-12 col-sm-12 col-xs-12">--}}
            {{--<div class="header_content">--}}
                {{--<h2 class="text-left">--}}
                    {{--Admin--}}
                {{--</h2>--}}

            {{--</div>--}}
        {{--</div>--}}


    {{--</div>--}}
    {{--<div class="row">--}}
        {{--<div class="col-md-12 col-sm-12 col-xs-12 m-auto">--}}
            {{--<div class="table-responsive">--}}
                {{--<table class="table table-hover">--}}
                    {{--<thead>--}}
                    {{--<tr>--}}
                        {{--<th>#</th>--}}
                        {{--<th>Profile</th>--}}
                        {{--<th>Name</th>--}}
                        {{--<th>Email</th>--}}
                        {{--<th>School</th>--}}
                        {{--<th>Address</th>--}}
                        {{--<th>Contact</th>--}}
                        {{--<th>Action</th>--}}
                    {{--</tr>--}}
                    {{--</thead>--}}

                    {{--<tbody>--}}
                    {{--<tr>--}}
                        {{--<td></td>--}}
                        {{--<td></td>--}}
                        {{--<td></td>--}}
                        {{--<td></td>--}}
                        {{--<td></td>--}}
                    {{--</tr>--}}
                    {{--</tbody>--}}
                {{--</table>--}}
            {{--</div>--}}
        {{--</div>--}}
    {{--</div>--}}

    {{--end container_fluid included already--}}

@endsection