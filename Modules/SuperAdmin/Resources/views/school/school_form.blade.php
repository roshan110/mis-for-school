<h5 class="text-muted">School Details</h5>
<hr>

<div class="form-group row">
    <div class="col-md-3 col-sm-4 col-xs-6">
        <label for="schoolname">Name:</label>
    </div>
    <div class="col-md-9 col-sm-8 col-xs-6">
        <input type="text" name="schoolname" id="schoolname" class="form-control" autocomplete="off">
    </div>
</div>

<div class="form-group row">
    <div class="col-md-3 col-sm-4 col-xs-6">
        <label for="schooladdress">Address:</label>
    </div>
    <div class="col-md-9 col-sm-8 col-xs-6">
        <textarea name="schooladdress" id="schooladdress" cols="20" rows="5" class="form-control" autocomplete="off"></textarea>
    </div>
</div>

<div class="form-group row">
    <div class="col-md-3 col-sm-4 col-xs-6">
        <label for="schoolcontact">Contact:</label>
    </div>
    <div class="col-md-9 col-sm-8 col-xs-6">
        <input type="text" name="schoolcontact" id="schoolcontact" class="form-control" autocomplete="off">
    </div>
</div>

<h5 class="text-muted">Admin Details</h5>
<hr>
<div class="form-group row">
    <div class="col-md-3 col-sm-4 col-xs-6">
        <label for="schooladmin">Name:</label>
    </div>
    <div class="col-md-9 col-sm-8 col-xs-6">
        <input type="text" name="adminname" id="schooladmin" class="form-control" autocomplete="off" required>
    </div>
</div>
<div class="form-group row">
    <div class="col-md-3 col-sm-4 col-xs-6">
        <label for="adminemail">Email:</label>
    </div>
    <div class="col-md-9 col-sm-8 col-xs-6">
        <input type="email" name="adminemail" id="adminemail" class="form-control" autocomplete="off" required>
    </div>
</div>

<div class="form-group row">
    <div class="col-md-3 col-sm-4 col-xs-6">
        <label for="adminpassword">Password:</label>
    </div>
    <div class="col-md-9 col-sm-8 col-xs-6">
        <input type="password" name="adminpassword" id="adminpassword" class="form-control" autocomplete="off" required>
    </div>
</div>
{{--<div class="form-group row">--}}
    {{--<div class="col-md-3 col-sm-4 col-xs-6">--}}
        {{--<label for="admincpassword">Confirm Password:</label>--}}
    {{--</div>--}}
    {{--<div class="col-md-9 col-sm-8 col-xs-6">--}}
        {{--<input type="text" name="cpassword" id="admincpassword" class="form-control" autocomplete="off">--}}
    {{--</div>--}}
{{--</div>--}}

<div class="form-group row">
    <div class="col-md-3 col-sm-4 col-xs-6">
        <label for="adminaddress">Addres:</label>
    </div>
    <div class="col-md-9 col-sm-8 col-xs-6">
        <textarea name="adminaddress" id="adminaddress" cols="20" rows="5" class="form-control"></textarea>
    </div>
</div>
<div class="form-group row">
    <div class="col-md-3 col-sm-4 col-xs-6">
        <label for="admincontact">Contact:</label>
    </div>
    <div class="col-md-9 col-sm-8 col-xs-6">
        <input type="text" name="admincontact" id="admincontact" class="form-control" autocomplete="off">
    </div>
</div>
<div class="form-group row">
    <div class="col-md-3 col-sm-4 col-xs-6">
        <label for="adminprofile">Profile:</label>
    </div>
    <div class="col-md-9 col-sm-8 col-xs-6">
        <input type="file" name="profile_img" id="adminprofile" class="form-control" autocomplete="off">
    </div>
</div>

<button type="submit" class="btn btn-success text-white mx-auto">
    <i class="fa fa-save"> Save</i>
</button>
