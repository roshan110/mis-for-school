<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::group(['middleware' => 'auth', 'prefix' => 'teacher'],function() {
    Route::get('/', 'TeacherController@index')->name('teacher.main');
    // marksadd
    Route::get('/marks',['as'=>'marks/index','uses'=>'TeacherController@marksindex']);
    Route::get('/marks/add',['as'=>'marks/add','uses'=>'TeacherController@marksadd']);
    Route::post('/marks/store',['as'=>'marks/store','uses'=>'TeacherController@marksstore']);
    Route::get('/marks/check',['as'=>'marks/check','uses'=>'TeacherController@markscheck']);
    Route::post('/marks/details',['as'=>'marks/details','uses'=>'TeacherController@marksdetails']);
    Route::get('/backtoadmin','TeacherController@backtoadmin')->name('teacher.backtoadmin');
// Route::post('dynamic_dependent/fetch',['as'=>'dynamindependent.fetch_section','uses'=>'TeacherController@fetchsections']);
// Route::post('dynamic_dependent/fetchstudent',['as'=>'dynamicindependent.fetch_student','uses'=>'TeacherController@fetchstudent']);

    Route::post('dynamic_dependent/fetches',[
        'as'=>'dynamindependent.fetch_sections','uses'=>'TeacherController@fetchsections'
    ]);

    Route::post('dynamic_dependent/fetchstudents',[
        'as'=>'dynamindependent.fetch_students','uses'=>'TeacherController@fetchstudents'
    ]);
});


