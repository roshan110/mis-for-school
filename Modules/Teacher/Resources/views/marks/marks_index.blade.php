@extends('layouts.teacher_dashboard')

@section('content')

    {{--container-fluid already included--}}

    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="heading_block">
                <div class="row">
                    <div class="col-md-6 col-sm-6 col-xs-12">
                        <h4>Mark</h4>
                    </div>
                    <div class="col-md-6 col-sm-6 col-xs-12 text-right">
                        <a href="{{route('marks/add')}}" class="btn btn-primary"><i class="fa fa-plus"></i>Add Mark</a>
                    </div>
                </div>
            </div>

           
        </div>
    </div>
@endsection
