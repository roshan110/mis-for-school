@extends('layouts.teacher_dashboard')


@section('content')

    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="heading_block">
                <div class="row">
                    <div class="col-md-6 col-sm-6 col-xs-12">
                        <h4>Marksheet Add</h4>
                    </div>                        
                
                </div>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-sm-8 col-xs-12 mx-auto">
                <form action="{{route('marks/store')}}" method="post" enctype="multipart/form-data">
                    {{csrf_field()}}
                    @include('teacher::marks.marks_form')

                </form>
            </div>
        </div>
    </div>

@endsection

@section('script')
    <script>
        $(document).ready(function(){

            $('.dynamic').change(function(){
                if($(this).val() != '')
                {
                    var select = $(this).attr("id");
                    var value = $(this).val();
                    var dependent = $(this).data('dependent');
                    var _token = $('input[name="_token"]').val();
                    console.log(select);
                    console.log(value);
                    console.log(dependent);
                    $.ajax({
                        url:"{{ route('dynamindependent.fetch_sections') }}",
                        method:"POST",
                        data:{select:select, value:value, _token:_token, dependent:dependent},
                        success:function(result)
                        {

                            
                            $('#'+dependent).html(result);
                        }
    
                    })
                }
            });

            $('#grade').change(function(){
                $('#section').val('');
            });



        });

       
        $(document).ready(function(){

            $('.dynamical').change(function(){
                if($(this).val() != '')
                {
                    var select = $(this).attr("id");
                    var value = $(this).val();
                    var dependent = $(this).data('dependent');
                    var _token = $('input[name="_token"]').val();
                    console.log(select);
                    console.log(value);
                    console.log(dependent);
                    $.ajax({
                        url:"{{ route('dynamindependent.fetch_students') }}",
                        method:"POST",
                        data:{select:select, value:value, _token:_token, dependent:dependent},
                        success:function(result)
                        {
                            console.log(result);

                            $('#'+dependent).html(result);

                        }
    
                    })
                }
            });

            $('#section').change(function(){
                $('#student').val('');
            });



        });
    </script>

@endsection
