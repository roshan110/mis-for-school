<h5 class="text-muted">Mark Add</h5>
<hr>

<div class="form-group row">
    <div class="col-md-3 col-sm-4 col-xs-6">
        <label for="grade">Class:</label>
    </div>
    <div class="col-md-9 col-sm-8 col-xs-6">
        <select name="grade_id" id="grade" class="form-control dynamic" data-dependent="section">
             <option selected="selected">Select Class</option>
            @foreach($grade_data as $gd)
                <option value="{{$gd['id']}}">{{$gd['name']}}</option>
            @endforeach
        </select>
    </div>
</div>
<div class="form-group row">
    <div class="col-md-3 col-sm-4 col-xs-6">
        <label for="section">Section:</label>
    </div>
    <div class="col-md-9 col-sm-8 col-xs-6">
        <select name="section_id" id="section" class="form-control dynamical" data-dependent="student">
            <option selected="selected">Select Section</option>
        </select>

    </div>
</div>
<div class="form-group row">
    <div class="col-md-3 col-sm-4 col-xs-6">
        <label for="subject">Subject:</label>
    </div>
    <div class="col-md-9 col-sm-8 col-xs-6">
        <select name="subject_id" id="subject" class="form-control">
            <option selected="selected">Select Subject</option>
            @foreach($subject_data as $sd)
            <option value="{{$sd['id']}}">{{$sd['name']}}</option>
            @endforeach
        </select>

    </div>
</div>
<div class="form-group row">
    <div class="col-md-3 col-sm-4 col-xs-6">
        <label for="exam">Exam</label>
    </div>
    <div class="col-md-9 col-sm-8 col-xs-6">
        <select name="name" id="exam" class="form-control">
            <option selected="selected">Select Exam</option>
            @foreach($examtype_data as $etd)
                   <option value="{{$etd['id']}}">{{$etd['examtype']}}</option>
                   @endforeach
        </select>

    </div>
</div>

<hr>
<h5 class="text-muted"> Marks FillUp</h5>
<div class="body_block">
                    <div class="table-responsive">
                        <table class=" table table-hover">
                            <thead class="thead-dark">
                            <tr>
                                <th>S.N.</th>
                                <th>Name</th>
                                <th>Theory Marks</th>
                                <th>Practical Marks</th>
                            </tr>
                            </thead>
                            <tbody class="dynamical" id="student">
                                
                            </tbody>
                        </table>
                    </div>
                </div>
<button type="submit" class="btn btn-success text-white mx-auto">
    <i class="fa fa-save"> Save</i>
</button>

 