@extends('layouts.teacher_dashboard')

@section('content')

    {{--container-fluid already included--}}

    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="heading_block">
                <div class="row">
                    <div class="col-md-6 col-sm-6 col-xs-12">
                      
                    </div>
                    
                </div>
            </div>

            <div class="body_block">
                <div class="table-responsive">
                    <table class=" table table-hover">
                        <thead class="thead-dark">
                        <tr>
                            <th>S.N</th>
                            <th>Name</th>
                            <th>Marks</th>
                        </tr>
                        </thead>
                        <tbody>
                            <?php $i=1; ?>
                            @foreach($check as $md)
                            <tr>
                                <td>{{$i}}</td>
                                <td>{{\App\Attendance::student_name($md->st_id)}}</td>
                                <td>{{$md['marks']}}</td>
                                <?php $i++; ?>
                                @endforeach
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection
