@extends('layouts.teacher_dashboard')
@section('content')
<form method="POST" action="{{route('marks/details')}}" >
    {{csrf_field()}}
<h5 class="text-muted">Check Mark</h5>
<hr>

<div class="form-group row">
    <div class="col-md-3 col-sm-4 col-xs-6">
        <label for="grade">Class:</label>
    </div>
    <div class="col-md-9 col-sm-8 col-xs-6">
        <select name="grade_id" id="grade" class="form-control dynamic" data-dependent="section">
             <option selected="selected">Select Class</option>
            @foreach($grade_data as $gd)
                <option value="{{$gd['id']}}">{{$gd['name']}}</option>
            @endforeach
        </select>
    </div>
</div>
<div class="form-group row">
    <div class="col-md-3 col-sm-4 col-xs-6">
        <label for="section">Section:</label>
    </div>
    <div class="col-md-9 col-sm-8 col-xs-6">
        <select name="section_id" id="section" class="form-control dynamical" data-dependent="student">
            <option selected="selected">Select Section</option>
        </select>

    </div>
</div>
<div class="form-group row">
    <div class="col-md-3 col-sm-4 col-xs-6">
        <label for="subject">Subject:</label>
    </div>
    <div class="col-md-9 col-sm-8 col-xs-6">
        <select name="subject_id" id="subject" class="form-control">
            <option selected="selected">Select Subject</option>
            @foreach($subject_data as $sd)
            <option value="{{$sd['id']}}">{{$sd['name']}}</option>
            @endforeach
        </select>

    </div>
</div>

<hr>
<h5 class="text-muted"> Marks Check</h5>

<button type="submit" class="btn btn-success text-white mx-auto">
    <i class="fa fa-check"> Check</i>
</button>

 
</form>
@endsection
@section('script')
    <script>
        $(document).ready(function(){

            $('.dynamic').change(function(){
                if($(this).val() != '')
                {
                    var select = $(this).attr("id");
                    var value = $(this).val();
                    var dependent = $(this).data('dependent');
                    var _token = $('input[name="_token"]').val();
                    console.log(select);
                    console.log(value);
                    console.log(dependent);
                    $.ajax({
                        url:"{{ route('dynamindependent.fetch_section') }}",
                        method:"POST",
                        data:{select:select, value:value, _token:_token, dependent:dependent},
                        success:function(result)
                        {

                            $('#'+dependent).html(result);
                        }

                    })
                }
            });

            $('#class').change(function(){
                $('#section').val('');
            });



        });

    </script>


@endsection

