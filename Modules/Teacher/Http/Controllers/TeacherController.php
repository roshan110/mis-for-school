<?php

namespace Modules\Teacher\Http\Controllers;
use App\Teacher;
use App\Grade;
use App\Subject;
use App\Student;
use App\Mark;
use App\Exam;
use App\Section;
use Dompdf\Dompdf;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use jeremykenedy\LaravelRoles\Traits\HasRoleAndPermission;

class TeacherController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Response
     */

    use HasRoleAndPermission;

    public function __construct()
    {
        $this->middleware('role:teacher');

    }

    public function index()
    {
        return view('teacher::index');
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */

    public function create()
    {
        return view('teacher::create');
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Response
     */
    public function show($id)
    {
        return view('teacher::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Response
     */
    public function edit($id)
    {
        return view('teacher::edit');
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Response
     */
    public function destroy($id)
    {
        //
    }
    public function marksindex()
    {
        return view('teacher::marks.marks_index');
    }
    public function marksadd()
    {
        $school_data=Teacher::where('user_id',Auth::user()->id)->select('school_id')->distinct('school_id')->get();
        foreach ($school_data as $sd)
        {
            $school_id=$sd->school_id;
        }
        $grade_data=Grade::where('school_id',$school_id)->get();

        $subject_data=Subject::where('id',Auth::user()->id)->select('school_id')->distinct('school_id')->get();
        foreach ($school_data as $sd)
        {
            $school_id=$sd->school_id;
        }
        $subject_data=Subject::where('school_id',$school_id)->get();

        $school_data=Exam::where('id',Auth::user()->id)->select('school_id')->distinct('school_id')->get();
        foreach ($school_data as $sd)
        {
            $school_id=$sd->school_id;
        }
        $examtype_data=Exam::where('school_id',$school_id)->get();

        return view('teacher::marks.marks_add',['grade_data'=>$grade_data,'subject_data'=>$subject_data,'examtype_data'=>$examtype_data]);
    }
    public function marksstore(Request $request)
    {
        $school_data = Teacher::where('user_id', Auth::user()->id)->select('school_id')->distinct('school_id')->get();
        foreach ($school_data as $sd) {
            $school_id = $sd->school_id;
        }
        if($request!=null)
        {
            $data=$request->all();
            for ($i=0; $i < count($data['student']); $i++) { 
                $input = [
                    'school_id'=>$school_id,
                    'grade_id'=>$data['grade_id'],
                    'section_id'=>$data['section_id'],
                    'subject_id'=>$data['subject_id'],
                    'st_id'=>$data['student'][$i],
                    'examtype'=>$data['name'],
                    'marks'=>$data['theory_marks'][$i],
                    'practical_marks'=>$data['practical_marks'][$i],
                    'status' => 1,
                ];
                $result=Mark::create($input);
            }
            return redirect()->route('marks/index')->with('success','Marks Updated');
        } 
        else
        {
             return redirect()->route('marks/index')->with('error','Something went wrong');
        }      
    }
    public function markscheck()
    {
        $school_data = Teacher::where('user_id', Auth::user()->id)->select('school_id')->distinct('school_id')->get();
        foreach ($school_data as $sd) {
            $school_id = $sd->school_id;
        }
        $grade_data=Grade::where('school_id',$school_id)->get();
        $subject_data=Subject::where('id',Auth::user()->id)->select('school_id')->distinct('school_id')->get();
        foreach ($school_data as $sd)
        {
            $school_id=$sd->school_id;
        }
        $subject_data=Subject::where('school_id',$school_id)->get();
        return view('teacher::marks.marks_check',['grade_data'=>$grade_data,'subject_data'=>$subject_data]);
    }
    public function marksdetails(Request $request)
    {
        $check=Mark::where('grade_id',$request->grade_id)->where('section_id',$request->section_id)->get();
        $class=$request->grade_id;
        $section=$request->section_id;
        return view('teacher::marks.marks_details',['check'=>$check,'class'=>$class,'section'=>$section]);
    }    
    // fetchsection
    public function fetchsections(Request $request)
    {
        $select = $request->get('select');
        $value = $request->get('value');
        $dependent = $request->get('dependent');
        $data = Section::where('grade_id', $value)
            ->get();
        $output = '<option value="">Select '.ucfirst('Section').'</option>';
        foreach($data as $row)
        {
            $output .= '<option value="'.$row->id.'">'.$row->name.'</option>';
        }
        echo $output;
    }
    // fetchstudent
    public function fetchstudents(Request $request)
    {
        $select = $request->get('select');
        $value = $request->get('value');
        $dependent = $request->get('dependent');
        $datas = Student::where('section_id', $value)
            ->get();
         // return $datas;   
           $i=0;
           $count=1;
//          return $datas;
        $outputs='';
        foreach($datas as $row)
        {
            $outputs .= '<tr class="dynamical">
                                <td>'.$count.'</td>
                                <td>'.$row->name.
                                    '<input type="hidden" name="student['.$i.']" value="'.$row->id.'">
                                </td>
                                <td>
                                <div class="form-group ">
                                    <div class="col-xs-5">
                                        <input class="form-control" type="text" name="theory_marks['.$i.']" >
                                    </div>
                                </div>
                               </td>
                               <td>
                                <div class="form-group ">
                                    <div class="col-xs-5">
                                        <input class="form-control" type="text" name="practical_marks['.$i.']" >
                                    </div>
                                </div>                               
                               </td>
                            </tr>';
                $i++;
                $count++;
        }
        echo $outputs;
    }

    function backtoadmin(){
        $id = session('loginAs');
        Auth::logout();
        \Auth::loginUsingId($id);
        return redirect('admin');

    }
}
