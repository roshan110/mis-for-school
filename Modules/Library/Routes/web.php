<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::prefix('library')->group(function() {
    Route::get('/', 'LibraryController@index');
    Route::get('/books',[
        'as' => 'library/books', 'uses' => 'LibraryController@books'
    ]);

//    For Category
    Route::get('/category',[
        'as' => 'library/category', 'uses' => 'LibraryController@bookscategory'
    ]);
    Route::get('/books/categorycreate',[
        'as' => 'library/books/categorycreate', 'uses' => 'LibraryController@bookscategorycreate'
    ]);
    Route::post('/books/categorystore',[
        'as' => 'library/books/categorystore', 'uses' => 'LibraryController@bookscategorystore'
    ]);
    Route::get('/books/categoryedit/{id}',[
        'as' => 'library/books/categoryedit', 'uses' => 'LibraryController@bookscategoryedit'
    ]);
    Route::post('/books/categoryupdate/{id}',[
        'as' => 'library/books/categoryupdate', 'uses' => 'LibraryController@bookscategoryupdate'
    ]);
    Route::get('/books/categorydelete/{id}',[
        'as' => 'library/books/categorydelete', 'uses' => 'LibraryController@bookscategorydelete'
    ]);
//    For Booksitem
    Route::get('/books/bookcreate',[
        'as' => 'library/books/bookcreate', 'uses' => 'LibraryController@bookscreate'
    ]);
    Route::post('/books/bookstore',[
        'as' => 'library/books/bookstore', 'uses' => 'LibraryController@booksstore'
    ]);
    Route::get('/books/bookedit/{id}',[
        'as' => 'library/books/bookedit', 'uses' => 'LibraryController@booksedit'
    ]);
    Route::post('/books/bookupdate/{id}',[
        'as' => 'library/books/bookupdate', 'uses' => 'LibraryController@booksupdate'
    ]);
    Route::get('/books/bookdelete/{id}',[
        'as' => 'library/books/bookdelete', 'uses' => 'LibraryController@booksdelete'
    ]);
});
