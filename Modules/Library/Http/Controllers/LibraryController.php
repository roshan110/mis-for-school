<?php

namespace Modules\Library\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;

use Session;
use Illuminate\Support\Facades\Hash;
use jeremykenedy\LaravelRoles\Traits\HasRoleAndPermission;
use App\Admin;
use Illuminate\Support\Facades\Auth;
use App\School;
use App\Grade;

use App\Book;
use App\Bookcategory;

class LibraryController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Response
     */
    use HasRoleAndPermission;

    public function __construct()
    {
        $this->middleware('role:admin');

    }
    public function index()
    {
        return view('library::index');
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        return view('library::create');
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Response
     */
    public function show($id)
    {
        return view('library::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Response
     */
    public function edit($id)
    {
        return view('library::edit');
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Response
     */
    public function destroy($id)
    {
        //
    }

//    Library Admin
//    For Books

    public function books(Request $request)
    {
        $books=Book::query();
        if($request->category){
            $books=$books->with('parent')->where('category_id',$request->category)->orderby('order_by')->get();

        }
        else{
            $books = $books->get();
        }
        $bookcategories = Bookcategory::all();
//        $parent=$bookcategories->getParentTitleAttribute();


//        $data['bookcategories'] = Bookcategory::orderBy('order_by','asc')->get();
//        $data['books']=Book::orderBy('order_by','asc')->get();
        return view('library::books',compact('books','bookcategories'));
    }
//    For Book category
    public function bookscategory()
    {
        $data['bookcategories'] = Bookcategory::orderBy('order_by','asc')->get();
        return view('library::category',$data);
    }
    public function bookscategorycreate()
    {
        $auth_id=Auth::user()->id;
        $school_data=Admin::where('admin_id',$auth_id)->select('school_id')->distinct('school_id')->get();
        foreach($school_data as $sd)
        {
            $school_id=$sd->school_id;
        }
        $parents = Bookcategory::where('parent_id', 0)->get();
        $grade_data=Grade::where('school_id',$school_id)->get();
        return view('library::add_category',compact('parents','grade_data'));
    }
    public function bookscategorystore(Request $request)
    {
//        $this->validate($request, [
//            'title' => 'required|unique:categories',
//            'details' => 'required',
//        ]);

        $auth_id=Auth::user()->id;
        $school_data=Admin::where('admin_id',$auth_id)->select('school_id')->distinct('school_id')->get();
        foreach($school_data as $sd)
        {
            $school_id=$sd->school_id;
        }

        $slug = $request->slug? $request->slug : $request->title;
        $slug = $slug == '/'? $slug : str_slug($slug);

        $bookcategory = new Bookcategory();
        $bookcategory->school_id=$school_id;
        $bookcategory->parent_id = $request->parent_id;
        $bookcategory->title = $request->title;
        $bookcategory->slug = $slug;
        $bookcategory->details = $request->details;

        $bookcategory->order_by=(Bookcategory::max('order_by') + 1);
        $bookcategory->save();

        return redirect()->route('library/category')->with('success', 'Book category successfully added.');
    }
    public function bookscategoryedit($id)
    {
        $bookcategory=Bookcategory::find($id);
        $parents = Bookcategory::where('parent_id', 0)->get();
        return view('librarian::edit_category', compact('bookcategory', 'parents'));
    }
    public function bookscategoryupdate(Request $request,$id )
    {
//            $this->validate($request, [
//            'title' => 'required|unique:categories,title,' . $bookcategory->id,
//
//            'details' => 'required',
//            ]);

        $slug = $request->slug? $request->slug : $request->title;
        $slug = $slug == '/'? $slug : str_slug($slug);

        $bookcategory=Bookcategory::find($id);
        $updated=$bookcategory->fill($request->all())->save();
//

        return redirect()->route('library/category')->with('success', 'Book category edited.');
    }
    public function bookscategorydelete($id)
    {
        $bookcategory=Bookcategory::find($id)->delete();

        return redirect('library/category')->with('success', 'Book Category deleted successfully');
    }
//    For Book Item
    public function bookscreate()
    {
        $categories = Bookcategory::where('parent_id', 0)->get();
        return view('library::add_books',compact('categories'));
    }
    public function booksstore(Request $request)
    {
        $slug = $request->slug? $request->slug : $request->title;
        $slug = $slug == '/'? $slug : str_slug($slug);

        $books = new Book();
        $books->title = $request->title;
        $books->slug = $slug;
        $books->category_id = $request->category_id;
        $books->subcategory_id = $request->subcategory_id;
        $books->author = $request->author;
        $books->quantity = $request->quantity;
        $books->remaining= $books->quantity;
        $books->details = $request->details;
        $books->order_by=(Book::where('subcategory_id', $request->subcategory_id)->max('order_by') + 1);
        $books->save();

        return redirect()->route('library/books')->with('success', 'Book successfully added.');
    }
    public function booksedit($id)
    {
        $books=Book::find($id);
        $categories = Bookcategory::where('parent_id', 0)->get();
        return view('librarian::edit_book', compact('books', 'categories'));
    }
    public function booksupdate(Request $request,$id)
    {
        $slug = $request->slug? $request->slug : $request->title;
        $slug = $slug == '/'? $slug : str_slug($slug);

        $books=Book::find($id);
        $updated=$books->fill($request->all())->save();
//

        return redirect()->route('library/books')->with('success', 'Book edited.');

    }
    public function booksdelete($id)
    {
        $books=Book::find($id)->delete();

        return redirect('library/books')->with('success', 'Book deleted successfully');
    }


}
