@extends('layouts.admin_dashboard')

@section('content')

    <div class="col-3 mt-5">
    </div>
    <div class="col-6 mt-5">
        <div class="card">
            <div class="card-body">
                <a href="{{route('library/books')}}" class="btn btn-sm btn-secondary mb-3 pull-right"><i class="fa fa-arrow-left"></i>Back</a>
                <br>
                <h4 class="header-title">Edit Book</h4>

                <form action="{{route('library/books/bookupdate',$books->id)}}" method="post" accept-charset="utf-8">
                    {{csrf_field()}}
                    <div class="form-row">
                        <div class="col-4">
                            <div class="form-group">
                                <label for="category-title-input" class="col-form-label">Title<span class="text-danger">*</span></label>
                                <input class="form-control" type="text" autocomplete="off" id="category-title-input" name="title" value="{{ old('title', isset($books) ? $books->title : null) }}">
                            </div>
                        </div>
                        <div class="col-4">
                            <div class="form-group">
                                <label class="col-form-label" for="category_id">Category</label>
                                <select class="custom-select" name="category_id" id="category_id" class="form-control">
                                    <option value="0">None</option>
                                    @foreach ($categories as $category)
                                        <option value="{{ $category->id }}" {{ $category->id == old('category_id', isset($books) ? $books->category_id : null ) ? 'selected="selected"' : null }}>{{ $category->title }}</option>
                                    @endforeach


                                </select>
                            </div>
                        </div>
                        <div class="col-4">
                            <div class="form-group">
                                <label class="col-form-label" for="subcategory_id">Category</label>
                                <select class="custom-select" name="subcategory_id" id="subcategory_id" class="form-control">
                                    <option value="0">None</option>
                                    @foreach ($categories as $category)
                                        @foreach ($category->child as $child)
                                            <option value="{{ $child->id }}" {{ $child->id == old('subcategory_id', isset($books) ? $books->subcategory_id : null ) ? 'selected="selected"' : null }}>{{ $child->title }}</option>
                                        @endforeach
                                    @endforeach


                                </select>
                            </div>
                        </div>


                    </div>

                    <div class="form-group">
                        <label for="book-author-input" class="col-form-label">Author<span class="text-danger">*</span></label>
                        <input class="form-control" type="text" autocomplete="off" id="book-author-input" name="author" value="{{ old('author', isset($books) ? $books->author : null) }}">
                    </div>

                    <div class="form-group">
                        <label for="book-quantity-input" class="col-form-label">Quantity<span class="text-danger">*</span></label>
                        <input class="form-control" type="number" autocomplete="off" id="book-quantity-input" name="quantity" value="{{ old('quantity', isset($books) ? $books->quantity : null) }}" min="1">
                    </div>


                    <div class="form-group">
                        <label for="book-details-input" class="col-form-label text-muted mb-3 mt-4 d-block">Details<span class="text-danger">*</span></label>
                        <textarea name="details" id="book-details-input" class="form-control">{{ old('details', isset($books) ? $books->details : null) }}</textarea>

                    </div>

                    <div class="form-group">
                        <button type="submit" class="btn btn-flat btn-success mb-3"><span class="ti-save"></span> Save</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <div class="col-3 mt-5">
    </div>
@endsection
