@extends('layouts.admin_dashboard')

@section('title')
    Books
@endsection
@section('css')
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.css">
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.18/css/dataTables.bootstrap4.min.css">
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/responsive/2.2.3/css/responsive.bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/responsive/2.2.3/css/responsive.jqueryui.min.css">
@endsection
@section('content')


    <div class="row">
    <div class="col-sm-12 mt-5">

        <div class="card">

            <div class="card-body">
                <div class="add_section">
                    <a href="{{route('library/books/bookcreate')}}" class="btn btn-sm btn-primary mb-3 pull-right">
                        <span class="ti-plus"></span> Add Book
                    </a>
                </div>
                <h4 class="header-title">Book</h4>



                <div class="data-tables">
                    <table id="dataTable" class="table table-hover text-center">
                        <thead class="bg-light text-capitalize">

                        <tr>
                                            <th>Faculty</th>
                                            <th>Semester</th>
                                            <th>Title</th>
                                            <th>Author</th>
                                            <th>Quantity</th>
                                            <th>Created At</th>
                                            <th></th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach ($books as $book)
                            <tr id="{{$book->id}}">
                                <td>{{$book->category_title}}</td>
                                <td>{{ $book->subcategory_title }}</td>
                                <td>{{$book->title}}</td>
                                <td>{{$book->author}}</td>
                                <td>{{$book->quantity}}</td>
                                <td>{{ $book->created_at}}</td>
                                <td>
                                    <ul class="d-flex justify-content-center">
                                        <li class="mr-3"><a href="{{route('library/books/bookedit',$book->id)}}" class="text-secondary"><i class="fa fa-edit"></i></a></li>
                                        <li class="mr-3"><a href="{{route('library/books/bookdelete',$book->id)}}" class="text-danger"><i class="ti-trash"></i></a></li>
                                    </ul>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                 </div>
             </div>
         </div>
    </div>
    </div>
    @endsection

@section('script')
        <!-- Start datatable js -->
                    <script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.js"></script>
                    <script src="https://cdn.datatables.net/1.10.18/js/jquery.dataTables.min.js"></script>
                    <script src="https://cdn.datatables.net/1.10.18/js/dataTables.bootstrap4.min.js"></script>
                    <script src="https://cdn.datatables.net/responsive/2.2.3/js/dataTables.responsive.min.js"></script>
                    <script src="https://cdn.datatables.net/responsive/2.2.3/js/responsive.bootstrap.min.js"></script>



@endsection