@extends('layouts.admin_dashboard')

@section('title')

    books categories

@endsection

@section('content')


    <div class="col-12 mt-5">
        <div class="card">
            <div class="card-body">
                <a href="{{route('library/books/categorycreate')}}" class="btn btn-sm btn-primary mb-3 pull-right">
                    <span class="ti-plus"></span> Add Category
                </a>
                <br>
                <h4 class="header-title">Category</h4>






                {{--Table--}}
                <div class="single-table">
                    <div class="table-responsive">
                        <table class="table table-hover progress-table text-center">
                            <thead class="text-uppercase">
                            <tr>
                                <th scope="col">Parent</th>
                                <th scope="col">Title</th>
                                <th scope="col">Created At</th>

                                <th scope="col">Action</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach ($bookcategories as $bookcategory)
                                <tr id="{{$bookcategory->id}}">
                                    <td>{{ $bookcategory->parent_title }}</td>
                                    <td>{{$bookcategory->title}}</td>
                                    <td>{{ $bookcategory->created_at->format('M d, Y') }}</td>
                                    <td>
                                        <ul class="d-flex justify-content-center">

                                            <li class="mr-3"><a href="{{route('library/books/categoryedit',$bookcategory->id)}}" class="text-secondary"><i class="fa fa-edit"></i></a></li>
                                            <li class="mr-3"><a href="{{route('library/books/categorydelete',$bookcategory->id)}}" class="text-danger"><i class="ti-trash"></i></a></li>
                                        </ul>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
                {{--Table End--}}
            </div>

         </div>

    </div>

@endsection