@extends('layouts.admin_dashboard')
@section('title')
    add category
@endsection

@section('content')

    <div class="col-sm-3 mt-5">
    </div>
    <div class="col-sm-6 mt-5">
        <div class="card">
            <div class="card-body">
                <a href="{{route('library/category')}}" class="btn btn-sm btn-primary mb-3 pull-right"><i class="fa fa-arrow-left"></i>Back</a>
                <br>
                <h4 class="header-title">Add new Category</h4>

                <form action="{{route('library/books/categorystore')}}" method="post" accept-charset="utf-8">
                    {{csrf_field()}}
                            {{--<div class="form-group">--}}
                                {{--<label for="category-title-input" class="col-form-label">Class<span class="text-danger">*</span></label>--}}
                                {{--<select name="title" id="category_title_input" class="form-control">--}}
                                    {{--<option selected="selected">Select Class</option>--}}
                                    {{--@foreach ($grade_data as $gd)--}}
                                        {{--<option value="{{ $gd->name }}" {{ $gd->name == old('title', isset($bookcategory) ? $bookcategory->title : null ) ? 'selected="selected"' : null }}>{{ $gd->name}}</option>--}}
                                    {{--@endforeach--}}

                                {{--</select>                            </div>--}}
                    <div class="form-group">
                        <label for="category-title-input" class="col-form-label">Title<span class="text-danger">*</span></label>
                        <input class="form-control" type="text" autocomplete="off" id="category-title-input" name="title" value="{{ old('title', isset($bookcategory) ? $bookcategory->title : null) }}">
                    </div>

                            <div class="form-group">
                                <label class="col-form-label" for="parent_id">Parent</label>
                                <select name="parent_id" id="parent_id" class="form-control">
                                    <option value="0">None</option>
                                    @foreach ($parents as $parent)
                                        <option value="{{ $parent->id }}" {{ $parent->id == old('parent_id', isset($bookcategory) ? $bookcategory->parent_id : null ) ? 'selected="selected"' : null }}>{{ $parent->title }}</option>
                                    @endforeach

                                </select>
                            </div>




                    <div class="form-group">
                        <label for="category-details-input" class="col-form-label text-muted mb-3 mt-4 d-block">Details<span class="text-danger">*</span></label>
                        <textarea name="details" id="category-details-input" class="form-control" rows="5">{{ old('details', isset($bookcategory) ? $bookcategory->details : null) }}</textarea>

                    </div>


                    <div class="form-group">
                        <button type="submit" class="btn btn-flat btn-success mb-3"><span class="ti-save"></span> Save</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <div class="col-sm-3 mt-5">
    </div>
@endsection
