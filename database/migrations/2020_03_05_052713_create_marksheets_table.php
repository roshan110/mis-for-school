<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMarksheetsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('marksheets', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('school_id');                     
            $table->integer('grade_id');
            $table->integer('section_id');
            $table->integer('student_id');  
            $table->integer('exam_id');
            $table->float('total_theory_marks');
            $table->float('total_practical_marks');
            $table->float('total_marks');
            $table->float('percentage');
            $table->string('result');
            $table->string('rank')->nullable();
            $table->float('gpa')->nullable();
            $table->string('grade')->nullable();
            $table->integer('status');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('marksheets');
    }
}
