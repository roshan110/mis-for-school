-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Oct 24, 2019 at 09:26 AM
-- Server version: 10.1.37-MariaDB
-- PHP Version: 7.2.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `mis`
--

-- --------------------------------------------------------

--
-- Table structure for table `academicinfos`
--

CREATE TABLE `academicinfos` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `s_id` bigint(20) UNSIGNED DEFAULT NULL,
  `registration_no` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `academic_year` int(11) NOT NULL,
  `grade_id` int(11) NOT NULL,
  `section_id` int(11) NOT NULL,
  `roll_no` int(11) NOT NULL,
  `status` int(11) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `accountants`
--

CREATE TABLE `accountants` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `school_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `address` text COLLATE utf8mb4_unicode_ci,
  `contact` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `image` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `accountants`
--

INSERT INTO `accountants` (`id`, `school_id`, `user_id`, `name`, `address`, `contact`, `image`, `status`, `created_at`, `updated_at`) VALUES
(3, 2, 11, 'Test Accountant', 'lakdjlaksdlaksdjla aksdlalksdj alskdal', '98412584', 'accountant-3.jpeg', 1, '2019-09-30 02:44:01', '2019-10-14 23:35:18');

-- --------------------------------------------------------

--
-- Table structure for table `admin_details`
--

CREATE TABLE `admin_details` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `admin_id` int(11) NOT NULL,
  `school_id` int(11) NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `address` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `contact` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `profile_img` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `admin_details`
--

INSERT INTO `admin_details` (`id`, `admin_id`, `school_id`, `name`, `address`, `contact`, `profile_img`, `status`, `created_at`, `updated_at`) VALUES
(1, 4, 2, 'Roshan', 'Gothatar', '9843412261', NULL, 0, '2019-09-16 05:11:51', '2019-09-16 05:11:51');

-- --------------------------------------------------------

--
-- Table structure for table `attendances`
--

CREATE TABLE `attendances` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `grade_id` int(11) NOT NULL,
  `section_id` int(11) NOT NULL,
  `date` date NOT NULL,
  `st_id` int(11) NOT NULL,
  `status` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `attendances`
--

INSERT INTO `attendances` (`id`, `grade_id`, `section_id`, `date`, `st_id`, `status`, `created_at`, `updated_at`) VALUES
(1, 3, 1, '2019-09-22', 1, 1, '2019-09-22 01:17:03', '2019-09-22 01:17:03'),
(2, 3, 1, '2019-09-22', 2, 0, '2019-09-22 01:17:03', '2019-09-22 01:17:03'),
(3, 4, 3, '2019-09-22', 3, 1, '2019-09-22 07:03:18', '2019-09-22 07:03:18'),
(4, 4, 3, '2019-09-23', 3, 1, '2019-09-23 01:42:30', '2019-09-23 01:42:30'),
(5, 3, 1, '2019-09-23', 1, 1, '2019-09-23 04:05:35', '2019-09-23 04:05:35'),
(6, 3, 1, '2019-09-23', 2, 0, '2019-09-23 04:05:35', '2019-09-23 04:05:35'),
(7, 3, 1, '2019-09-24', 1, 1, '2019-09-24 00:18:58', '2019-09-24 00:18:58'),
(8, 3, 1, '2019-09-24', 2, 0, '2019-09-24 00:18:58', '2019-09-24 00:18:58'),
(9, 4, 3, '2019-09-24', 3, 0, '2019-09-24 03:29:42', '2019-09-24 03:29:42'),
(10, 3, 1, '2019-09-24', 1, 1, '2019-09-24 03:30:03', '2019-09-24 03:30:03'),
(11, 3, 1, '2019-09-24', 2, 0, '2019-09-24 03:30:03', '2019-09-24 03:30:03'),
(12, 17, 6, '2019-10-22', 9, 1, '2019-10-21 23:18:54', '2019-10-21 23:18:54');

-- --------------------------------------------------------

--
-- Table structure for table `books`
--

CREATE TABLE `books` (
  `id` int(10) UNSIGNED NOT NULL,
  `category_id` int(11) NOT NULL,
  `subcategory_id` int(11) NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `author` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `quantity` int(10) NOT NULL,
  `remaining` int(11) DEFAULT NULL,
  `details` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` int(11) NOT NULL,
  `order_by` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `books`
--

INSERT INTO `books` (`id`, `category_id`, `subcategory_id`, `title`, `slug`, `author`, `quantity`, `remaining`, `details`, `status`, `order_by`, `created_at`, `updated_at`) VALUES
(1, 0, 0, 'Mathematics', 'mathematics', 'Professor ABS', 10, 10, 'Maths book', 1, 1, '2019-09-26 03:53:07', '2019-09-26 03:53:07'),
(2, 1, 0, 'Optional Mathematics', 'optional-mathematics', 'dr.drake', 5, 5, 'jh', 1, 2, '2019-09-26 03:54:41', '2019-09-26 03:54:41');

-- --------------------------------------------------------

--
-- Table structure for table `book_categories`
--

CREATE TABLE `book_categories` (
  `id` int(10) UNSIGNED NOT NULL,
  `school_id` int(11) NOT NULL,
  `parent_id` int(11) NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `details` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` int(11) NOT NULL,
  `order_by` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `book_categories`
--

INSERT INTO `book_categories` (`id`, `school_id`, `parent_id`, `title`, `slug`, `details`, `status`, `order_by`, `created_at`, `updated_at`) VALUES
(1, 2, 0, 'Maths', 'maths', 'sdf', 1, 1, '2019-09-26 03:53:44', '2019-09-26 03:53:44');

-- --------------------------------------------------------

--
-- Table structure for table `exams`
--

CREATE TABLE `exams` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `school_id` int(11) NOT NULL,
  `examtype` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `exams`
--

INSERT INTO `exams` (`id`, `school_id`, `examtype`, `description`, `status`, `created_at`, `updated_at`) VALUES
(2, 2, '2nd term', 'second term examination', 1, '2019-09-27 01:08:02', '2019-09-30 05:48:40'),
(4, 2, 'First term', 'first term exam', 1, '2019-09-30 04:37:10', '2019-10-14 22:54:15');

-- --------------------------------------------------------

--
-- Table structure for table `grades`
--

CREATE TABLE `grades` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `school_id` int(11) NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` longtext COLLATE utf8mb4_unicode_ci,
  `status` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `grades`
--

INSERT INTO `grades` (`id`, `school_id`, `name`, `description`, `status`, `created_at`, `updated_at`) VALUES
(14, 2, 'Eight', 'eight class', 0, '2019-09-30 04:13:44', '2019-09-30 04:13:44'),
(15, 2, 'Seven', 'seven class', 0, '2019-09-30 04:13:56', '2019-09-30 04:13:56'),
(16, 2, 'Nine', 'ksjdfh', 1, '2019-10-14 23:32:27', '2019-10-14 23:32:27'),
(17, 2, 'Five', NULL, 1, '2019-10-17 01:48:20', '2019-10-17 05:11:29');

-- --------------------------------------------------------

--
-- Table structure for table `grade_subject`
--

CREATE TABLE `grade_subject` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `grade_id` int(11) NOT NULL,
  `subject_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `grade_subject`
--

INSERT INTO `grade_subject` (`id`, `grade_id`, `subject_id`, `created_at`, `updated_at`) VALUES
(3, 17, 4, NULL, NULL),
(4, 17, 5, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `librarians`
--

CREATE TABLE `librarians` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `school_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `address` text COLLATE utf8mb4_unicode_ci,
  `contact` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `image` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `librarians`
--

INSERT INTO `librarians` (`id`, `school_id`, `user_id`, `name`, `address`, `contact`, `image`, `status`, `created_at`, `updated_at`) VALUES
(2, 2, 13, 'Librarian', 'asdijaosdij alsdkalsdkja alskdjjalskdj asldkjalskdj asldkals', '987462456', 'librarian-2.jpeg', 1, '2019-09-30 04:31:41', '2019-09-30 04:31:41');

-- --------------------------------------------------------

--
-- Table structure for table `marks`
--

CREATE TABLE `marks` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `school_id` int(11) NOT NULL,
  `grade_id` int(11) NOT NULL,
  `section_id` int(11) NOT NULL,
  `subject_id` int(11) NOT NULL,
  `st_id` int(11) NOT NULL,
  `marks` int(11) NOT NULL,
  `practical_marks` int(11) NOT NULL,
  `examtype` int(11) NOT NULL,
  `status` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `marks`
--

INSERT INTO `marks` (`id`, `school_id`, `grade_id`, `section_id`, `subject_id`, `st_id`, `marks`, `practical_marks`, `examtype`, `status`, `created_at`, `updated_at`) VALUES
(6, 2, 3, 1, 1, 1, 60, 25, 1, 0, '2019-09-29 23:10:04', '2019-09-29 23:10:04'),
(7, 2, 3, 1, 1, 2, 65, 25, 1, 0, '2019-09-29 23:10:04', '2019-09-29 23:10:04'),
(8, 2, 4, 3, 1, 3, 70, 25, 1, 0, '2019-09-29 23:10:58', '2019-09-29 23:10:58'),
(9, 2, 14, 5, 2, 4, 60, 20, 4, 1, '2019-10-17 01:51:00', '2019-10-17 01:51:00'),
(10, 2, 14, 5, 2, 6, 60, 20, 4, 1, '2019-10-17 01:51:00', '2019-10-17 01:51:00'),
(11, 2, 14, 5, 2, 7, 60, 20, 4, 1, '2019-10-17 01:51:00', '2019-10-17 01:51:00'),
(12, 2, 14, 5, 2, 8, 60, 20, 4, 1, '2019-10-17 01:51:00', '2019-10-17 01:51:00'),
(13, 2, 17, 6, 2, 9, 60, 20, 4, 1, '2019-10-17 04:54:53', '2019-10-17 04:54:53'),
(14, 2, 17, 6, 3, 9, 60, 20, 4, 1, '2019-10-20 05:02:19', '2019-10-20 05:02:19'),
(15, 2, 17, 6, 4, 9, 50, 20, 4, 1, '2019-10-20 05:02:57', '2019-10-20 05:02:57');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2016_01_15_105324_create_roles_table', 1),
(4, '2016_01_15_114412_create_role_user_table', 1),
(5, '2016_01_26_115212_create_permissions_table', 1),
(6, '2016_01_26_115523_create_permission_role_table', 1),
(7, '2016_02_09_132439_create_permission_user_table', 1),
(8, '2019_09_16_090201_create_schools_table', 2),
(10, '2019_09_16_090416_create_admin_details_table', 3),
(11, '2019_09_17_060552_create_sections_table', 4),
(12, '2019_09_17_060958_create_grades_table', 4),
(13, '2019_09_17_092225_create_subjects_table', 5),
(14, '2019_09_17_111626_create_students_table', 5),
(15, '2019_09_17_111659_create_parents_table', 5),
(16, '2019_09_18_073055_add_school_id_to_subjects_table', 6),
(17, '2019_09_18_073343_add_school_id_to_students_table', 6),
(18, '2019_09_18_073425_add_school_id_to_parents_table', 6),
(19, '2019_09_18_074156_add_practicalmarks_theorymarks_to_subjects_table', 7),
(20, '2019_09_18_075542_create_teachers_table', 8),
(21, '2019_09_18_081618_add_image_to_teachers_table', 9),
(22, '2019_09_18_081617_add_image_to_teachers_table', 10),
(23, '2019_09_18_103050_create_attendances_table', 11),
(24, '2019_09_18_081109_add_image_to_parent_table', 12),
(25, '2019_09_18_081804_add_image_to_students_table', 12),
(26, '2019_09_19_060757_add_status_to_atttendances_table', 13),
(27, '2019_09_25_063517_add_user_id_to_teachers_table', 14),
(28, '2019_09_25_102532_create_marks_table', 15),
(29, '2019_09_27_061915_create_exams_table', 16),
(30, '2019_09_27_102808_add_practical_marks_to_marks_table', 17),
(31, '2019_09_30_103159_add_status_to_admin_details_table', 18),
(32, '2019_09_30_103437_add_status_to_exams_table', 18),
(33, '2019_09_30_103544_add_status_to_grades_table', 18),
(34, '2019_09_30_103649_add_status_to_marks_table', 18),
(35, '2019_09_30_103718_add_status_to_parents_table', 18),
(36, '2019_09_30_103742_add_status_to_schools_table', 18),
(37, '2019_09_30_103810_add_status_to_sections_table', 18),
(38, '2019_09_30_103835_add_status_to_students_table', 18),
(39, '2019_09_30_103859_add_status_to_subjects_table', 18),
(40, '2019_09_30_103936_add_status_to_teachers_table', 18),
(41, '2019_09_30_103817_create_grade_subject_table', 19),
(42, '2019_10_15_060043_add_dob_gender_religion_column_to_students', 20),
(43, '2019_09_30_104817_create_grade_subject_table', 21),
(44, '2019_10_21_092402_create_academicinfos_table', 22),
(45, '2019_10_21_092912_create_academicinfos_table', 23),
(46, '2019_10_20_192912_create_academicinfos_table', 24),
(47, '2019_10_21_283635_add_foreign_key_sid_on_acedamicinfo', 24);

-- --------------------------------------------------------

--
-- Table structure for table `parents`
--

CREATE TABLE `parents` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `school_id` int(11) NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `address` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `contact` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `parents`
--

INSERT INTO `parents` (`id`, `school_id`, `name`, `email`, `address`, `contact`, `image`, `status`, `created_at`, `updated_at`) VALUES
(1, 2, 'Hari Kunwar', 'hari@gmail.com', 'bafal', '9876543212', 'Hari Kunwar-9876543212.jpeg', 0, '2019-09-19 01:44:02', '2019-09-19 01:44:02'),
(2, 2, 'Prent2', 'parent2@gmail.com', 'Ktm', '9876543212', 'Prent2-9876543212.jpeg', 0, '2019-09-21 23:24:03', '2019-09-21 23:24:04'),
(3, 2, 'Parents1', 'parents1@gmail.com', 'Kathmandu', '9876543212', 'Parents1-9876543212.jpeg', 0, '2019-09-30 01:16:16', '2019-10-14 23:27:17'),
(4, 2, 'Parents2', 'parents2@gmail.com', 'bhaktapur', '9876543212', 'Parents2-9876543212.jpeg', 1, '2019-10-14 23:25:27', '2019-10-14 23:25:27');

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `permissions`
--

CREATE TABLE `permissions` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `model` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `permission_role`
--

CREATE TABLE `permission_role` (
  `id` int(10) UNSIGNED NOT NULL,
  `permission_id` int(10) UNSIGNED NOT NULL,
  `role_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `permission_user`
--

CREATE TABLE `permission_user` (
  `id` int(10) UNSIGNED NOT NULL,
  `permission_id` int(10) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `roles`
--

CREATE TABLE `roles` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `level` int(11) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `roles`
--

INSERT INTO `roles` (`id`, `name`, `slug`, `description`, `level`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'Super Admin', 'superadmin', 'SuperAdmin Role', 7, NULL, NULL, NULL),
(2, 'Admin', 'admin', 'Admin Role', 5, NULL, NULL, NULL),
(4, 'Teacher', 'teacher', 'Teacher Role', 4, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `role_user`
--

CREATE TABLE `role_user` (
  `id` int(10) UNSIGNED NOT NULL,
  `role_id` int(10) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `role_user`
--

INSERT INTO `role_user` (`id`, `role_id`, `user_id`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 1, 1, NULL, NULL, NULL),
(2, 2, 4, '2019-09-16 05:11:51', '2019-09-16 05:11:51', NULL),
(3, 4, 8, '2019-09-25 01:28:18', '2019-09-25 01:28:18', NULL),
(5, 4, 11, '2019-09-30 05:38:02', '2019-09-30 05:38:02', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `schools`
--

CREATE TABLE `schools` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `address` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `contact` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `schools`
--

INSERT INTO `schools` (`id`, `name`, `address`, `contact`, `status`, `created_at`, `updated_at`) VALUES
(2, 'Zenith', 'Bafal,Kathmandu', '014270965', 0, '2019-09-16 05:11:51', '2019-09-16 05:11:51');

-- --------------------------------------------------------

--
-- Table structure for table `sections`
--

CREATE TABLE `sections` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `grade_id` int(11) NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` longtext COLLATE utf8mb4_unicode_ci,
  `status` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `sections`
--

INSERT INTO `sections` (`id`, `grade_id`, `name`, `description`, `status`, `created_at`, `updated_at`) VALUES
(1, 3, 'ABC', 'kjhgfdghjklkjhgfcfxfghj', 0, '2019-09-17 02:54:49', '2019-09-17 02:54:49'),
(2, 3, 'Section2', 'kjdhfkjh', 0, '2019-09-18 23:47:54', '2019-09-18 23:47:54'),
(3, 4, 'XYZ', 'ksdf', 0, '2019-09-21 23:21:56', '2019-09-21 23:21:56'),
(4, 4, 'Mt.Everest', 'kjhf', 0, '2019-09-23 03:01:55', '2019-09-23 03:01:55'),
(5, 14, 'Makalu', 'kjshdkfj', 1, '2019-10-14 23:05:28', '2019-10-14 23:05:28'),
(6, 17, 'Annapurna', 'okksdfoj', 1, '2019-10-17 04:51:57', '2019-10-17 04:51:57');

-- --------------------------------------------------------

--
-- Table structure for table `students`
--

CREATE TABLE `students` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `school_id` int(11) NOT NULL,
  `parent_id` int(11) NOT NULL,
  `grade_id` int(11) NOT NULL,
  `section_id` int(11) NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `address` longtext COLLATE utf8mb4_unicode_ci,
  `contact` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `image` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `date_of_birth` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `gender` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `religion` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `students`
--

INSERT INTO `students` (`id`, `school_id`, `parent_id`, `grade_id`, `section_id`, `name`, `email`, `address`, `contact`, `image`, `date_of_birth`, `status`, `created_at`, `updated_at`, `gender`, `religion`) VALUES
(3, 2, 2, 4, 3, 'Ayush Raj Shrestha', 'ayushraj@gmail.com', 'Bhaktapur', '9876543212', 'Ayush Raj Shrestha-9876543212.jpeg', '', 1, '2019-09-21 23:25:08', '2019-10-14 23:14:06', '', ''),
(4, 2, 3, 14, 5, 'Adesh KC', 'adesh@gmail.com', 'lalitpur', '9812345678', 'Adesh KC-9812345678.jpeg', '', 1, '2019-10-14 23:08:00', '2019-10-14 23:08:00', '', ''),
(6, 2, 4, 14, 5, 'Pemba Tamang', 'pemba@gmail.com', 'Lalitpur', '9876543212', 'Pemba Tamang-9876543212.jpeg', '1995-08-24', 1, '2019-10-16 01:31:01', '2019-10-16 01:31:01', NULL, NULL),
(7, 2, 4, 14, 5, 'Nabin Acharya', 'nabin@gmail.com', 'Kathmandu', '9876543212', 'Nabin Acharya-9876543212.jpeg', '1996-09-23', 1, '2019-10-16 01:51:20', '2019-10-16 01:51:20', 'male', 'Hindu'),
(8, 2, 3, 14, 5, 'Kushal KC', 'kushal@gmail.com', 'Kathmandu', '9876543212', 'Kushal KC-9876543212.jpeg', '1997-06-04', 1, '2019-10-16 02:17:48', '2019-10-16 02:17:48', 'male', 'Hindu'),
(9, 2, 4, 17, 6, 'Sabin Maharjan', 'sabin@gmail.com', 'Lalitpur', '9876543212', 'Sabin Maharjan-9876543212.jpeg', '1997-07-08', 1, '2019-10-17 04:53:44', '2019-10-17 04:53:45', 'male', 'Hindu');

-- --------------------------------------------------------

--
-- Table structure for table `subjects`
--

CREATE TABLE `subjects` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `school_id` int(11) NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `fullmarks` int(11) NOT NULL,
  `practical_marks` int(11) NOT NULL,
  `theory_marks` int(11) NOT NULL,
  `description` longtext COLLATE utf8mb4_unicode_ci,
  `status` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `subjects`
--

INSERT INTO `subjects` (`id`, `school_id`, `name`, `fullmarks`, `practical_marks`, `theory_marks`, `description`, `status`, `created_at`, `updated_at`) VALUES
(1, 2, 'Computer Science', 100, 25, 75, 'This is the technical subject . must cover practical.', 0, '2019-09-18 23:49:52', '2019-09-18 23:49:52'),
(2, 2, 'Computer', 100, 20, 80, 'computer science a technical related subject.', 0, '2019-09-30 04:31:14', '2019-09-30 04:31:54'),
(3, 2, 'C++', 100, 40, 60, 'object oriented', 1, '2019-10-14 23:38:05', '2019-10-14 23:38:05'),
(4, 2, 'Science', 80, 20, 60, 'ksjhdkjh', 1, '2019-10-17 05:08:35', '2019-10-17 05:08:35'),
(5, 2, 'Social Studies', 100, 25, 75, 'Social Studies description', 1, '2019-10-17 05:09:22', '2019-10-17 05:09:22');

-- --------------------------------------------------------

--
-- Table structure for table `teachers`
--

CREATE TABLE `teachers` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `user_id` int(11) NOT NULL,
  `school_id` int(11) NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `address` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `contact` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `teachers`
--

INSERT INTO `teachers` (`id`, `user_id`, `school_id`, `name`, `email`, `address`, `contact`, `image`, `status`, `created_at`, `updated_at`) VALUES
(1, 0, 3, 'Teacher2', 'teacher2@gmail.com', 'ktm', '987654322', NULL, 0, '2019-09-18 02:49:17', '2019-09-18 02:49:17'),
(7, 8, 2, 'Jignesh Sharma', 'jignesh@gmail.com', 'Ktm', '9876543212', 'Jignesh Sharma-9876543212.jpeg', 0, '2019-09-25 01:28:18', '2019-09-25 01:28:18'),
(9, 11, 2, 'Teacher3', 'teacher3@gmail.com', 'Ltp', '9876543215', 'Teacher3-9876543215.jpeg', 1, '2019-09-30 05:38:01', '2019-09-30 05:38:02');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `email_verified_at`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'superadmin', 'superadmin@gmail.com', NULL, '$2y$12$QHtU/tNDIbyjMkrvQSgN9eARLV9blukI3Py.VKGp1exky6kitKzii', NULL, NULL, NULL),
(4, 'Roshan', 'roshan@gmail.com', NULL, '$2y$10$FAiLowUm0AtjEuRjNBaWzuN4i6.nKo1f1vsvEHAziJ3L.lS7EumXq', NULL, '2019-09-16 05:11:51', '2019-09-16 05:11:51'),
(6, 'AyushRaj Shrestha', 'ayushsir@gmail.com', NULL, '$2y$10$OctETm/K0IqejVYsf3BAWOrTOEoCSvL9VQwUoDST8N.g1g19kxkMW', NULL, '2019-09-25 01:10:51', '2019-09-25 01:10:51'),
(8, 'Jignesh Sharma', 'jignesh@gmail.com', NULL, '$2y$10$9z.0ozcyz6tf/RxOLX.7GO1R.Tng.u2Av0XX8o9rNUe9pW61ICtPq', NULL, '2019-09-25 01:28:18', '2019-09-25 01:28:18'),
(9, 'teacher2', 'teacher2@gmail.com', NULL, '$2y$10$0vcQW7iE39ZOYAnvsnKC2.CFymSdWu2OWP.7/kpf57vp4Sn17VFJa', NULL, '2019-09-30 05:27:14', '2019-09-30 05:27:14'),
(11, 'Teacher3', 'teacher3@gmail.com', NULL, '$2y$10$oQQkkzmO2mvDnDdGphl3AOWjPWQUfpXiPffeHL/S0yEK8KcSggKAK', NULL, '2019-09-30 05:38:01', '2019-09-30 05:38:01');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `academicinfos`
--
ALTER TABLE `academicinfos`
  ADD PRIMARY KEY (`id`),
  ADD KEY `academicinfos_s_id_foreign` (`s_id`);

--
-- Indexes for table `accountants`
--
ALTER TABLE `accountants`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `admin_details`
--
ALTER TABLE `admin_details`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `attendances`
--
ALTER TABLE `attendances`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `books`
--
ALTER TABLE `books`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `book_categories`
--
ALTER TABLE `book_categories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `exams`
--
ALTER TABLE `exams`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `grades`
--
ALTER TABLE `grades`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `grade_subject`
--
ALTER TABLE `grade_subject`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `librarians`
--
ALTER TABLE `librarians`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `marks`
--
ALTER TABLE `marks`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `parents`
--
ALTER TABLE `parents`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `parents_email_unique` (`email`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `permissions`
--
ALTER TABLE `permissions`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `permissions_slug_unique` (`slug`);

--
-- Indexes for table `permission_role`
--
ALTER TABLE `permission_role`
  ADD PRIMARY KEY (`id`),
  ADD KEY `permission_role_permission_id_index` (`permission_id`),
  ADD KEY `permission_role_role_id_index` (`role_id`);

--
-- Indexes for table `permission_user`
--
ALTER TABLE `permission_user`
  ADD PRIMARY KEY (`id`),
  ADD KEY `permission_user_permission_id_index` (`permission_id`),
  ADD KEY `permission_user_user_id_index` (`user_id`);

--
-- Indexes for table `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `roles_slug_unique` (`slug`);

--
-- Indexes for table `role_user`
--
ALTER TABLE `role_user`
  ADD PRIMARY KEY (`id`),
  ADD KEY `role_user_role_id_index` (`role_id`),
  ADD KEY `role_user_user_id_index` (`user_id`);

--
-- Indexes for table `schools`
--
ALTER TABLE `schools`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sections`
--
ALTER TABLE `sections`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `students`
--
ALTER TABLE `students`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `students_email_unique` (`email`);

--
-- Indexes for table `subjects`
--
ALTER TABLE `subjects`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `teachers`
--
ALTER TABLE `teachers`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `academicinfos`
--
ALTER TABLE `academicinfos`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `accountants`
--
ALTER TABLE `accountants`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `admin_details`
--
ALTER TABLE `admin_details`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `attendances`
--
ALTER TABLE `attendances`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `books`
--
ALTER TABLE `books`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `book_categories`
--
ALTER TABLE `book_categories`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `exams`
--
ALTER TABLE `exams`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `grades`
--
ALTER TABLE `grades`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- AUTO_INCREMENT for table `grade_subject`
--
ALTER TABLE `grade_subject`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `librarians`
--
ALTER TABLE `librarians`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `marks`
--
ALTER TABLE `marks`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=48;

--
-- AUTO_INCREMENT for table `parents`
--
ALTER TABLE `parents`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `permissions`
--
ALTER TABLE `permissions`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `permission_role`
--
ALTER TABLE `permission_role`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `permission_user`
--
ALTER TABLE `permission_user`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `roles`
--
ALTER TABLE `roles`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `role_user`
--
ALTER TABLE `role_user`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `schools`
--
ALTER TABLE `schools`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `sections`
--
ALTER TABLE `sections`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `students`
--
ALTER TABLE `students`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `subjects`
--
ALTER TABLE `subjects`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `teachers`
--
ALTER TABLE `teachers`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `academicinfos`
--
ALTER TABLE `academicinfos`
  ADD CONSTRAINT `academicinfos_s_id_foreign` FOREIGN KEY (`s_id`) REFERENCES `students` (`id`);

--
-- Constraints for table `permission_role`
--
ALTER TABLE `permission_role`
  ADD CONSTRAINT `permission_role_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `permission_role_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `permission_user`
--
ALTER TABLE `permission_user`
  ADD CONSTRAINT `permission_user_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `permission_user_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `role_user`
--
ALTER TABLE `role_user`
  ADD CONSTRAINT `role_user_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `role_user_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
